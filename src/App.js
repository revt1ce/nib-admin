import React from "react";
import AdminApp from "./apps/Admin";
import Login from "./pages/Login";
import { Switch, Route } from "react-router-dom";

function App() {
	return (
		<Switch>
			<Route path="/" component={AdminApp} />
			<Route exact path="/login" component={Login} />
		</Switch>
		
	);
}

export default App;
