import React, { useState, useEffect } from "react";
import Modal from "./Modal";
import CatsTreeModal from "./CatsTreeModal";
import { GET_CATS, GET_CURRENT_USER, GET_GALLERIES } from "../graphql/queries";
import { useQuery, useMutation } from "react-apollo";
import { CREATE_GALLERY } from "../graphql/mutations";
import Message from "./Message";

const NewGalleryModal = ({ modalHandler }) => {
	const [name, setName] = useState("");
	const [cat, setCat] = useState("");
	const [author, setAuthor] = useState("");
	const [pickCat, setPickCat] = useState(false);

	const [message, setMessage] = useState("");
	const [showMessage, setShowMessage] = useState(false);
	const [messageType, setMessageType] = useState("");

	useQuery(GET_CURRENT_USER, {
		onCompleted(data) {
			setAuthor(data.me.username);
		},
	});

	const { data } = useQuery(GET_CATS, {
		variables: {
			language: "mn",
		},
	});

	const [createGallery, { data: galleryData }] = useMutation(CREATE_GALLERY, {
		onCompleted() {
			setName("");
			setCat("");
			setMessage("Амжилттай нэмэгдлээ!");
			setShowMessage(true);
			setMessageType("is-success");
		},
		update(cache, { data: { createGallery } }) {
			const { getGalleries } = cache.readQuery({
				query: GET_GALLERIES,
				variables: {
					language: "mn",
					catKey: galleryData?.category.key,
				},
			});
			cache.writeQuery({
				query: GET_GALLERIES,
				data: { getGalleries: [...getGalleries, createGallery] },
				variables: {
					language: "mn",
					catKey: galleryData?.category.key,
				},
			});
		},
		onError(error) {
			setMessageType("is-danger");
			setMessage(error?.graphQLErrors[0]?.message);
			setShowMessage(true);
		},
	});

	const submit = (e) => {
		e.preventDefault();
		if (!cat) {
			setMessageType("is-danger");
			setMessage("Цэс сонгоогүй байна!");
			setShowMessage(true);
		} else {
			createGallery({
				variables: {
					name: name,
					catKey: cat.key,
					author: author,
				},
			});
		}
	};

	const selectCatHandler = (cat) => {
		setCat(cat);
		setPickCat(false);
	};

	const catModalHandler = () => {
		setPickCat(!pickCat);
	};

	const clearStates = () => {
		setName("");
		setCat("");
		setShowMessage(false);
	};

	useEffect(() => {
		if (showMessage) {
			setTimeout(() => {
				setShowMessage(false);
			}, [4000]);
		}
	}, [showMessage]);

	useEffect(() => {
		setShowMessage(false);
	}, [name, pickCat]);

	return (
		<div className="box container" style={{ maxWidth: 800 }}>
			<Modal active={pickCat} modalHandler={catModalHandler}>
				<CatsTreeModal
					cats={data?.getCats}
					language="mn"
					modalHandler={catModalHandler}
					onSelect={selectCatHandler}
				/>
			</Modal>
			<div className="is-flex" style={{ justifyContent: "space-between" }}>
				<div />
				<button
					className="button is-danger is-light"
					onClick={() => {
						modalHandler(false);
						clearStates();
					}}
				>
					Буцах
				</button>
			</div>
			<form onSubmit={submit}>
				<div className="field">
					<label className="label">Галерейн нэр</label>
					<div className="control">
						<input
							className="input"
							type="text"
							value={name}
							onChange={(e) => setName(e.target.value)}
							required
						/>
					</div>
				</div>
				<div className="field">
					<label className="label">Цэс</label>
					<div className="control is-flex">
						<input className="input" disabled={true} value={cat && cat.name} type="text" required />
						<span style={{ marginLeft: 10 }} className="button" onClick={() => setPickCat(true)}>
							Сонгох
						</span>
					</div>
				</div>
				{showMessage && <Message type={messageType} message={message} />}
				<button className="button is-success">Нэмэх</button>
			</form>
		</div>
	);
};

export default NewGalleryModal;
