import React from "react";

const Modal = ({ children, active, width, height, modalHandler }) => {
	return (
		<div className={`modal ${active ? "is-active" : ""}`}>
			<div className="modal-background" />
			<div className="modal-content" style={{ width: width ? width : "", height: height ? height : "" }}>
				{children}
			</div>
		</div>
	);
};

export default Modal;
