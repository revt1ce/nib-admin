import React, { useState, useEffect } from "react";
import CKEditor from "ckeditor4-react";

const TabInput = ({ modalHandler, onSubmit, data }) => {
	const [title, setTitle] = useState("");
	const [content, setContent] = useState("");

	const submit = (e) => {
		e.preventDefault();
		onSubmit({
			title: title,
			content: content,
		});
	};
	useEffect(() => {
		if (data) {
			setTitle(data.title);
			setContent(data.content);
		} else {
			setTitle("");
			setContent("");
		}
	}, [data]);

	return (
		<div className="box container">
			<div className="is-flex buttons" style={{ justifyContent: "space-between" }}>
				<div />
				<button
					className="button is-danger is-light"
					onClick={() => {
						modalHandler(false);
						setTitle("");
						setContent("");
					}}
				>
					Буцах
				</button>
			</div>
			<form onSubmit={submit}>
				<div className="field">
					<label className="label">Нэр</label>
					<div className="control">
						<input
							className="input "
							type="text"
							value={title}
							onChange={(e) => setTitle(e.target.value)}
							required
						/>
					</div>
				</div>
				<div className="field">
					<label className="label">Агуулга</label>
					<div className="control">
						<CKEditor data={content} onChange={(evt) => setContent(evt.editor.getData())} />
					</div>
				</div>
				<div className="is-flex" style={{ justifyContent: "space-between" }}>
					<div />
					<div className="buttons">
						<button className="button">Болих</button>
						<button className="button is-success">Нэмэх</button>
					</div>
				</div>
			</form>
		</div>
	);
};

export default TabInput;
