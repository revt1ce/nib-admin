import React from "react";

const Message = ({ message, type }) => {
	return (
		<article className={`message ${type}`}>
			<div className="message-body is-size-7">{message}</div>
		</article>
	);
};

export default Message;
