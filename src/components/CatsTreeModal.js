import React from "react";
import Tree from "./Tree";
import { useQuery } from "react-apollo";
import { GET_CATS } from "../graphql/queries";

const CatsTreeModal = ({ type, language, modalHandler, onSelect, isFilter }) => {
	const { data } = useQuery(GET_CATS, {
		variables: {
			language: language,
		},
	});

	return (
		<div className="box">
			<div className="is-flex buttons" style={{ justifyContent: "space-between" }}>
				<div>
					{isFilter && (
						<button className="button" onClick={() => onSelect(null)}>
							Бүгдийг сонгох
						</button>
					)}
				</div>
				<button className="button is-danger is-light" onClick={() => modalHandler(false)}>
					Хаах
				</button>
			</div>
			<aside className="menu" style={{ maxHeight: "80vh", overflowY: "auto" }}>
				{data?.getCats &&
					data.getCats.map((cat) => {
						return (
							<ul className="menu-list" key={cat._id}>
								<Tree cat={cat} language={language} isModal={true} onSelect={onSelect} type={type} />
							</ul>
						);
					})}
			</aside>
		</div>
	);
};

export default CatsTreeModal;
