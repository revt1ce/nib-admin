import React, { useEffect, useState } from "react";

const ContactTabInput = ({ modalHandler, onSubmit, data }) => {
	const [title, setTitle] = useState("");
	const [number, setNumber] = useState("");

	const submit = (e) => {
		e.preventDefault();
		onSubmit({
			title: title,
			number: number,
		});
	};

	useEffect(() => {
		if (data) {
			setTitle(data.title);
			setNumber(data.number);
		} else {
			setTitle("");
			setNumber("");
		}
	}, [data]);
	return (
		<div className="box container">
			<div className="is-flex buttons" style={{ justifyContent: "space-between" }}>
				<div />
				<button
					className="button is-danger is-light"
					onClick={() => {
						modalHandler(false);
					}}
				>
					Буцах
				</button>
			</div>
			<form onSubmit={submit}>
				<div className="field">
					<label className="label">Салбарын нэр</label>
					<div className="control">
						<input
							className="input "
							type="text"
							value={title}
							onChange={(e) => setTitle(e.target.value)}
							required
						/>
					</div>
				</div>
				<div className="field">
					<label className="label">Утасны дугаар</label>
					<div className="control">
						<input
							className="input "
							type="text"
							value={number}
							onChange={(e) => setNumber(e.target.value)}
							required
						/>
					</div>
				</div>
				<div className="is-flex" style={{ justifyContent: "space-between" }}>
					<div />
					<div className="buttons">
						<button className="button is-success">Нэмэх</button>
					</div>
				</div>
			</form>
		</div>
	);
};

export default ContactTabInput;
