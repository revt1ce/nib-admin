import React from "react";

const LayoutsModal = ({ modalHandler, onSelect }) => {
	const layouts = [1, 2, 3];
	return (
		<div className="box container" style={{ maxWidth: 900 }}>
			<div className="is-flex" style={{ justifyContent: "space-between" }}>
				<div />
				<button className="button is-danger is-light" onClick={() => modalHandler(false)}>
					Xaax
				</button>
			</div>
			<div className="is-flex">
				{layouts.map((layout) => {
					return (
						<figure
							key={layout}
							className="image"
							onClick={() => onSelect(layout)}
							style={{
								display: "flex",
								justifyContent: "center",
								alignItems: "center",
								cursor: "pointer",
								width: 200,
								height: 150,
								margin: 10,
								backgroundColor: "#ccc",
								boxShadow: "0 0 5px #000",
							}}
						>
							{layout}
						</figure>
					);
				})}
			</div>
		</div>
	);
};

export default LayoutsModal;
