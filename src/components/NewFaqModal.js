import React, { useEffect, useState } from "react";
import { useMutation } from "react-apollo";
import { CREATE_FAQ, UPDATE_FAQ } from "../graphql/mutations";
import { GET_FAQ } from "../graphql/queries";
import Message from "./Message";

const NewFaqModal = ({ modalHandler, faq }) => {
	const [question, setQuestion] = useState("");
	const [answer, setAnswer] = useState("");
	const [message, setMessage] = useState("");
	const [messageType, setMessageType] = useState("");
	const [showMessage, setShowMessage] = useState(false);
	const [createFaq, { loading: creating }] = useMutation(CREATE_FAQ, {
		onCompleted() {
			setMessage("Амжилттай нэмэгдлээ!");
			setMessageType("is-success");
			setShowMessage(true);
			setAnswer("");
			setQuestion("");
		},
		update(cache, { data: { createFaq } }) {
			const { getFaq } = cache.readQuery({
				query: GET_FAQ,
				variables: { language: "mn" },
			});

			cache.writeQuery({
				query: GET_FAQ,
				variables: { language: "mn" },
				data: { getFaq: [...getFaq, createFaq] },
			});
		},
	});
	const [updateFaq, { loading: updating }] = useMutation(UPDATE_FAQ, {
		onCompleted() {
			modalHandler(false);
		},
	});

	const submit = (e) => {
		e.preventDefault();
		if (faq) {
			updateFaq({
				variables: {
					_id: faq._id,
					question: question,
					answer: answer,
				},
			});
		} else {
			createFaq({
				variables: {
					question: question,
					answer: answer,
				},
			});
		}
	};
	useEffect(() => {
		if (showMessage) {
			setTimeout(() => {
				setShowMessage(false);
			}, [3000]);
		}
	}, [showMessage]);
	useEffect(() => {
		if (faq) {
			setQuestion(faq.question);
			setAnswer(faq.answer);
		} else {
			setQuestion("");
			setAnswer("");
		}
	}, [faq]);

	return (
		<div className="box container">
			<div className="is-flex buttons" style={{ justifyContent: "space-between" }}>
				<div />
				<span
					className="button is-danger is-light"
					onClick={() => {
						modalHandler(false);
					}}
				>
					Буцах
				</span>
			</div>
			{showMessage && <Message message={message} type={messageType} />}
			<form onSubmit={submit}>
				<div className="field">
					<label className="label">Асуулт</label>
					<div className="control">
						<input
							className="input"
							type="text"
							value={question}
							onChange={(e) => setQuestion(e.target.value)}
							required
						/>
					</div>
				</div>
				<div className="field">
					<label className="label">Хариулт</label>
					<div className="control">
						<textarea
							className="textarea"
							type="text"
							value={answer}
							onChange={(e) => setAnswer(e.target.value)}
							required
						/>
					</div>
				</div>
				<div className="buttons">
					<button className="button is-success" disabled={creating || updating}>
						Нэмэх
					</button>
				</div>
			</form>
		</div>
	);
};

export default NewFaqModal;
