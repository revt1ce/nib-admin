import React from "react";
import { useQuery } from "react-apollo";
import { GET_CATS } from "../graphql/queries";
import { Link } from "react-router-dom";
import BoxLoader from "./BoxLoader";

const Tree = ({ language, cat, isModal, onSelect, type }) => {
	const { data, loading } = useQuery(GET_CATS, {
		variables: {
			language: language,
			parentKey: cat.key,
			type: type,
		},
	});

	return (
		<li key={cat._id}>
			{loading && <BoxLoader />}
			{isModal ? (
				<Link className="is-size-7" style={{ cursor: "pointer" }} onClick={() => onSelect(cat)}>
					{cat.name === " " ? "Бөглөөгүй байна" : cat.name}
				</Link>
			) : (
				<Link to={`/cats/detail/${cat.key}`} className="is-size-7">
					{cat.name === " " ? "Бөглөөгүй байна" : cat.name}
				</Link>
			)}
			<ul>
				{data?.getCats &&
					data.getCats.map((cat) => (
						<Tree key={cat._id} language={language} cat={cat} isModal={isModal} onSelect={onSelect} />
					))}
			</ul>
		</li>
	);
};

export default Tree;
