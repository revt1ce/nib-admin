import React, { useState } from "react";
import ArticleTabInput from "./ArticleTabInput";
import Modal from "./Modal";

const ArticleTabItem = ({ value, onChange, onDelete }) => {
	const [open, setOpen] = useState(false);

	const handleSubmit = (data) => {
		setOpen(false);
		onChange({
			title: data.title,
			description: data.description,
			body: data.body,
			image: data.image,
		});
	};

	const modalHandler = () => {
		setOpen(!open);
	};
	return (
		<div
			className="control"
			style={{
				display: "flex",
				alignItems: "center",
				marginBottom: "1rem",
				borderBottom: "1px solid #ccc",
				paddingBottom: 10,
			}}
		>
			<Modal active={open} width={1200} modalHandler={modalHandler}>
				<ArticleTabInput modalHandler={modalHandler} onSubmit={handleSubmit} data={value} />
			</Modal>
			<div className="is-flex buttons">
				<span
					className="button"
					style={{ width: 300, height: 45, border: 0, outline: "0" }}
					onClick={() => setOpen(true)}
				>
					{value ? value.title : "Хоосон"}
				</span>

				<span
					className="button is-small is-danger"
					style={{ width: 150, height: 45 }}
					onClick={() => onDelete()}
				>
					<i className="fa fa-recycle" style={{ marginRight: 10 }}></i>Устгах
				</span>
			</div>
		</div>
	);
};

export default ArticleTabItem;
