import React, { useState } from "react";
import { GET_VALUABLES_KEY } from "../graphql/queries";
import BoxLoader from "./BoxLoader";
import { useQuery } from "react-apollo";
import Modal from "./Modal";
import NewValuableModal from "./NewValuableModal";

const ValuableDetailModal = ({ modalHandler, valuable }) => {
	const { data, loading } = useQuery(GET_VALUABLES_KEY, {
		variables: {
			key: valuable?.key,
		},
	});
	const [isNew, setIsNew] = useState(false);
	const [selectedValuable, setSelectedValuable] = useState(null);

	const newModalHandler = (value) => {
		setIsNew(value);
		setSelectedValuable(null);
	};
	return (
		<div className="box container">
			<Modal active={isNew} modalHandler={newModalHandler}>
				<NewValuableModal modalHandler={newModalHandler} valuable={selectedValuable} />
			</Modal>
			{loading && <BoxLoader />}
			<div className="buttons is-flex" style={{ justifyContent: "space-between" }}>
				<div />
				<div>
					<button className="button is-danger is-light" onClick={() => modalHandler(false)}>
						Буцах
					</button>
				</div>
			</div>
			<table className="table is-fullwidth is-bordered">
				<thead>
					<tr>
						<th>Нэр</th>
						<th>Хэл</th>
						<th style={{ width: 1 }}></th>
					</tr>
				</thead>
				<tbody>
					{data?.getValuablesKey &&
						data.getValuablesKey.map((valuable) => {
							return (
								<tr key={valuable._id}>
									<td>{valuable.name === " " ? "Бөглөөгүй байна" : valuable.name}</td>
									<td>{valuable.language}</td>
									<td>
										<span className="is-flex">
											<button
												style={{ marginLeft: 10 }}
												className={
													valuable.name === " "
														? "button is-small is-danger is-light"
														: "button is-small"
												}
												onClick={() => {
													setSelectedValuable(valuable);
													setIsNew(true);
												}}
											>
												Засах
											</button>
										</span>
									</td>
								</tr>
							);
						})}
				</tbody>
			</table>
		</div>
	);
};

export default ValuableDetailModal;
