import React, { useState, useEffect } from "react";
import { useMutation } from "react-apollo";
import { CREATE_CAT } from "../graphql/mutations";
import { GET_CATS } from "../graphql/queries";
import Modal from "./Modal";
import Images from "../pages/Images";
import LayoutsModal from "./LayoutsModal";
import Message from "./Message";

const NewCatModal = ({ modalHandler, isRoot, parentKey }) => {
	const [name, setName] = useState("");
	const [slug, setSlug] = useState("");
	const [image, setImage] = useState(null);
	const [imageModal, setImageModal] = useState(false);
	const [layout, setLayout] = useState(null);
	const [layoutModal, setLayoutModal] = useState(false);
	const [message, setMessage] = useState("");
	const [showMessage, setShowMessage] = useState(false);
	const [messageType, setMessageType] = useState("");

	const [createCat, { loading }] = useMutation(CREATE_CAT, {
		onCompleted() {
			setName("");
			setSlug("");
			setImage(null);
			setMessageType("is-success");
			setShowMessage(true);
			setMessage("Амжилттай нэмэгдлээ!");
		},
		update(cache, { data: { createCat } }) {
			const { getCats } = cache.readQuery({
				query: GET_CATS,
				variables: { language: "mn" },
			});

			cache.writeQuery({
				query: GET_CATS,
				data: { getCats: [...getCats, createCat] },
				variables: { language: "mn" },
			});
		},
	});

	const [createSubCat, { loading: subLoading }] = useMutation(CREATE_CAT, {
		onCompleted() {
			setName("");
			setSlug("");
			setImage(null);
			setMessageType("is-success");
			setShowMessage(true);
			setMessage("Амжилттай нэмэгдлээ!");
		},
		update(cache, { data: { createCat } }) {
			const { getCats } = cache.readQuery({
				query: GET_CATS,
				variables: {
					parentKey: parentKey,
					language: "mn",
				},
			});

			cache.writeQuery({
				query: GET_CATS,
				data: { getCats: [...getCats, createCat] },
				variables: {
					parentKey: parentKey,
					language: "mn",
				},
			});
		},
	});

	const submit = (e) => {
		e.preventDefault();
		if (isRoot) {
			if (!layout) {
				setMessageType("is-danger");
				setMessage("Layout сонгоогүй байна!");
				setShowMessage(true);
			} else {
				createCat({
					variables: {
						name: name,
						slug: slug,
						image: image,
						layout: layout,
					},
				});
			}
		} else {
			createSubCat({
				variables: {
					parentKey: parentKey,
					name: name,
					slug: slug,
					image: image,
				},
			});
		}
	};

	const imageModalHandler = () => {
		setImageModal(!imageModal);
	};

	const layoutModalHandler = () => {
		setLayoutModal(!layoutModal);
	};

	const layoutSelectHandler = (layout) => {
		setLayoutModal(false);
		setLayout(layout);
	};

	const imageSelectHandler = (image) => {
		setImageModal(false);
		setImage({
			_id: image._id,
			path: image.path,
			height: image.height,
			width: image.width,
			type: image.type,
		});
	};

	useEffect(() => {
		if (showMessage) {
			setTimeout(() => {
				setShowMessage(false);
			}, [4000]);
		}
	}, [showMessage]);

	return (
		<div className="box container" style={{ maxWidth: 800 }}>
			<Modal active={imageModal} modalHandler={imageModalHandler}>
				<Images modalHandler={imageModalHandler} isModal={true} onSelect={imageSelectHandler} />
			</Modal>
			<Modal active={layoutModal} modalHandler={layoutModalHandler}>
				<LayoutsModal modalHandler={layoutModalHandler} onSelect={layoutSelectHandler} />
			</Modal>
			<div className="is-flex" style={{ justifyContent: "space-between" }}>
				<div />
				<button
					className="button is-danger is-light is-small"
					onClick={() => {
						modalHandler(false);
						setShowMessage(false);
						setImage(null);
					}}
				>
					Буцах
				</button>
			</div>
			<form onSubmit={submit}>
				<div className="field">
					<label className="label">Нэр</label>
					<div className="control">
						<input
							className="input "
							type="text"
							value={name}
							onChange={(e) => setName(e.target.value)}
							required
						/>
					</div>
				</div>
				<div className="field">
					<label className="label">Slug</label>
					<div className="control">
						<input
							className="input "
							type="text"
							value={slug}
							onChange={(e) => setSlug(e.target.value)}
							required
						/>
					</div>
				</div>
				<div className="is-flex" style={{ justifyContent: "space-between" }}>
					<div className="field" style={{ width: "50%" }}>
						<span className="button is-small" onClick={() => setImageModal(true)}>
							Зураг сонгох
						</span>
						{image ? (
							<div
								style={{
									display: "flex",
									justifyContent: "center",
									alignItems: "center",
									width: "90%",
									height: 200,
									marginTop: 10,
									color: "white",
									backgroundSize: "contain",
									backgroundPosition: "center",
									backgroundColor: "#ccc",
									backgroundRepeat: "no-repeat",
									backgroundImage: `url('http://localhost:8081/${image.path.slice(6)}')`,
									boxShadow: "0 0 5px #000",
								}}
							/>
						) : (
							<div
								style={{
									display: "flex",
									justifyContent: "center",
									alignItems: "center",
									width: "90%",
									height: 200,
									marginTop: 10,
									color: "white",
									backgroundColor: "#ccc",
									boxShadow: "0 0 5px #000",
								}}
							>
								зураггүй
							</div>
						)}
					</div>

					{isRoot && (
						<div className="field" style={{ width: "50%" }}>
							<span className="button is-small" onClick={() => setLayoutModal(true)}>
								Загвар сонгох
							</span>
							{layout ? (
								<div
									style={{
										display: "flex",
										justifyContent: "center",
										alignItems: "center",
										maxWidth: "90%",
										height: 200,
										marginTop: 10,
										color: "white",
										backgroundColor: "#ccc",
										boxShadow: "0 0 5px #000",
									}}
								>
									{layout}
								</div>
							) : (
								<div
									style={{
										display: "flex",
										justifyContent: "center",
										alignItems: "center",
										maxWidth: "90%",
										height: 200,
										marginTop: 10,
										color: "white",
										backgroundColor: "#ccc",
										boxShadow: "0 0 5px #000",
									}}
								>
									сонгох
								</div>
							)}
						</div>
					)}
				</div>
				<hr />
				{showMessage && <Message message={message} type={messageType} />}
				<button className="button is-success" disabled={loading || subLoading}>
					Нэмэх
				</button>
			</form>
		</div>
	);
};

export default NewCatModal;
