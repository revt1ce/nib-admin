import React, { useState } from "react";
import { useQuery } from "react-apollo";
import { GET_FAQ_KEY } from "../graphql/queries";
import BoxLoader from "./BoxLoader";
import Modal from "./Modal";
import NewFaqModal from "./NewFaqModal";

const FaqDetailModal = ({ modalHandler, faq }) => {
	const [isUpdate, setIsUpdate] = useState(false);
	const [selectedFaq, setSelectedFaq] = useState(null);
	const { data, loading } = useQuery(GET_FAQ_KEY, {
		variables: {
			key: faq ? faq.key : null,
		},
	});
	const updateModalHandler = () => {
		setSelectedFaq(null);
		setIsUpdate(!isUpdate);
	};
	return (
		<div className="box container">
			<Modal active={isUpdate} width={1000}>
				<NewFaqModal modalHandler={updateModalHandler} faq={selectedFaq} />
			</Modal>
			{loading && <BoxLoader />}
			<div className="buttons is-flex" style={{ justifyContent: "space-between" }}>
				<div />
				<div>
					<button className="button is-danger is-light" onClick={() => modalHandler(false)}>
						Буцах
					</button>
				</div>
			</div>
			<table className="table is-fullwidth is-bordered">
				<thead>
					<tr>
						<th>Асуулт</th>
						<th>Хариулт</th>
						<th>Хэл</th>
						<th style={{ width: 1 }}></th>
					</tr>
				</thead>
				<tbody>
					{data?.getFaqKey &&
						data.getFaqKey.map((faq) => {
							return (
								<tr key={faq._id}>
									<td>{faq.question}</td>
									<td>{faq.answer}</td>
									<td>{faq.language}</td>
									<td>
										<span className="is-flex">
											<button
												style={{ marginLeft: 10 }}
												className="button is-small is-danger is-light"
												onClick={() => {
													setSelectedFaq(faq);
													setIsUpdate(true);
												}}
											>
												Засах
											</button>
										</span>
									</td>
								</tr>
							);
						})}
				</tbody>
			</table>
		</div>
	);
};

export default FaqDetailModal;
