import React, { useState } from "react";
import Modal from "./Modal";
import SocialTabInput from "./SocialTabInput";

const SocialTabItem = ({ value, onChange, onDelete }) => {
	const [open, setOpen] = useState(false);

	const handleSubmit = (data) => {
		setOpen(false);
		onChange({
			name: data.name,
			url: data.url,
		});
	};

	const modalHandler = () => {
		setOpen(!open);
	};
	return (
		<div
			className="control"
			style={{
				display: "flex",
				alignItems: "center",
				marginBottom: "1rem",
				borderBottom: "1px solid #ccc",
				paddingBottom: 10,
			}}
		>
			<Modal active={open} modalHandler={modalHandler}>
				<SocialTabInput modalHandler={modalHandler} onSubmit={handleSubmit} data={value} />
			</Modal>
			<div className="is-flex buttons">
				<span
					className="button"
					style={{ width: 300, height: 45, border: 0, outline: "0" }}
					onClick={() => setOpen(true)}
				>
					{value ? value.name : "Хоосон"}
				</span>

				<span
					className="button is-small is-danger"
					style={{ width: 150, height: 45 }}
					onClick={() => onDelete()}
				>
					<i className="fa fa-recycle" style={{ marginRight: 10 }}></i>Устгах
				</span>
			</div>
		</div>
	);
};

export default SocialTabItem;
