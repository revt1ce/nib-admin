import React, { useState } from "react";
import { useQuery } from "react-apollo";
import { GET_HISTORIES_KEY } from "../graphql/queries";
import BoxLoader from "./BoxLoader";
import Modal from "./Modal";
import NewHistoryModal from "./NewHistoryModal";

const HistoryDetailModal = ({ modalHandler, history }) => {
	const [isNew, setIsNew] = useState(false);
	const { data, loading } = useQuery(GET_HISTORIES_KEY, {
		variables: {
			key: history?.key,
		},
	});
	const [selectedHistory, setSelectedHistory] = useState(null);
	const newModalHandler = () => {
		setSelectedHistory(null);
		setIsNew(!isNew);
	};
	return (
		<div className="box container">
			<Modal active={isNew}>
				<NewHistoryModal modalHandler={newModalHandler} history={selectedHistory} />
			</Modal>
			{loading && <BoxLoader />}
			<div className="buttons is-flex" style={{ justifyContent: "space-between" }}>
				<div />
				<div>
					<button className="button is-danger is-light" onClick={() => modalHandler(false)}>
						Буцах
					</button>
				</div>
			</div>
			<table className="table is-fullwidth is-bordered">
				<thead>
					<tr>
						<th>Он</th>
						<th>Хэл</th>
						<th>Табууд</th>
						<th style={{ width: 1 }}></th>
					</tr>
				</thead>
				<tbody>
					{data?.getHistoriesKey &&
						data.getHistoriesKey.map((history) => {
							return (
								<tr key={history._id}>
									<td>{history.name === " " ? "Бөглөөгүй байна" : history.name}</td>
									<td>{history.language}</td>
									<td>{history.tabs.length}</td>
									<td>
										<span className="is-flex">
											<button
												style={{ marginLeft: 10 }}
												className={
													history.name === " "
														? "button is-small is-danger is-light"
														: "button is-small"
												}
												onClick={() => {
													setSelectedHistory(history);
													setIsNew(true);
												}}
											>
												Засах
											</button>
										</span>
									</td>
								</tr>
							);
						})}
				</tbody>
			</table>
		</div>
	);
};

export default HistoryDetailModal;
