import React, { useState, useEffect } from "react";
import ImageField from "./ImageField";

const GalleryItemInput = ({ modalHandler, onSubmit, data }) => {
	const [title, setTitle] = useState("");
	const [image, setImage] = useState(null);

	const submit = (e) => {
		e.preventDefault();
		onSubmit({
			title: title,
			image: image,
		});
	};

	const updateImage = (image) => {
		setImage(image);
	};

	useEffect(() => {
		if (data) {
			setTitle(data.title);
			setImage(data.image);
		}
	}, [data]);

	return (
		<div className="box container">
			<div className="is-flex buttons" style={{ justifyContent: "space-between" }}>
				<div />
				<button
					className="button is-danger is-light"
					onClick={() => {
						modalHandler(false);
					}}
				>
					Буцах
				</button>
			</div>
			<form onSubmit={submit}>
				<div className="field">
					<label className="label">Нэр</label>
					<div className="control">
						<input
							className="input "
							type="text"
							value={title}
							onChange={(e) => setTitle(e.target.value)}
							required
						/>
					</div>
				</div>
				<div className="field">
					<ImageField
						size={150}
						value={image}
						onChange={(image) => updateImage(image)}
						onDelete={() => updateImage(null)}
					/>
				</div>
				<button className="button is-success is-small">Нэмэх</button>
			</form>
		</div>
	);
};

export default GalleryItemInput;
