import React, { useEffect, useState } from "react";

const SocialTabInput = ({ modalHandler, onSubmit, data }) => {
	const [name, setName] = useState("");
	const [url, setUrl] = useState("");

	const submit = (e) => {
		e.preventDefault();
		onSubmit({
			name: name,
			url: url,
		});
	};

	useEffect(() => {
		if (data) {
			setName(data.name);
			setUrl(data.url);
		} else {
			setName("");
			setUrl("");
		}
	}, [data]);
	return (
		<div className="box container">
			<div className="is-flex buttons" style={{ justifyContent: "space-between" }}>
				<div />
				<button
					className="button is-danger is-light"
					onClick={() => {
						modalHandler(false);
					}}
				>
					Буцах
				</button>
			</div>
			<form onSubmit={submit}>
				<div className="field">
					<label className="label">Сошиал нэр</label>
					<div className="control">
						<input
							className="input "
							type="text"
							value={name}
							onChange={(e) => setName(e.target.value)}
							required
						/>
					</div>
				</div>
				<div className="field">
					<label className="label">Холбоoс</label>
					<div className="control">
						<input
							className="input "
							type="text"
							value={url}
							onChange={(e) => setUrl(e.target.value)}
							required
						/>
					</div>
				</div>
				<div className="is-flex" style={{ justifyContent: "space-between" }}>
					<div />
					<div className="buttons">
						<button className="button is-success">Нэмэх</button>
					</div>
				</div>
			</form>
		</div>
	);
};

export default SocialTabInput;
