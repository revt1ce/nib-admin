import React from "react";

const Loader = () => {
	return (
		<div className="has-text-centered">
			<span className={`button is-dark is-loading is-large`} style={{ backgroundColor: "transparent" }} />
		</div>
	);
};

export default Loader;
