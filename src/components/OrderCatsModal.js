import React, { useState } from "react";
import { useQuery, useMutation } from "react-apollo";
import { GET_LANGUAGES, GET_CATS } from "../graphql/queries";
import { ORDER_CAT } from "../graphql/mutations";
import BoxLoader from "./BoxLoader";
import { ReactSortable } from "react-sortablejs";

const OrderCatsModal = ({ modalHandler, parentKey }) => {
	const [language, setLanguage] = useState("mn");
	const [cats, setCats] = useState([]);

	const { data: languageData } = useQuery(GET_LANGUAGES);
	const { loading } = useQuery(GET_CATS, {
		variables: {
			language: language,
			parentKey: parentKey ? parentKey : null,
		},
		onCompleted(data) {
			setCats(data.getCats);
		},
	});

	const [orderCat] = useMutation(ORDER_CAT, {
		update(cache, { data: { orderCat } }) {
			if (orderCat) {
				cache.writeQuery({
					query: GET_CATS,
					variables: { language: cats[0].language },
					data: { getCats: cats },
				});
			}
		},
	});

	const orderHandler = () => {
		if (cats.length) {
			orderCat({
				variables: {
					params: cats.map((cat) => cat._id),
				},
			});
		}
	};

	if (loading) return <BoxLoader />;
	return (
		<div className="box container" style={{ maxWidth: 800 }}>
			<div className="is-flex buttons" style={{ justifyContent: "space-between" }}>
				<div className="select" style={{ marginRight: 10 }}>
					<select value={language} onChange={(e) => setLanguage(e.target.value)}>
						{languageData?.getLanguages.map((language) => {
							return (
								<option value={language.slug} key={language.slug}>
									{language.country}
								</option>
							);
						})}
					</select>
				</div>
				<button className="button is-danger is-light" onClick={() => modalHandler(false)}>
					Буцах
				</button>
			</div>
			<ReactSortable
				handle=".Handle"
				onEnd={orderHandler}
				list={cats}
				setList={setCats}
				animation={100}
				ghostClass="_ghost"
				style={{ maxHeight: 400, overflowY: "auto" }}
			>
				{cats &&
					cats.map((cat) => {
						return (
							<div className="box" key={cat._id} style={{ display: "flex", alignItems: "center" }}>
								<span
									className="Handle has-text-grey-light"
									style={{
										marginRight: "2rem",
										fontSize: "0.8rem",
										cursor: "grab",
									}}
								>
									<svg
										viewBox="0 0 320 512"
										style={{
											display: "inline-block",
											height: "1em",
											overflow: "visible",
											verticalAlign: "-0.125em",
										}}
									>
										<path
											fill="currentColor"
											d="M96 32H32C14.33 32 0 46.33 0 64v64c0 17.67 14.33 32 32 32h64c17.67 0 32-14.33 32-32V64c0-17.67-14.33-32-32-32zm0 160H32c-17.67 0-32 14.33-32 32v64c0 17.67 14.33 32 32 32h64c17.67 0 32-14.33 32-32v-64c0-17.67-14.33-32-32-32zm0 160H32c-17.67 0-32 14.33-32 32v64c0 17.67 14.33 32 32 32h64c17.67 0 32-14.33 32-32v-64c0-17.67-14.33-32-32-32zM288 32h-64c-17.67 0-32 14.33-32 32v64c0 17.67 14.33 32 32 32h64c17.67 0 32-14.33 32-32V64c0-17.67-14.33-32-32-32zm0 160h-64c-17.67 0-32 14.33-32 32v64c0 17.67 14.33 32 32 32h64c17.67 0 32-14.33 32-32v-64c0-17.67-14.33-32-32-32zm0 160h-64c-17.67 0-32 14.33-32 32v64c0 17.67 14.33 32 32 32h64c17.67 0 32-14.33 32-32v-64c0-17.67-14.33-32-32-32z"
										/>
									</svg>
								</span>
								{cat.name}
							</div>
						);
					})}
			</ReactSortable>
		</div>
	);
};

export default OrderCatsModal;
