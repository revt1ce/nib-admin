import React, { useState, useEffect } from "react";
import CKEditor from "ckeditor4-react";
import ImageField from "./ImageField";
import { useMutation } from "react-apollo";
import { CREATE_VALUABLE, UPDATE_VALUABLE } from "../graphql/mutations";
import Message from "./Message";
import { GET_VALUABLES } from "../graphql/queries";

const NewValuableModal = ({ modalHandler, valuable }) => {
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [image, setImage] = useState(null);

	const [message, setMessage] = useState("");
	const [messageType, setMessageType] = useState("");
	const [showMessage, setShowMessage] = useState(false);

	const [updateValuable] = useMutation(UPDATE_VALUABLE, {
		onCompleted() {
			setMessage("Амжилттай хадгалагдлаа!");
			setMessageType("is-success");
			setShowMessage(true);
		},
	});

	const [createValuable, { loading }] = useMutation(CREATE_VALUABLE, {
		onCompleted() {
			setName("");
			setDescription("");
			setImage("");
			setMessage("Амжилттай нэмэгдлээ!");
			setMessageType("is-success");
			setShowMessage(true);
		},
		update(cache, { data: { createValuable } }) {
			const { getValuables } = cache.readQuery({
				query: GET_VALUABLES,
				variables: { language: "mn" },
			});

			cache.writeQuery({
				query: GET_VALUABLES,
				variables: { language: "mn" },
				data: { getValuables: [...getValuables, createValuable] },
			});
		},
	});

	const submit = () => {
		if (valuable) {
			updateValuable({
				variables: {
					_id: valuable._id,
					name: name,
					description: description,
					image: image
						? {
								_id: image._id,
								path: image.path,
								width: image.width,
								height: image.height,
								type: image.type,
						  }
						: null,
				},
			});
		} else {
			createValuable({
				variables: {
					name: name,
					description: description,
					image: image
						? {
								_id: image._id,
								path: image.path,
								width: image.width,
								height: image.height,
								type: image.type,
						  }
						: null,
				},
			});
		}
	};

	const updateImage = (image) => {
		setImage(image);
	};

	useEffect(() => {
		if (valuable) {
			setName(valuable.name);
			setDescription(valuable.description);
			setImage(valuable.image);
		} else {
			setName("");
			setDescription("");
			setImage("");
		}
	}, [valuable]);

	useEffect(() => {
		if (showMessage) {
			setTimeout(() => {
				setShowMessage(false);
			}, [2000]);
		}
	}, [showMessage]);

	return (
		<div className="box container">
			<div className="is-flex buttons" style={{ justifyContent: "space-between" }}>
				<div />
				<span
					className="button is-danger is-light"
					onClick={() => {
						modalHandler(false);
						setName("");
						setDescription("");
						setImage("");
					}}
				>
					Буцах
				</span>
			</div>
			{showMessage && <Message message={message} type={messageType} />}
			<div>
				<div className="field">
					<label className="label">Нэр</label>
					<div className="control">
						<input
							className="input "
							type="text"
							value={name}
							onChange={(e) => setName(e.target.value)}
							required
						/>
					</div>
				</div>
				<div className="field">
					<label className="label">Агуулга</label>
					<div className="control">
						<CKEditor data={description} onChange={(evt) => setDescription(evt.editor.getData())} />
					</div>
				</div>
				<div className="field">
					<ImageField
						size={150}
						value={image}
						onChange={(image) => updateImage(image)}
						onDelete={() => updateImage(null)}
					/>
				</div>
				<button className="button is-success is-small" disabled={loading} onClick={() => submit()}>
					Нэмэх
				</button>
			</div>
		</div>
	);
};

export default NewValuableModal;
