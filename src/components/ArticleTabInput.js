import React, { useState, useEffect } from "react";
import CKEditor from "ckeditor4-react";
import ImageField from "./ImageField";

const ArticleTabInput = ({ modalHandler, onSubmit, data }) => {
	const [title, setTitle] = useState("");
	const [description, setDescription] = useState("");
	const [body, setBody] = useState("");
	const [image, setImage] = useState(null);

	const submit = (e) => {
		e.preventDefault();
		onSubmit({
			title: title,
			description: description,
			body: body,
			image: image,
		});
	};
	const updateImage = (image) => {
		setImage(image);
	};
	useEffect(() => {
		if (!data) {
			setTitle("");
			setDescription("");
			setBody("");
			setImage(null);
		} else {
			setTitle(data.title);
			setDescription(data.description);
			setBody(data.body);
			setImage(data.image);
		}
	}, [data]);

	return (
		<div className="box container">
			<div className="is-flex buttons" style={{ justifyContent: "space-between" }}>
				<div />
				<button
					className="button is-danger is-light"
					onClick={() => {
						modalHandler(false);
					}}
				>
					Буцах
				</button>
			</div>
			<form onSubmit={submit}>
				<div className="field">
					<label className="label">Нэр</label>
					<div className="control">
						<input
							className="input "
							type="text"
							value={title}
							onChange={(e) => setTitle(e.target.value)}
							required
						/>
					</div>
				</div>
				<div className="field">
					<label className="label">Тайлбар</label>
					<div className="control">
						<input
							className="input "
							type="text"
							value={description}
							onChange={(e) => setDescription(e.target.value)}
							required
						/>
					</div>
				</div>
				<div className="field">
					<label className="label">Агуулга</label>
					<div className="control">
						<CKEditor data={body} onChange={(evt) => setBody(evt.editor.getData())} />
					</div>
				</div>
				<div className="field">
					<ImageField
						size={150}
						value={image}
						onChange={(image) => updateImage(image)}
						onDelete={() => updateImage(null)}
					/>
				</div>
				<div className="is-flex" style={{ justifyContent: "space-between" }}>
					<div />
					<div className="buttons">
						<button className="button">Болих</button>
						<button className="button is-success">Нэмэх</button>
					</div>
				</div>
			</form>
		</div>
	);
};

export default ArticleTabInput;
