import React from "react";

const AcceptModal = ({ message, modalHandler, yes }) => {
	return (
		<div className="box container" style={{ maxWidth: 400 }}>
			<p className="is-size-6">{message}</p>
			<div className="is-flex buttons" style={{ marginTop: "2rem" }}>
				<button
					className="button is-success"
					onClick={() => {
						yes();
						modalHandler(false);
					}}
				>
					Тийм
				</button>
				<button className="button is-danger is-light" onClick={() => modalHandler(false)}>
					Үгүй
				</button>
			</div>
		</div>
	);
};

export default AcceptModal;
