import React from "react";

const ComponentTypeModal = ({ modalHandler, onSubmit }) => {
	const types = [
		{
			title: "type1",
			image: "/type1.png",
		},
		{
			title: "type2",
			image: "/type2.png",
		},
		{
			title: "type3",
			image: "/type3.png",
		},
		{
			title: "type4",
			image: "/type4.png",
		},
		{
			title: "type5",
			image: "/type5.png",
		},
		{
			title: "type6",
			image: "/type6.png",
		},
	];
	return (
		<div className="box">
			<div className="buttons is-flex" style={{ justifyContent: "space-between" }}>
				<div />
				<div>
					<button className="button is-danger is-light" onClick={() => modalHandler(false)}>
						Буцах
					</button>
				</div>
			</div>
			<div className="is-flex" style={{ flexWrap: "wrap" }}>
				{types.map((type, index) => {
					return (
						<div key={index} className="field" style={{ margin: "1.5%", width: "47%" }}>
							<figure>
								<img
									alt="compnentype"
									className="component__type__modal"
									src={type.image}
									style={{ width: "100%" }}
									onClick={() => {
										onSubmit(type.title);
										modalHandler(false);
									}}
								/>
							</figure>
						</div>
					);
				})}
			</div>
		</div>
	);
};

export default ComponentTypeModal;
