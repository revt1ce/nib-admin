import React, { useEffect, useState } from "react";
import { useMutation } from "react-apollo";
import { CREATE_HISTORY, UPDATE_HISTORY } from "../graphql/mutations";
import { GET_HISTORIES } from "../graphql/queries";
import TabItem from "./TabItem";

const NewHistoryModal = ({ modalHandler, history }) => {
	const [name, setName] = useState("");
	const [tabs, setTabs] = useState([null]);

	const [createHistory, { loading: creating }] = useMutation(CREATE_HISTORY, {
		variables: {
			name: name,
			tabs: tabs
				.filter((tab) => tab !== null)
				.map((oneTab) => {
					return {
						name: oneTab.title,
						description: oneTab.content,
					};
				}),
		},
		onCompleted() {
			setName("");
			setTabs([null]);
			modalHandler(false);
		},
		update(cache, { data: { createHistory } }) {
			const { getHistories } = cache.readQuery({
				query: GET_HISTORIES,
				variables: { language: "mn" },
			});

			cache.writeQuery({
				query: GET_HISTORIES,
				variables: { language: "mn" },
				data: { getHistories: [...getHistories, createHistory] },
			});
		},
	});
	const [updateHistory, { loading: updating }] = useMutation(UPDATE_HISTORY, {
		variables: {
			_id: history?._id,
			name: name,
			tabs: tabs
				.filter((tab) => tab !== null)
				.map((oneTab) => {
					return {
						name: oneTab.title,
						description: oneTab.content,
					};
				}),
		},
		onCompleted() {
			setName("");
			setTabs([null]);
			modalHandler(false);
		},
	});

	const addOneTab = () => {
		const newTabs = [...tabs, null];
		setTabs(newTabs);
	};
	const deleteTab = (index) => {
		if (tabs.length > 1) {
			tabs.splice(index, 1);
			setTabs([...tabs]);
		} else {
			updateTab(null, index);
		}
	};
	const updateTab = (tab, index) => {
		const newTabs = [...tabs];
		newTabs[index] = tab;
		setTabs(newTabs);
	};

	const submit = () => {
		if (history) {
			updateHistory();
		} else {
			createHistory();
		}
	};

	useEffect(() => {
		if (history) {
			setName(history.name);
			setTabs(
				history.tabs.map((tab) => {
					return {
						title: tab.name,
						content: tab.description,
					};
				})
			);
		} else {
			setName("");
			setTabs([null]);
		}
	}, [history]);
	return (
		<div className="box container" style={{ maxWidth: 800 }}>
			<div className="is-flex" style={{ justifyContent: "space-between" }}>
				<div />
				<button
					className="button is-danger is-light"
					onClick={() => {
						modalHandler(false);
						setName("");
						setTabs([null]);
					}}
				>
					Буцах
				</button>
			</div>
			<div>
				<div className="field">
					<label className="label">Он</label>
					<div className="control" style={{ maxWidth: 200 }}>
						<input
							placeholder="ОН"
							className="input "
							type="number"
							value={name}
							onChange={(e) => setName(e.target.value)}
							required
						/>
					</div>
				</div>
				<div className="has-background-white-ter">
					<div className="field " style={{ padding: 15 }}>
						<label className="label">Табууд</label>
						{tabs.map((tab, index) => {
							return (
								<TabItem
									key={index}
									value={tab}
									onDelete={() => deleteTab(index)}
									onChange={(data) => updateTab(data, index)}
								/>
							);
						})}
						<span
							className="button is-small is-uppercase"
							style={{
								width: "100%",
								backgroundColor: "transparent",
								border: "dashed 1px #48c774",
								height: 40,
								color: "#48c774",
							}}
							onClick={() => addOneTab()}
						>
							<i className="fa fa-plus" style={{ marginRight: 10 }}></i> Таб нэмэх
						</span>
					</div>
				</div>
			</div>
			<button
				style={{ marginTop: 10 }}
				className="button is-success"
				disabled={creating || updating}
				onClick={() => submit()}
			>
				Нэмэх
			</button>
		</div>
	);
};

export default NewHistoryModal;
