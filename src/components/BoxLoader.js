import React from "react";

const BoxLoader = () => {
	return (
		<div
			style={{
				display: "flex",
				zIndex: 1,
				justifyContent: "center",
				alignItems: "center",
				position: "absolute",
				top: 0,
				bottom: 0,
				right: 0,
				left: 0,
				background: "rgba(255,255,255,0.5)",
			}}
		>
			<span className={`button is-warning is-loading is-large`} style={{ backgroundColor: "transparent" }} />
		</div>
	);
};

export default BoxLoader;
