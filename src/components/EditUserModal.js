import React, { useState, useEffect } from "react";
import { useQuery, useMutation } from "react-apollo";
import { GET_ONE_USER, GET_ALL_CATS, GET_USERS } from "../graphql/queries";
import BoxLoader from "./BoxLoader";
import { UPDATE_USER, DELETE_USER } from "../graphql/mutations";
import Modal from "./Modal";
import AcceptModal from "./AcceptModal";
import { useHistory } from "react-router-dom";
import Message from "./Message";

const EditUserModal = ({ _id, modalHandler }) => {
	const history = useHistory();

	const [disabled, setDisabled] = useState(true);
	const [isDelete, setIsDelete] = useState(false);

	const [username, setUsername] = useState("");
	const [roles, setRoles] = useState([]);

	const [showMessage, setShowMessage] = useState(false);
	const [message, setMessage] = useState("");
	const [messageType, setMessageType] = useState("");
	const { data: catsData } = useQuery(GET_ALL_CATS);
	const { data, loading } = useQuery(GET_ONE_USER, {
		variables: {
			_id: _id,
		},
		onCompleted(data) {
			setUsername(data.getOneUser.username);
			setRoles(data.getOneUser.roles);
		},
	});
	const [updateUser] = useMutation(UPDATE_USER, {
		variables: {
			_id: _id,
			username: username,
			roles: roles,
		},
		onCompleted() {
			setMessageType("is-success");
			setShowMessage(true);
			setMessage("Амжилттай шинэчлэгдлээ!");
		},
		onError(error) {
			setMessageType("is-danger");
			setShowMessage(true);
			setMessage(error?.graphQLErrors[0]?.message);
			setDisabled(false);
		},
	});

	const [deleteUser] = useMutation(DELETE_USER, {
		variables: {
			_id: _id,
		},
		onCompleted() {
			modalHandler(false);
			setDisabled(true);
			setShowMessage(false);
			history.push("/users");
		},
		update(cache, { data: { deleteUser } }) {
			const { getUsers } = cache.readQuery({
				query: GET_USERS,
			});
			cache.writeQuery({
				query: GET_USERS,
				data: { getUsers: [...getUsers, deleteUser] },
			});
		},
	});

	const checkHandler = (e) => {
		if (e.target.checked) {
			setRoles([...roles, e.target.value]);
		} else {
			setRoles(roles.filter((role) => role !== e.target.value));
		}
	};

	const deleteModalHandler = () => {
		setIsDelete(!isDelete);
	};

	useEffect(() => {
		if (showMessage) {
			setTimeout(() => {
				setShowMessage(false);
			}, [4000]);
		}
	}, [showMessage]);

	useEffect(() => {
		setShowMessage(false);
	}, [username]);

	if (loading) return <BoxLoader />;
	return (
		<div className="box container" style={{ maxWidth: 1200 }}>
			<Modal active={isDelete} modalHandler={deleteModalHandler}>
				<AcceptModal message="Устгахыг зөвшөөрч байна уу?" modalHandler={deleteModalHandler} yes={deleteUser} />
			</Modal>
			<div className="is-flex buttons" style={{ justifyContent: "space-between" }}>
				<div />
				<button
					className="button is-danger is-light"
					onClick={() => {
						modalHandler(false);
						setDisabled(true);
						setShowMessage(false);
					}}
				>
					Буцах
				</button>
			</div>
			{showMessage && <Message message={message} type={messageType} />}
			<form>
				<div className="field">
					<label className="label">Хэрэглэгчийн нэр</label>
					<div className="control">
						<input
							className="input "
							type="text"
							placeholder="Хэрэглэгчийн нэр"
							value={username}
							onChange={(e) => setUsername(e.target.value)}
							disabled={disabled}
							required
						/>
					</div>
				</div>
				{disabled ? (
					<div className="field">
						<label className="label">Хэрэглэгчийн эрх</label>
						<ul style={{ marginLeft: "1rem" }}>
							{data?.getOneUser.roles.map((role, index) => {
								return (
									<li key={index} style={{ margin: 5 }} className="tag is-info">
										{role}
									</li>
								);
							})}
						</ul>
					</div>
				) : (
					<div className="field" style={{ overflowY: "auto" }}>
						<table className="table is-bordered is-fullwidth">
							<thead>
								<tr>
									<th>Цэс</th>
									<th>Create, Update</th>
									<th>Delete</th>
									<th>Only Read</th>
								</tr>
							</thead>
							<tbody>
								{catsData?.getAllCats &&
									catsData.getAllCats.map((cat) => {
										return (
											<tr key={cat._id}>
												<td>{cat.name}</td>
												<td>
													<label className="checkbox">
														<input
															type="checkbox"
															value={`${cat.name}_create_update`}
															onChange={(e) => checkHandler(e)}
															checked={roles.includes(`${cat.name}_create_update`)}
														/>
													</label>
												</td>
												<td>
													<label className="checkbox">
														<input
															type="checkbox"
															value={`${cat.name}_delete`}
															onChange={(e) => checkHandler(e)}
															checked={roles.includes(`${cat.name}_delete`)}
														/>
														{}
													</label>
												</td>
												<td>
													<label className="checkbox">
														<input
															type="checkbox"
															value={`${cat.name}_readonly`}
															onChange={(e) => checkHandler(e)}
															checked={roles.includes(`${cat.name}_readonly`)}
														/>
													</label>
												</td>
											</tr>
										);
									})}
							</tbody>
						</table>
					</div>
				)}
				<hr />
				<div className="buttons is-flex" style={{ justifyContent: "flex-end" }}>
					<span className="button has-background-danger-light" onClick={() => setIsDelete(true)}>
						<i className="fas fa-trash-alt has-text-danger" />
					</span>
					{!disabled && (
						<button
							className="button"
							disabled={disabled}
							onClick={() => {
								setDisabled(true);
								setUsername(data?.getOneUser.username);
							}}
						>
							Болих
						</button>
					)}
					<button className="button is-warning" disabled={!disabled} onClick={() => setDisabled(false)}>
						Засах
					</button>
					<button
						className="button is-success"
						disabled={disabled}
						onClick={() => {
							updateUser();
							setDisabled(true);
						}}
					>
						Хадгалах
					</button>
				</div>
			</form>
		</div>
	);
};

export default EditUserModal;
