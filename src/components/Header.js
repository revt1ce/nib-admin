import React, { useState } from "react";
import { Link } from "react-router-dom";
import { useQuery } from "react-apollo";
import { GET_CURRENT_USER } from "../graphql/queries";

const Header = () => {
	const [open, setOpen] = useState(false);

	const { data } = useQuery(GET_CURRENT_USER);

	return (
		<>
			<nav className="navbar is-fixed-top" style={{ backgroundColor: "#232880" }} role="navigation">
				<div className="container is-fluid">
					<div className="navbar-brand is-flex" style={{ alignItems: "center" }}>
						<img src="./logo.png" alt="" style={{ height: 25 }} />
						<Link to="/" className="navbar-item has-text-white">
							NI BANK DASHBOARD
						</Link>
						{!open && (
							<span role="button" className={`navbar-burger burger`} onClick={() => setOpen(true)}>
								<span aria-hidden="true"></span>
								<span aria-hidden="true"></span>
								<span aria-hidden="true"></span>
							</span>
						)}
					</div>
					<div className="navbar-menu">
						<div className="navbar-end">
							<div className="navbar-item has-dropdown is-hoverable">
								<span className="navbar-link is-capitalized navbar__dropdown">
									<i
										style={{
											borderRadius: "50%",
											border: "solid 1px #fff",
											padding: 5,
											marginRight: 10,
											fontSize: 10,
											color: "white",
										}}
										className="fa fa-user"
									/>
									<span className="has-text-white" style={{ paddingRight: 20 }}>
										{data?.me.username}
									</span>
								</span>
								<div className="navbar-dropdown">
									<a href="#profile" className="navbar-item">
										Профайл
									</a>
									<hr className="navbar-divider" />
									<a href="#sendbugs" className="navbar-item">
										Алдаа мэдэгдэх
									</a>
									<Link to="/logout" key="logout" className="navbar-item">
										Гарах
									</Link>
								</div>
							</div>
						</div>
					</div>
				</div>
			</nav>
			<div className={`modal ${open ? "is-active" : ""}`}>
				<div className="modal-background" style={{ backgroundColor: "rgba(10, 10, 10, 0.92)" }}></div>
				<div className="modal-content">
					<div key="logout" className="has-text-centered" style={{ marginBottom: "1rem" }}>
						<Link to="/logout" onClick={() => setOpen(false)} className="has-text-white">
							Гарах
						</Link>
					</div>
				</div>
				<button className="modal-close is-large" aria-label="close" onClick={() => setOpen(false)} />
			</div>
		</>
	);
};

export default Header;
