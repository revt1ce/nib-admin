import React, { useState } from "react";
import { useQuery } from "react-apollo";
import { GET_REPORTS_KEY } from "../graphql/queries";
import BoxLoader from "./BoxLoader";
import Modal from "./Modal";
import NewReportModal from "./NewReportModal";

const ReportDetailModal = ({ modalHandler, report }) => {
	const [selectedReport, setSelectedReport] = useState(null);
	const [isUpdate, setIsUpdate] = useState(false);
	const { data, loading } = useQuery(GET_REPORTS_KEY, {
		variables: {
			key: report?.key,
		},
	});
	const updateModalHandler = () => {
		setSelectedReport(null);
		setIsUpdate(!isUpdate);
	};

	return (
		<div className="box container">
			<Modal active={isUpdate}>
				<NewReportModal modalHandler={updateModalHandler} report={selectedReport} />
			</Modal>
			{loading && <BoxLoader />}
			<div className="buttons is-flex" style={{ justifyContent: "space-between" }}>
				<div />
				<div>
					<button className="button is-danger is-light" onClick={() => modalHandler(false)}>
						Буцах
					</button>
				</div>
			</div>
			<table className="table is-fullwidth is-bordered">
				<thead>
					<tr>
						<th>Тайлан нэр</th>
						<th>Хэл</th>
						<th style={{ width: 1 }}></th>
					</tr>
				</thead>
				<tbody>
					{data?.getReportsKey &&
						data.getReportsKey.map((report) => {
							return (
								<tr key={report._id}>
									<td>{report.title === " " ? "Бөглөөгүй байна" : report.title}</td>
									<td>{report.language}</td>
									<td>
										<span className="is-flex">
											<button
												style={{ marginLeft: 10 }}
												className={
													report.name === " "
														? "button is-small is-danger is-light"
														: "button is-small"
												}
												onClick={() => {
													setSelectedReport(report);
													setIsUpdate(true);
												}}
											>
												Засах
											</button>
										</span>
									</td>
								</tr>
							);
						})}
				</tbody>
			</table>
		</div>
	);
};

export default ReportDetailModal;
