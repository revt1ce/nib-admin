import CKEditor from "ckeditor4-react";
import React, { useEffect, useState } from "react";
import { useMutation } from "react-apollo";
import { CREATE_REPORT, UPDATE_REPORT } from "../graphql/mutations";
import { GET_REPORTS } from "../graphql/queries";
import CatsTreeModal from "./CatsTreeModal";
import Modal from "./Modal";

const NewReportModal = ({ modalHandler, report }) => {
	const [title, setTitle] = useState("");
	const [description, setDescription] = useState("");
	const [uri, setUri] = useState("");
	const [cat, setCat] = useState(null);
	const [pickCat, setPickCat] = useState(false);

	const [createReport] = useMutation(CREATE_REPORT, {
		onCompleted() {
			setTitle("");
			setDescription("");
			setUri("");
			setCat("");
			modalHandler(false);
		},
		update(cache, { data: { createReport } }) {
			const { getReports } = cache.readQuery({
				query: GET_REPORTS,
				variables: { language: "mn", catKey: null },
			});

			cache.writeQuery({
				query: GET_REPORTS,
				variables: { language: "mn", catKey: null },
				data: { getReports: [...getReports, createReport] },
			});
		},
	});
	const [updateReport] = useMutation(UPDATE_REPORT, {
		onCompleted() {
			modalHandler(false);
		},
	});

	const submit = () => {
		if (report) {
			updateReport({
				variables: {
					_id: report._id,
					title: title,
					description: description,
					uri: uri,
					catKey: cat ? cat.key : null,
				},
			});
		} else {
			createReport({
				variables: {
					title: title,
					description: description,
					uri: uri,
					catKey: cat ? cat.key : null,
				},
			});
		}
	};

	const catSelectHandler = (cat) => {
		setCat(cat);
		setPickCat(!pickCat);
	};
	const pickCatModalHandler = () => {
		setPickCat(!pickCat);
	};
	useEffect(() => {
		if (report) {
			setTitle(report.title);
			setDescription(report.description);
			setUri(report.uri);
			setCat(report.category);
		} else {
			setTitle("");
			setDescription("");
			setUri("");
			setCat("");
		}
	}, [report]);
	return (
		<div className="box container">
			<Modal active={pickCat}>
				<CatsTreeModal
					type="finance"
					language="mn"
					modalHandler={pickCatModalHandler}
					onSelect={catSelectHandler}
				/>
			</Modal>
			<div className="is-flex buttons" style={{ justifyContent: "space-between" }}>
				<div />
				<span
					className="button is-danger is-light"
					onClick={() => {
						setTitle("");
						setDescription("");
						setUri("");
						setCat("");
						modalHandler(false);
					}}
				>
					Буцах
				</span>
			</div>
			<div>
				<div className="field">
					<label className="label">Нэр</label>
					<div className="control">
						<input
							className="input "
							type="text"
							value={title}
							onChange={(e) => setTitle(e.target.value)}
							required
						/>
					</div>
				</div>
				<div className="field">
					<label className="label">Цэс</label>
					<div className="control is-flex">
						<input className="input" disabled={true} value={cat && cat.name} type="text" required />
						<span style={{ width: "20%" }} className="button" onClick={() => setPickCat(true)}>
							Сонгох
						</span>
					</div>
				</div>
				<div className="field">
					<label className="label">Агуулга</label>
					<div className="control">
						<CKEditor data={description} onChange={(evt) => setDescription(evt.editor.getData())} />
					</div>
				</div>
				<form encType="multipart/form-data" id="fileForm" style={{ padding: 0, border: 0, width: "100%" }}>
					<div className="file">
						<label className="file-label">
							<input multiple={false} className="file-input" type="file" name="files" />
							<span
								className="button is-small is-uppercase"
								style={{
									display: "inline-block",
									width: "100%",
									backgroundColor: "transparent",
									border: "dashed 1px #48c774",
									height: 40,
									color: "#48c774",
									padding: 10,
									margin: 0,
								}}
							>
								<i className="fal fa-upload" style={{ marginRight: 10 }}></i>Файл хуулах
							</span>
							<span class="file-name" style={{ width: "100%" }}>
								{uri ? uri : "No file"}
							</span>
						</label>
					</div>
				</form>
			</div>
			<div className="buttons is-flex" style={{ marginTop: 10, justifyContent: "space-between" }}>
				<div />
				<button className="button is-success" onClick={() => submit()}>
					Нэмэх
				</button>
			</div>
		</div>
	);
};

export default NewReportModal;
