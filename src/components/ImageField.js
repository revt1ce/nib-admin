import React, { useState } from "react";
import Modal from "./Modal";
import Images from "../pages/Images";

const ImageField = ({ value, onChange, onDelete, size }) => {
	const [open, setOpen] = useState(false);

	function handleSelect(image) {
		setOpen(false);
		onChange({
			_id: image._id,
			path: image.path,
			height: image.height,
			width: image.width,
			type: image.type,
		});
	}

	const modalHandler = () => {
		setOpen(!open);
	};

	return (
		<div
			className="control"
			style={{
				display: "flex",
				alignItems: "center",
				marginBottom: "1rem",
				borderBottom: "1px solid #ccc",
				paddingBottom: 10,
			}}
		>
			<Modal active={open} modalHandler={modalHandler}>
				<Images modalHandler={modalHandler} isModal={true} onSelect={handleSelect} />
			</Modal>
			{value ? (
				<div
					style={{
						backgroundSize: "cover",
						backgroundPosition: "80%",
						backgroundRepeat: "no-repeat",
						backgroundImage: `url('http://localhost:8081/${value.path.slice(6)}')`,
						backgroundColor: "#eee",
						width: size ? size : 100,
						height: size ? size : 100,
						border: "1px solid #ccc",
						borderRadius: 3,
						marginRight: "1rem",
						display: "flex",
						alignItems: "center",
						justifyContent: "center",
					}}
				/>
			) : (
				<div
					style={{
						backgroundColor: "#eee",
						width: size ? size : 100,
						height: size ? size : 100,
						border: "1px solid #ccc",
						borderRadius: 3,
						marginRight: "1rem",
						display: "flex",
						alignItems: "center",
						justifyContent: "center",
					}}
				>
					No image
				</div>
			)}

			<div
				style={{ display: "flex", justifyContent: "flex-start", flexDirection: "column", alignItems: "center" }}
			>
				<span
					className="button is-small is-info"
					style={{ marginBottom: "0.5rem", width: 150, height: 45 }}
					onClick={() => setOpen(true)}
				>
					<i className="fa fa-plus" style={{ marginRight: 10 }}></i>Зураг сонгох
				</span>
				<span
					className="button is-small is-danger"
					style={{ width: 150, height: 45 }}
					onClick={() => onDelete()}
				>
					<i className="fa fa-recycle" style={{ marginRight: 10 }}></i>Устгах
				</span>
			</div>
		</div>
	);
};

export default ImageField;
