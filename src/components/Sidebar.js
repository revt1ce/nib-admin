import React from "react";
import { NavLink } from "react-router-dom";

const Sidebar = () => {
	return (
		<aside className="menu" style={{ width: "100%", padding: 20, height: "100%" }}>
			<p className="menu-label">Хэрэглэгч</p>
			<ul className="menu-list">
				<li>
					<NavLink to="/users">Хэрэглэгчид</NavLink>
				</li>
				<li>
					<a href="/log">Лог</a>
				</li>
			</ul>
			<p className="menu-label">Цэс</p>
			<ul className="menu-list">
				<li>
					<NavLink to="/cats">Цэсүүд</NavLink>
				</li>
			</ul>
			<p className="menu-label">Нийтлэл</p>
			<ul className="menu-list">
				<li>
					<NavLink to="/posts">Нийтлэлүүд</NavLink>
				</li>
				<li>
					<NavLink to="/galleries">Галерей</NavLink>
				</li>
				<li>
					<NavLink to="/products">Бүтээгдэхүүн</NavLink>
				</li>
			</ul>
			<p className="menu-label">Банк</p>
			<ul className="menu-list">
				<li>
					<NavLink to="/introduction">Бидний тухай</NavLink>
				</li>
				<li>
					<NavLink to="/valuable">Үнэт зүйлс</NavLink>
				</li>
				<li>
					<NavLink to="/history">Түүх</NavLink>
				</li>
				<li>
					<NavLink to="/report">Санхүү</NavLink>
				</li>
				<li>
					<NavLink to="/article">Удирдлага</NavLink>
				</li>
				<li>
					<NavLink to="/currency">Ханш</NavLink>
				</li>
			</ul>
			<p className="menu-label">Нэмэлт</p>
			<ul className="menu-list">
				<li>
					<NavLink to="/faq">Түгээмэл асуулт</NavLink>
				</li>
				<li>
					<NavLink to="/capital">Борлуулах хөрөнгө</NavLink>
				</li>
				<li>
					<NavLink to="/branches">Салбарын мэдээлэл</NavLink>
				</li>
				<li>
					<NavLink to="/workplace">Ажлын байр</NavLink>
				</li>
				<li>
					<NavLink to="/tender">Тендер</NavLink>
				</li>
				<li>
					<NavLink to="/contact">Холбоо барих</NavLink>
				</li>
				<li>
					<NavLink to="/socials">Сошиал холбоос</NavLink>
				</li>
			</ul>
			<p className="menu-label">Контент</p>
			<ul className="menu-list">
				<li>
					<NavLink to="/images">Зураг</NavLink>
					<NavLink to="/videos">Видео</NavLink>
				</li>
			</ul>
		</aside>
	);
};

export default Sidebar;
