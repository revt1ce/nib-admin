import React, { useState } from "react";
import { useQuery, useMutation } from "react-apollo";
import { GET_ONE_CAT } from "../graphql/queries";
import BoxLoader from "./BoxLoader";
import { UPDATE_CAT } from "../graphql/mutations";
import Modal from "./Modal";
import Images from "../pages/Images";

const EditCatModal = ({ _id, modalHandler }) => {
	const [name, setName] = useState("");
	const [slug, setSlug] = useState("");
	const [image, setImage] = useState(null);
	const [imageModal, setImageModal] = useState(false);
	// const [layout, setLayout] = useState("");
	const [message, setMessage] = useState("");
	const [showMessage, setShowMessage] = useState(false);
	const { loading } = useQuery(GET_ONE_CAT, {
		variables: {
			_id: _id,
		},
		onCompleted(data) {
			setName(data.getOneCat.name);
			setSlug(data.getOneCat.slug);
			setImage(
				data.getOneCat.image && {
					_id: data.getOneCat.image._id,
					path: data.getOneCat.image.path,
					width: data.getOneCat.image.width,
					height: data.getOneCat.image.height,
				}
			);
		},
	});

	const [updateCat] = useMutation(UPDATE_CAT, {
		variables: {
			_id: _id,
			name: name,
			image: image,
			slug: slug,
		},
		onCompleted() {
			setMessage("Амжилттай шинэчлэгдлээ!");
			setShowMessage(true);
		},
	});

	const submit = (e) => {
		e.preventDefault();
		updateCat();
	};

	const imageModalHandler = () => {
		setImageModal(!imageModal);
	};

	const imageSelectHandler = (image) => {
		setImageModal(false);
		setImage({
			_id: image._id,
			path: image.path,
			height: image.height,
			width: image.width,
			type: image.type,
		});
	};

	if (loading) return <BoxLoader />;
	return (
		<div className="container box" style={{ maxWidth: 800 }}>
			<Modal active={imageModal} modalHandler={imageModalHandler}>
				<Images modalHandler={imageModalHandler} isModal={true} onSelect={imageSelectHandler} />
			</Modal>
			<div className="is-flex" style={{ justifyContent: "space-between" }}>
				<div />
				<button
					className="button is-danger is-light is-small"
					onClick={() => {
						modalHandler(false);
						setShowMessage(false);
					}}
				>
					Буцах
				</button>
			</div>
			<form onSubmit={submit}>
				<div className="field">
					<label className="label">Нэр</label>
					<div className="control">
						<input
							className="input "
							type="text"
							value={name}
							onChange={(e) => setName(e.target.value)}
							required
						/>
					</div>
				</div>
				<div className="field">
					<label className="label">Slug</label>
					<div className="control">
						<input
							className="input "
							type="text"
							value={slug}
							onChange={(e) => setSlug(e.target.value)}
							required
						/>
					</div>
				</div>
				<div className="field">
					<span className="button is-small" onClick={() => setImageModal(true)}>
						Зураг сонгох
					</span>
					{image ? (
						<div
							style={{
								display: "flex",
								justifyContent: "center",
								alignItems: "center",
								width: "100%",
								height: 200,
								marginTop: 10,
								color: "white",
								backgroundSize: "contain",
								backgroundPosition: "center",
								backgroundColor: "#ccc",
								backgroundRepeat: "no-repeat",
								backgroundImage: `url('http://localhost:8081/${image.path.slice(6)}')`,
								boxShadow: "0 0 5px #000",
							}}
						/>
					) : (
						<div
							style={{
								display: "flex",
								justifyContent: "center",
								alignItems: "center",
								width: "100%",
								height: 200,
								marginTop: 10,
								color: "white",
								backgroundColor: "#ccc",
								boxShadow: "0 0 5px #000",
							}}
						>
							зураггүй
						</div>
					)}
				</div>
				{showMessage && (
					<article className="message is-success">
						<div className="message-body is-size-7">{message}</div>
					</article>
				)}
				<button className="button is-success">Хадгалах</button>
			</form>
		</div>
	);
};

export default EditCatModal;
