import React, { useState, useEffect } from "react";
import { useMutation, useQuery } from "react-apollo";
import { REGISTER } from "../graphql/mutations";
import { GET_USERS, GET_CATS } from "../graphql/queries";
import Message from "./Message";

const NewUserModal = ({ modalHandler }) => {
	const [username, setUsername] = useState("");
	const [password, setPassword] = useState("");
	const [roles, setRoles] = useState([]);
	const [message, setMessage] = useState("");
	const [showMessage, setShowMessage] = useState(false);
	const [messageType, setMessageType] = useState();

	const { data } = useQuery(GET_CATS, {
		variables: {
			language: "mn",
		},
	});

	const [register, { loading }] = useMutation(REGISTER, {
		variables: {
			username: username,
			password: password,
			roles: roles,
		},
		onError(error) {
			setMessageType("is-danger");
			setShowMessage(true);
			setMessage(error?.graphQLErrors[0]?.message);
		},
		onCompleted() {
			setUsername("");
			setPassword("");
			setRoles([]);
			setMessageType("is-success");
			setShowMessage(true);
			setMessage("Амжилттай нэмэгдлээ!");
		},
		update(cache, { data: { register } }) {
			const { getUsers } = cache.readQuery({
				query: GET_USERS,
			});

			cache.writeQuery({
				query: GET_USERS,
				data: { getUsers: [...getUsers, register] },
			});
		},
	});

	const submit = (e) => {
		e.preventDefault();
		register();
	};
	const checkHandler = (e) => {
		if (e.target.checked) {
			setRoles([...roles, e.target.value]);
		} else {
			setRoles(roles.filter((role) => role !== e.target.value));
		}
	};

	useEffect(() => {
		if (showMessage) {
			setTimeout(() => {
				setShowMessage(false);
			}, [4000]);
		}
	}, [showMessage]);

	useEffect(() => {
		setShowMessage(false);
	}, [username]);

	return (
		<div className="box container" style={{ maxWidth: 800 }}>
			<div className="is-flex" style={{ justifyContent: "space-between" }}>
				<div />
				<button
					className="button is-danger is-light"
					onClick={() => {
						modalHandler(false);
						setShowMessage(false);
					}}
				>
					Буцах
				</button>
			</div>
			{showMessage && <Message type={messageType} message={message} />}
			<form onSubmit={submit}>
				<div className="field">
					<label className="label">Хэрэглэгчийн нэр</label>
					<div className="control">
						<input
							className="input "
							type="text"
							value={username}
							onChange={(e) => setUsername(e.target.value)}
							required
						/>
					</div>
				</div>
				<div className="field">
					<label className="label">Нууц үг</label>
					<div className="control">
						<input
							className="input "
							type="password"
							value={password}
							onChange={(e) => setPassword(e.target.value)}
							required
						/>
					</div>
				</div>
				<div className="field" style={{ maxHeight: 500, overflowY: "auto" }}>
					<table className="table is-bordered is-fullwidth">
						<thead>
							<tr>
								<th>Цэс</th>
								<th>Create, Update</th>
								<th>Delete</th>
								<th>Only Read</th>
							</tr>
						</thead>
						<tbody>
							{data?.getCats &&
								data.getCats.map((cat) => {
									return (
										<tr key={cat._id}>
											<td>{cat.name}</td>
											<td>
												<label className="checkbox">
													<input
														type="checkbox"
														value={`${cat.name}_create_update`}
														onChange={(e) => checkHandler(e)}
														checked={roles.includes(`${cat.name}_create_update`)}
													/>
												</label>
											</td>
											<td>
												<label className="checkbox">
													<input
														type="checkbox"
														value={`${cat.name}_delete`}
														onChange={(e) => checkHandler(e)}
														checked={roles.includes(`${cat.name}_delete`)}
													/>
												</label>
											</td>
											<td>
												<label className="checkbox">
													<input
														type="checkbox"
														value={`${cat.name}_readonly`}
														onChange={(e) => checkHandler(e)}
														checked={roles.includes(`${cat.name}_readonly`)}
													/>
												</label>
											</td>
										</tr>
									);
								})}
						</tbody>
					</table>
				</div>
				<hr />
				<button className="button is-success" disabled={loading}>
					Нэмэх
				</button>
			</form>
		</div>
	);
};

export default NewUserModal;
