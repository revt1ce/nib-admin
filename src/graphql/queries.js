import { gql } from "apollo-boost";

export const GET_LANGUAGES = gql`
	query {
		getLanguages {
			country
			slug
		}
	}
`;

export const GET_CURRENT_USER = gql`
	query {
		me {
			_id
			username
			roles
		}
	}
`;
export const GET_USERS = gql`
	query {
		getUsers {
			_id
			username
			roles
		}
	}
`;
export const GET_ONE_USER = gql`
	query getOneUser($_id: ID!) {
		getOneUser(_id: $_id) {
			_id
			username
			roles
		}
	}
`;
export const GET_CATS = gql`
	query getCats($language: String, $parentKey: String, $type: String) {
		getCats(language: $language, parentKey: $parentKey, type: $type) {
			_id
			key
			name
			slug
			type
			parentKey
			layout
			image {
				_id
				path
				width
				height
			}
			language
		}
	}
`;
export const GET_CATS_KEY = gql`
	query getCatsKey($key: String) {
		getCatsKey(key: $key) {
			_id
			key
			name
			slug
			parentKey
			type
			layout
			image {
				_id
				path
				width
				height
			}
			language
		}
	}
`;
export const GET_ALL_CATS = gql`
	query getAllCats {
		getAllCats {
			_id
			key
			name
			slug
			parentKey
			layout
			image {
				_id
				path
				width
				height
			}
			language
		}
	}
`;

export const GET_ONE_CAT_KEY = gql`
	query getOneCatKey($key: String, $language: String) {
		getOneCatKey(key: $key, language: $language) {
			_id
			key
			name
			slug
			parentKey
			layout
			image {
				_id
				path
				width
				height
			}
			language
		}
	}
`;
export const GET_ONE_CAT = gql`
	query getOneCat($_id: ID!) {
		getOneCat(_id: $_id) {
			_id
			key
			name
			slug
			parentKey
			layout
			image {
				_id
				path
				width
				height
			}
			language
		}
	}
`;
export const GET_CONTENTS = gql`
	query getContents($from: String, $type: String) {
		getContents(from: $from, type: $type) {
			_id
			path
			width
			height
			type
		}
	}
`;
export const GET_POSTS = gql`
	query getPosts($from: String, $language: String, $catKey: String) {
		getPosts(from: $from, language: $language, catKey: $catKey) {
			_id
			key
			catKey
			category {
				_id
				name
			}
			title
			description
			body
			images {
				path
			}
			author
			language
		}
	}
`;
export const GET_POSTS_KEY = gql`
	query getPostsKey($key: String) {
		getPostsKey(key: $key) {
			_id
			key
			catKey
			category {
				_id
				name
			}
			title
			description
			body
			images {
				path
			}
			author
			language
		}
	}
`;
export const GET_ONE_POST = gql`
	query getOnePost($_id: ID!) {
		getOnePost(_id: $_id) {
			_id
			key
			catKey
			category {
				_id
				key
				name
			}
			title
			description
			body
			images {
				_id
				path
			}
			author
			language
		}
	}
`;
export const GET_GALLERIES = gql`
	query getGalleries($from: String, $catKey: String, $language: String) {
		getGalleries(from: $from, catKey: $catKey, language: $language) {
			_id
			key
			catKey
			category {
				_id
				key
				name
			}
			name
			images {
				title
				image {
					_id
					path
				}
			}
			author
			status
			language
		}
	}
`;
export const GET_GALLERIES_KEY = gql`
	query getGalleriesKey($key: String) {
		getGalleriesKey(key: $key) {
			_id
			key
			catKey
			category {
				_id
				key
				name
			}
			name
			images {
				title
				image {
					_id
					path
				}
			}
			author
			status
			language
		}
	}
`;
export const GET_ONE_GALLERY = gql`
	query getOneGallery($_id: ID!) {
		getOneGallery(_id: $_id) {
			_id
			key
			category {
				_id
				key
				name
			}
			name
			images {
				title
				image {
					_id
					path
				}
			}
			author
			language
		}
	}
`;
export const GET_PRODUCTS = gql`
	query getProducts($from: String, $catKey: String, $language: String) {
		getProducts(from: $from, catKey: $catKey, language: $language) {
			_id
			key
			catKey
			category {
				_id
				key
				name
			}
			name
			description
			tag
			image {
				_id
				path
				width
				height
				type
			}
			tabs {
				title
				content
			}
			author
			language
		}
	}
`;
export const GET_PRODUCTS_KEY = gql`
	query getProductsKey($key: String) {
		getProductsKey(key: $key) {
			_id
			key
			catKey
			category {
				_id
				name
			}
			name
			description
			tag
			image {
				_id
				path
			}
			tabs {
				title
				content
			}
			author
			language
		}
	}
`;
export const GET_ONE_PRODUCT = gql`
	query getOneProduct($_id: ID!) {
		getOneProduct(_id: $_id) {
			_id
			key
			catKey
			category {
				_id
				key
				name
			}
			name
			description
			tag
			image {
				_id
				path
				width
				height
				type
			}
			tabs {
				title
				content
			}
			author
			language
		}
	}
`;
export const GET_VALUABLES = gql`
	query getValuables($language: String) {
		getValuables(language: $language) {
			_id
			name
			description
			key
			image {
				_id
				path
			}
			language
		}
	}
`;
export const GET_ONE_VALUABLE = gql`
	query getOneValuable($_id: ID!) {
		getOneValuable(_id: $_id) {
			_id
			name
			description
			key
			image {
				_id
				path
			}
			language
		}
	}
`;
export const GET_VALUABLES_KEY = gql`
	query getValuablesKey($key: String) {
		getValuablesKey(key: $key) {
			_id
			name
			description
			key
			image {
				_id
				path
			}
			language
		}
	}
`;
export const GET_INTRODUCTION = gql`
	query getOneIntroduction($language: String) {
		getOneIntroduction(language: $language) {
			_id
			key
			about
			year
			branches
			goal
			vision
			language
		}
	}
`;
export const GET_HISTORIES = gql`
	query getHistories($from: String, $language: String) {
		getHistories(from: $from, language: $language) {
			_id
			key
			name
			tabs {
				name
				description
			}
			language
		}
	}
`;
export const GET_HISTORIES_KEY = gql`
	query getHistoriesKey($key: String) {
		getHistoriesKey(key: $key) {
			_id
			key
			name
			tabs {
				name
				description
			}
			language
		}
	}
`;
export const GET_ARTICLES = gql`
	query getArticles($language: String) {
		getArticles(language: $language) {
			_id
			key
			title
			description
			body
			image {
				_id
				path
			}
			internal
			external
			tabs {
				title
				description
				body
				image {
					_id
					path
				}
			}
			author
			type
			language
		}
	}
`;
export const GET_ARTICLES_KEY = gql`
	query getArticlesKey($key: String) {
		getArticlesKey(key: $key) {
			_id
			key
			title
			description
			body
			image {
				_id
				path
			}
			internal
			external
			tabs {
				title
				description
				body
				image {
					_id
					path
				}
			}
			author
			type
			language
		}
	}
`;
export const GET_ONE_ARTICLE = gql`
	query getOneArticle($_id: ID!) {
		getOneArticle(_id: $_id) {
			_id
			key
			title
			description
			body
			image {
				_id
				path
			}
			internal
			external
			tabs {
				title
				description
				body
				image {
					_id
					path
				}
			}
			author
			type
			language
		}
	}
`;
export const GET_REPORTS = gql`
	query getReports($from: String, $language: String, $catKey: String) {
		getReports(language: $language, from: $from, catKey: $catKey) {
			_id
			key
			catKey
			title
			category {
				_id
				name
			}
			description
			uri
			language
		}
	}
`;
export const GET_REPORTS_KEY = gql`
	query getReportsKey($key: String) {
		getReportsKey(key: $key) {
			_id
			key
			catKey
			category {
				_id
				name
			}
			title
			description
			uri
			language
		}
	}
`;
export const GET_FAQ = gql`
	query getFaq($from: String, $language: String) {
		getFaq(from: $from, language: $language) {
			_id
			key
			question
			answer
			language
		}
	}
`;
export const GET_FAQ_KEY = gql`
	query getFaqKey($key: String) {
		getFaqKey(key: $key) {
			_id
			key
			question
			answer
			language
		}
	}
`;
export const GET_TYPES = gql`
	query getTypes($language: String, $slug: String) {
		getTypes(language: $language, slug: $slug) {
			_id
			key
			title
			slug
			language
		}
	}
`;
export const GET_CAPITAL = gql`
	query getCapitals($from: String, $language: String, $typeKey: String) {
		getCapitals(from: $from, language: $language, typeKey: $typeKey) {
			_id
			key
			title
			size
			phonenumber
			address
			body
			images {
				_id
				path
			}
			author
			typeKey
			type {
				_id
				key
				language
				title
			}
			language
		}
	}
`;
export const GET_CAPITAL_KEY = gql`
	query getCapitalsKey($key: String) {
		getCapitalsKey(key: $key) {
			_id
			key
			title
			size
			phonenumber
			address
			body
			images {
				_id
				path
			}
			author
			typeKey
			type {
				_id
				title
			}
			language
		}
	}
`;

export const GET_ONE_CAPITAL = gql`
	query getOneCapital($_id: ID!) {
		getOneCapital(_id: $_id) {
			_id
			key
			title
			size
			phonenumber
			address
			body
			images {
				_id
				path
			}
			author
			typeKey
			type {
				_id
				key
				title
			}
			language
		}
	}
`;
export const GET_BRANCHES = gql`
	query getBranches($language: String, $typeKey: String) {
		getBranches(language: $language, typeKey: $typeKey) {
			_id
			key
			title
			description
			phonenumbers
			workdays
			weekend
			longitude
			latitude
			image {
				_id
				path
			}
			author
			typeKey
			type {
				_id
				key
				title
			}
			language
		}
	}
`;
export const GET_ONE_BRANCH = gql`
	query getOneBranch($_id: ID!) {
		getOneBranch(_id: $_id) {
			_id
			key
			title
			description
			phonenumbers
			workdays
			weekend
			longitude
			latitude
			image {
				_id
				path
			}
			author
			typeKey
			type {
				_id
				key
				title
			}
			language
		}
	}
`;
export const GET_BRANCHES_KEY = gql`
	query getBranchesKey($key: String) {
		getBranchesKey(key: $key) {
			_id
			key
			title
			description
			phonenumbers
			workdays
			weekend
			longitude
			latitude
			image {
				_id
				path
			}
			author
			typeKey
			type {
				_id
				key
				title
			}
			language
		}
	}
`;
export const GET_CONTACT = gql`
	query getContact($language: String) {
		getContact(language: $language) {
			_id
			key
			address
			longitude
			latitude
			phonenumber
			fax
			email
			workDays
			weekend
			branches {
				title
				number
			}
			language
		}
	}
`;
export const GET_JOBS = gql`
	query getJobs($from: String, $language: String, $type: String) {
		getJobs(from: $from, language: $language, type: $type) {
			_id
			key
			title
			startDate
			endDate
			jobNumber
			body
			contact
			ignore
			recieveEmail
			recieveAddress
			type
			author
			language
		}
	}
`;
export const GET_JOBS_KEY = gql`
	query getJobsKey($key: String) {
		getJobsKey(key: $key) {
			_id
			key
			title
			startDate
			endDate
			jobNumber
			body
			contact
			ignore
			recieveEmail
			recieveAddress
			type
			author
			language
		}
	}
`;
export const GET_ONE_JOB = gql`
	query getOneJob($_id: ID!) {
		getOneJob(_id: $_id) {
			_id
			key
			title
			startDate
			endDate
			jobNumber
			body
			contact
			ignore
			recieveEmail
			recieveAddress
			type
			author
			language
		}
	}
`;
export const GET_SOCIAL = gql`
	query getSocial($language: String) {
		getSocial(language: $language) {
			_id
			key
			phonenumber
			socials {
				name
				url
			}
			copyright
			language
		}
	}
`;
export const GET_LOGS = gql`
	query getLogs($from: String, $method: String, $user: String) {
		getLogs(from: $from, method: $method, user: $user) {
			_id
			username
			operation
			name
			model
			method
		}
	}
`;
