import { gql } from "apollo-boost";

export const LOGIN = gql`
	mutation Login($username: String, $password: String) {
		login(username: $username, password: $password) {
			token
			user {
				_id
				username
				roles
			}
		}
	}
`;
export const UPDATE_USER = gql`
	mutation updateUser($_id: ID!, $username: String, $roles: [String]) {
		updateUser(_id: $_id, username: $username, roles: $roles) {
			_id
			username
			roles
		}
	}
`;
export const DELETE_USER = gql`
	mutation deleteUser($_id: ID!) {
		deleteUser(_id: $_id)
	}
`;
export const REGISTER = gql`
	mutation register($username: String, $password: String, $roles: [String]) {
		register(username: $username, password: $password, roles: $roles) {
			_id
			username
			roles
		}
	}
`;
export const CREATE_CAT = gql`
	mutation createCat($name: String, $slug: String, $parentKey: String, $layout: Int, $image: ContentInput) {
		createCat(name: $name, slug: $slug, parentKey: $parentKey, layout: $layout, image: $image) {
			_id
			key
			name
			slug
			parentKey
			layout
			image {
				_id
				path
				width
				height
				type
			}
			language
		}
	}
`;
export const DELETE_CAT = gql`
	mutation deleteCat($key: String) {
		deleteCat(key: $key)
	}
`;
export const UPDATE_CAT = gql`
	mutation updateCat($_id: ID!, $name: String, $slug: String, $image: ContentInput) {
		updateCat(_id: $_id, name: $name, slug: $slug, image: $image) {
			_id
			key
			name
			slug
			parentKey
			layout
			image {
				_id
				path
				width
				height
			}
			language
		}
	}
`;
export const ORDER_CAT = gql`
	mutation orderCat($params: [String]) {
		orderCat(params: $params)
	}
`;
export const CREATE_CONTENT = gql`
	mutation createContent($path: String, $type: String) {
		createContent(path: $path, type: $type) {
			_id
			path
			width
			height
			type
		}
	}
`;
export const UPDATE_CONTENT = gql`
	mutation updateContent($_id: ID!, $path: String, $width: Int, $height: Int) {
		updateContent(_id: $_id, path: $path, width: $width, height: $height) {
			_id
			path
			width
			height
			type
		}
	}
`;
export const DELETE_CONTENT = gql`
	mutation deleteContent($_id: ID!) {
		deleteContent(_id: $_id)
	}
`;
export const CREATE_POST = gql`
	mutation createPost(
		$key: String
		$title: String
		$description: String
		$body: String
		$catKey: String
		$images: [ContentInput]
		$author: String
		$language: String
	) {
		createPost(
			key: $key
			title: $title
			description: $description
			body: $body
			catKey: $catKey
			images: $images
			author: $author
			language: $language
		) {
			_id
			key
			catKey
			category {
				_id
				name
			}
			title
			description
			body
			images {
				path
			}
			author
			language
		}
	}
`;
export const CREATE_POST_LANGUAGE = gql`
	mutation createPostLanguage($key: String, $catKey: String, $language: String, $author: String) {
		createPostLanguage(key: $key, catKey: $catKey, language: $language, author: $author) {
			_id
			key
			catKey
			category {
				_id
				name
			}
			title
			description
			body
			images {
				path
			}
			author
			language
		}
	}
`;

export const UPDATE_POST = gql`
	mutation updatePost(
		$_id: ID!
		$title: String
		$description: String
		$body: String
		$catKey: String
		$images: [ContentInput]
	) {
		updatePost(_id: $_id, title: $title, description: $description, body: $body, catKey: $catKey, images: $images) {
			_id
			key
			category {
				_id
				name
			}
			title
			description
			body
			images {
				path
			}
			author
			language
		}
	}
`;

export const ACTIVE_GALLERY = gql`
	mutation activeGallery($key: String) {
		activeGallery(key: $key)
	}
`;

export const DELETE_POST = gql`
	mutation deletePost($key: String) {
		deletePost(key: $key)
	}
`;

export const CREATE_GALLERY = gql`
	mutation createGallery($name: String, $catKey: String, $author: String) {
		createGallery(name: $name, catKey: $catKey, author: $author) {
			_id
			key
			catKey
			category {
				_id
				key
				name
			}
			name
			images {
				title
				image {
					_id
					path
				}
			}
			author
			status
			language
		}
	}
`;
export const DELETE_GALLERY = gql`
	mutation deleteGallery($key: String) {
		deleteGallery(key: $key)
	}
`;
export const CREATE_PRODUCT = gql`
	mutation createProduct(
		$catKey: String
		$name: String
		$description: String
		$tag: String
		$image: ContentInput
		$tabs: [TabInput]
		$author: String
	) {
		createProduct(
			catKey: $catKey
			name: $name
			description: $description
			tag: $tag
			image: $image
			tabs: $tabs
			author: $author
		) {
			_id
			key
			category {
				_id
				name
			}
			name
			description
			image {
				path
			}
			tabs {
				title
				content
			}
			author
			language
		}
	}
`;
export const UPDATE_PRODUCT = gql`
	mutation updateProduct(
		$_id: ID!
		$catKey: String
		$name: String
		$description: String
		$tag: String
		$image: ContentInput
		$tabs: [TabInput]
	) {
		updateProduct(
			_id: $_id
			catKey: $catKey
			name: $name
			description: $description
			tag: $tag
			image: $image
			tabs: $tabs
		) {
			_id
			key
			catKey
			category {
				_id
				name
			}
			name
			description
			tag
			image {
				_id
				path
			}
			tabs {
				title
				content
			}
			author
			language
		}
	}
`;

export const DELETE_PRODUCT = gql`
	mutation deleteProduct($key: String) {
		deleteProduct(key: $key)
	}
`;
export const UPDATE_GALLERY = gql`
	mutation updateGallery($_id: ID!, $name: String, $images: [GalleryItemInput]) {
		updateGallery(_id: $_id, name: $name, images: $images) {
			_id
			key
			catKey
			category {
				_id
				name
			}
			name
			images {
				title
				image {
					_id
					path
				}
			}
			author
			language
		}
	}
`;

export const CREATE_VALUABLE = gql`
	mutation createValuable($name: String, $description: String, $image: ContentInput) {
		createValuable(name: $name, description: $description, image: $image) {
			_id
			name
			description
			key
			image {
				_id
				path
			}
			language
		}
	}
`;
export const UPDATE_VALUABLE = gql`
	mutation updateValuable($_id: ID!, $name: String, $description: String, $image: ContentInput) {
		updateValuable(_id: $_id, name: $name, description: $description, image: $image) {
			_id
			name
			description
			key
			image {
				_id
				path
			}
		}
	}
`;
export const DELETE_VALUABLE = gql`
	mutation deleteValuable($key: String) {
		deleteValuable(key: $key)
	}
`;
export const UPDATE_INTRODUCTION = gql`
	mutation updateIntroduction($_id: ID!, $about: String, $year: Int, $branches: Int, $goal: String, $vision: String) {
		updateIntroduction(_id: $_id, about: $about, year: $year, branches: $branches, goal: $goal, vision: $vision) {
			_id
			key
			about
			year
			branches
			goal
			vision
			language
		}
	}
`;
export const CREATE_HISTORY = gql`
	mutation createHistory($name: String, $tabs: [HistoryTabInput]) {
		createHistory(name: $name, tabs: $tabs) {
			_id
			key
			name
			tabs {
				name
				description
			}
			language
		}
	}
`;
export const DELETE_HISTORY = gql`
	mutation deleteHistory($key: String) {
		deleteHistory(key: $key)
	}
`;
export const UPDATE_HISTORY = gql`
	mutation updateHistory($_id: ID!, $name: String, $tabs: [HistoryTabInput]) {
		updateHistory(_id: $_id, name: $name, tabs: $tabs) {
			_id
			key
			name
			language
			tabs {
				name
				description
			}
		}
	}
`;
export const CREATE_ARTICLE = gql`
	mutation createArticle(
		$title: String
		$description: String
		$body: String
		$internal: String
		$external: String
		$author: String
		$image: ContentInput
		$tabs: [ArticleTabInput]
		$type: String
	) {
		createArticle(
			title: $title
			description: $description
			body: $body
			internal: $internal
			external: $external
			author: $author
			image: $image
			tabs: $tabs
			type: $type
		) {
			_id
			key
			title
			description
			body
			image {
				_id
				path
			}
			internal
			external
			tabs {
				title
				description
				body
				image {
					_id
					path
				}
			}
			author
			type
			language
		}
	}
`;
export const DELETE_ARTICLE = gql`
	mutation deleteArticle($key: String) {
		deleteArticle(key: $key)
	}
`;
export const UPDATE_ARTICLE = gql`
	mutation updateArticle(
		$_id: ID!
		$title: String
		$description: String
		$body: String
		$internal: String
		$external: String
		$image: ContentInput
		$tabs: [ArticleTabInput]
	) {
		updateArticle(
			_id: $_id
			title: $title
			description: $description
			body: $body
			internal: $internal
			external: $external
			image: $image
			tabs: $tabs
		) {
			_id
			key
			title
			description
			body
			image {
				_id
				path
			}
			internal
			external
			tabs {
				title
				description
				body
				image {
					_id
					path
				}
			}
			author
			type
			language
		}
	}
`;
export const CREATE_REPORT = gql`
	mutation createReport($title: String, $description: String, $uri: String, $catKey: String) {
		createReport(title: $title, description: $description, uri: $uri, catKey: $catKey) {
			_id
			key
			catKey
			category {
				_id
				name
			}
			title
			description
			uri
			language
		}
	}
`;
export const DELETE_REPORT = gql`
	mutation deleteReport($key: String) {
		deleteReport(key: $key)
	}
`;
export const CREATE_FAQ = gql`
	mutation createFaq($question: String, $answer: String) {
		createFaq(question: $question, answer: $answer) {
			_id
			key
			question
			answer
			language
		}
	}
`;
export const UPDATE_FAQ = gql`
	mutation updateFaq($_id: ID!, $question: String, $answer: String) {
		updateFaq(_id: $_id, question: $question, answer: $answer) {
			_id
			key
			question
			answer
			language
		}
	}
`;
export const DELETE_FAQ = gql`
	mutation deleteFaq($key: String) {
		deleteFaq(key: $key)
	}
`;
export const UPDATE_REPORT = gql`
	mutation updateReport($_id: ID!, $title: String, $description: String, $uri: String, $catKey: String) {
		updateReport(_id: $_id, title: $title, description: $description, uri: $uri, catKey: $catKey) {
			_id
			key
			catKey
			category {
				_id
				name
			}
			title
			description
			uri
			language
		}
	}
`;
export const CREATE_CAPITAL = gql`
	mutation createCapital(
		$title: String
		$size: String
		$phonenumber: String
		$address: String
		$body: String
		$typeKey: String
		$images: [ContentInput]
		$author: String
	) {
		createCapital(
			title: $title
			size: $size
			phonenumber: $phonenumber
			address: $address
			body: $body
			typeKey: $typeKey
			images: $images
			author: $author
		) {
			_id
			key
			title
			size
			phonenumber
			address
			body
			images {
				_id
				path
			}
			author
			type {
				_id
				title
			}
			language
		}
	}
`;
export const UPDATE_CAPITAL = gql`
	mutation updateCapital(
		$_id: ID!
		$title: String
		$size: String
		$phonenumber: String
		$address: String
		$body: String
		$typeKey: String
		$images: [ContentInput]
		$author: String
	) {
		updateCapital(
			_id: $_id
			title: $title
			size: $size
			phonenumber: $phonenumber
			address: $address
			body: $body
			typeKey: $typeKey
			images: $images
			author: $author
		) {
			_id
			key
			title
			size
			phonenumber
			address
			body
			images {
				_id
				path
			}
			author
			typeKey
			type {
				_id
				title
			}
			language
		}
	}
`;
export const DELETE_CAPITAL = gql`
	mutation deleteCapital($key: String) {
		deleteCapital(key: $key)
	}
`;
export const DELETE_BRANCH = gql`
	mutation deleteBranch($key: String) {
		deleteBranch(key: $key)
	}
`;
export const CREATE_BRANCH = gql`
	mutation createBranch(
		$title: String
		$description: String
		$phonenumbers: [String]
		$workdays: String
		$weekend: String
		$longitude: String
		$latitude: String
		$image: ContentInput
		$author: String
		$typeKey: String
	) {
		createBranch(
			title: $title
			description: $description
			phonenumbers: $phonenumbers
			workdays: $workdays
			weekend: $weekend
			longitude: $longitude
			latitude: $latitude
			image: $image
			author: $author
			typeKey: $typeKey
		) {
			_id
			key
			title
			description
			phonenumbers
			workdays
			weekend
			longitude
			latitude
			image {
				_id
				path
			}
			author
			typeKey
			type {
				_id
				key
				title
			}
			language
		}
	}
`;
export const UPDATE_BRANCH = gql`
	mutation updateBranch(
		$_id: ID!
		$title: String
		$description: String
		$phonenumbers: [String]
		$workdays: String
		$weekend: String
		$longitude: String
		$latitude: String
		$image: ContentInput
		$author: String
		$typeKey: String
	) {
		updateBranch(
			_id: $_id
			title: $title
			description: $description
			phonenumbers: $phonenumbers
			workdays: $workdays
			weekend: $weekend
			longitude: $longitude
			latitude: $latitude
			image: $image
			author: $author
			typeKey: $typeKey
		) {
			_id
			key
			title
			description
			phonenumbers
			workdays
			weekend
			longitude
			latitude
			image {
				_id
				path
			}
			author
			typeKey
			type {
				_id
				key
				title
			}
			language
		}
	}
`;
export const UPDATE_CONTACT = gql`
	mutation updateContact(
		$_id: ID!
		$address: String
		$longitude: String
		$latitude: String
		$phonenumber: String
		$fax: String
		$email: String
		$workDays: String
		$weekend: String
		$branches: [ContactItemInput]
	) {
		updateContact(
			_id: $_id
			address: $address
			longitude: $longitude
			latitude: $latitude
			phonenumber: $phonenumber
			fax: $fax
			email: $email
			workDays: $workDays
			weekend: $weekend
			branches: $branches
		) {
			_id
			key
			address
			longitude
			latitude
			phonenumber
			fax
			email
			workDays
			weekend
			branches {
				title
				number
			}
			language
		}
	}
`;
export const DELETE_JOB = gql`
	mutation deleteJob($key: String) {
		deleteJob(key: $key)
	}
`;

export const CREATE_JOB = gql`
	mutation createJob(
		$title: String
		$startDate: String
		$endDate: String
		$jobNumber: String
		$body: String
		$contact: String
		$ignore: String
		$recieveEmail: String
		$recieveAddress: String
		$type: String
		$author: String
	) {
		createJob(
			title: $title
			startDate: $startDate
			endDate: $endDate
			jobNumber: $jobNumber
			body: $body
			contact: $contact
			ignore: $ignore
			recieveEmail: $recieveEmail
			recieveAddress: $recieveAddress
			type: $type
			author: $author
		) {
			_id
			key
			title
			startDate
			endDate
			jobNumber
			body
			contact
			ignore
			recieveEmail
			recieveAddress
			type
			author
			language
		}
	}
`;
export const UPDATE_JOB = gql`
	mutation updateJob(
		$_id: ID!
		$title: String
		$startDate: String
		$endDate: String
		$jobNumber: String
		$body: String
		$contact: String
		$ignore: String
		$recieveEmail: String
		$recieveAddress: String
		$type: String
		$author: String
	) {
		updateJob(
			_id: $_id
			title: $title
			startDate: $startDate
			endDate: $endDate
			jobNumber: $jobNumber
			body: $body
			contact: $contact
			ignore: $ignore
			recieveEmail: $recieveEmail
			recieveAddress: $recieveAddress
			type: $type
			author: $author
		) {
			_id
			key
			title
			startDate
			endDate
			jobNumber
			body
			contact
			ignore
			recieveEmail
			recieveAddress
			type
			author
			language
		}
	}
`;
export const UPDATE_SOCIAL = gql`
	mutation updateSocial($_id: ID!, $phonenumber: String, $socials: [LinkInput], $copyright: String) {
		updateSocial(_id: $_id, phonenumber: $phonenumber, socials: $socials, copyright: $copyright) {
			_id
			key
			phonenumber
			socials {
				name
				url
			}
			copyright
			language
		}
	}
`;
