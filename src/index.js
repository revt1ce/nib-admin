import React from "react";
import ReactDOM from "react-dom";
import "./index.scss";
import "bulma/css/bulma.css";
import App from "./App";
import * as serviceWorker from "./serviceWorker";
import ApolloClient, { InMemoryCache } from "apollo-boost";
import { ApolloProvider } from "react-apollo";
import { BrowserRouter as Router } from "react-router-dom";

const client = new ApolloClient({
	uri: "https://nib-backend.herokuapp.com/graphql",
	cache: new InMemoryCache(),
	request: (operation) => {
		const token = localStorage.getItem("Token");
		const language = localStorage.getItem("language");
		operation.setContext({
			headers: {
				authorization: token ? token : "",
				language: language ? language : "",
			},
		});
	},
});

ReactDOM.render(
	<ApolloProvider client={client}>
		<React.StrictMode>
			<Router>
				<App />
			</Router>
		</React.StrictMode>
	</ApolloProvider>,
	document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
