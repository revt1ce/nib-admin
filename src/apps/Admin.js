import React, { useEffect } from "react";
import { useQuery } from "react-apollo";
import { GET_CURRENT_USER } from "../graphql/queries";
import Loader from "../components/Loader";
import NotFound from "../pages/NotFound";
import Dashboard from "../pages/Dashboard";
import Login from "../pages/Login";
import { Switch, Route } from "react-router-dom";
import Sidebar from "../components/Sidebar";
import Header from "../components/Header";
import Logout from "../pages/Logout";
import Users from "../pages/Users";
import Cats from "../pages/Cats";
import CatDetail from "../pages/CatDetail";
import Images from "../pages/Images";
import Post from "../pages/Post";
import NewPost from "../pages/NewPost";
import Gallery from "../pages/Gallery";
import GalleryDetail from "../pages/GalleryDetail";
import PostDetail from "../pages/PostDetail";
import Product from "../pages/Product";
import NewProduct from "../pages/NewProduct";
import ProductDetail from "../pages/ProductDetail";
import EditGallery from "../pages/EditGallery";
import Valuable from "../pages/Valuable";
import Introduction from "../pages/Introduction";
import History from "../pages/History";
import Report from "../pages/Report";
import Article from "../pages/Article";
import NewArticle from "../pages/NewArticle";
import ArticleDetail from "../pages/ArticleDetail";
import Faq from "../pages/Faq";
import Capital from "../pages/Capital";
import NewCapital from "../pages/NewCapital";
import CapitalDetail from "../pages/CapitalDetail";
import Branches from "../pages/Branches";
import NewBranch from "../pages/NewBranch";
import BranchesDetail from "../pages/BranchesDetail";
import Contact from "../pages/Contact";
import Tender from "../pages/Tender";
import NewTender from "../pages/NewTender";
import TenderDetail from "../pages/TenderDetail";
import Workplace from "../pages/Workplace";
import NewWorkplace from "../pages/NewWorkplace";
import WorkplaceDetail from "../pages/WorkplaceDetail";
import Social from "../pages/Social";
import Currency from "../pages/Currency";
import Log from "../pages/Log";
import Videos from "../pages/Videos";

const AdminApp = () => {
	useEffect(() => {
		document.title = "NIBank";
	}, []);
	const { loading, data, error } = useQuery(GET_CURRENT_USER);
	if (loading) {
		return <Loader />;
	} else if (data) {
		return (
			<>
				<Header />
				<div className="is-flex" style={{ justifyContent: "flex-start", marginTop: 50, position: "relative" }}>
					<div className="" style={{ width: 250 }}>
						<Sidebar />
					</div>
					<div
						className="has-background-grey-lighter"
						style={{ minHeight: "calc(100vh - 52px)", padding: 15, width: "100%" }}
					>
						<Switch>
							<Route exact path="/" component={Dashboard} />
							<Route exact path="/users" component={Users} />

							<Route exact path="/log" component={Log} />
							<Route exact path="/valuable" component={Valuable} />
							<Route exact path="/introduction" component={Introduction} />
							<Route exact path="/history" component={History} />
							<Route exact path="/faq" component={Faq} />
							<Route exact path="/contact" component={Contact} />
							<Route exact path="/socials" component={Social} />
							<Route exact path="/currency" component={Currency} />

							<Route exact path="/workplace" component={Workplace} />
							<Route exact path="/workplace/detail/:key" component={WorkplaceDetail} />
							<Route exact path="/workplace/:id" component={NewWorkplace} />

							<Route exact path="/tender" component={Tender} />
							<Route exact path="/tender/detail/:key" component={TenderDetail} />
							<Route exact path="/tender/:id" component={NewTender} />

							<Route exact path="/capital" component={Capital} />
							<Route exact path="/capital/detail/:key" component={CapitalDetail} />
							<Route exact path="/capital/:id" component={NewCapital} />

							<Route exact path="/branches" component={Branches} />
							<Route exact path="/branches/:id" component={NewBranch} />
							<Route exact path="/branches/detail/:key" component={BranchesDetail} />

							<Route exact path="/report" component={Report} />

							<Route exact path="/cats" component={Cats} />
							<Route exact path="/cats/detail/:key" component={CatDetail} />

							<Route exact path="/posts" component={Post} />
							<Route exact path="/posts/detail/:key" component={PostDetail} />
							<Route exact path="/posts/:id" component={NewPost} />

							<Route exact path="/products" component={Product} />
							<Route exact path="/products/detail/:key" component={ProductDetail} />
							<Route exact path="/products/:id" component={NewProduct} />

							<Route exact path="/galleries" component={Gallery} />
							<Route exact path="/galleries/:id" component={EditGallery} />
							<Route exact path="/galleries/detail/:key" component={GalleryDetail} />

							<Route exact path="/article" component={Article} />
							<Route exact path="/article/detail/:key" component={ArticleDetail} />
							<Route exact path="/article/:id" component={NewArticle} />

							<Route exact path="/images" component={Images} />
							<Route exact path="/videos" component={Videos} />

							<Route exact path="/logout" component={Logout} />
							<Route component={NotFound} />
						</Switch>
					</div>
				</div>
			</>
		);
	} else if (error) {
		return <Login />;
	}
};

export default AdminApp;
