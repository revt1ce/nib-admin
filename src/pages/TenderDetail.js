import React, { useEffect } from "react";
import { useQuery } from "react-apollo";
import { Link, useParams } from "react-router-dom";
import BoxLoader from "../components/BoxLoader";
import { GET_JOBS_KEY } from "../graphql/queries";

const TenderDetail = () => {
	const { key } = useParams();

	const { data, loading } = useQuery(GET_JOBS_KEY, {
		variables: {
			key: key,
		},
	});

	useEffect(() => {
		if (data?.getJobsKey) {
			document.title = data?.getJobsKey[0].title;
		}
	}, [data]);

	return (
		<div className="box container" style={{ maxWidth: "100%" }}>
			{loading && <BoxLoader />}
			<div className="is-flex buttons" style={{ justifyContent: "space-between" }}>
				<div className="is-flex" style={{ alignItems: "center" }}>
					<Link to="/tender" className="button is-danger is-light">
						Буцах
					</Link>
				</div>
			</div>
			<table className="table is-fullwidth is-bordered">
				<thead>
					<tr>
						<th>Нэр</th>
						<th>Дуусах хугацаа</th>
						<th>Хэл</th>
						<th style={{ width: 1 }}></th>
					</tr>
				</thead>
				<tbody>
					{loading && <BoxLoader />}
					{data?.getJobsKey &&
						data.getJobsKey.map((job) => {
							return (
								<tr key={job._id}>
									<td>{job.title}</td>
									<td>{job.endDate}</td>
									<td>{job.language}</td>
									<td>
										<div className="is-flex">
											<Link
												style={{ marginLeft: 10 }}
												className={
													job.name === " "
														? "button is-small is-danger is-light"
														: "button is-small"
												}
												to={`/tender/${job._id}`}
											>
												Засах
											</Link>
										</div>
									</td>
								</tr>
							);
						})}
				</tbody>
			</table>
		</div>
	);
};

export default TenderDetail;
