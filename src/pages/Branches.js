import React, { useState } from "react";
import { useMutation, useQuery } from "react-apollo";
import { Link } from "react-router-dom";
import AcceptModal from "../components/AcceptModal";
import BoxLoader from "../components/BoxLoader";
import Modal from "../components/Modal";
import { DELETE_BRANCH } from "../graphql/mutations";
import { GET_LANGUAGES, GET_TYPES, GET_BRANCHES } from "../graphql/queries";

const Branches = () => {
	const allType = {
		title: "Бүгд",
		key: "",
	};
	const [language, setLanguage] = useState("mn");
	const [type, setType] = useState("");
	const [isDelete, setIsDelete] = useState(false);
	const [selectedBranch, setSelectedBranch] = useState(null);
	const [deleteBranch] = useMutation(DELETE_BRANCH, {
		variables: {
			key: selectedBranch?.key,
		},
		onCompleted() {
			refetch();
		},
	});
	const { data: branchTypes } = useQuery(GET_TYPES, {
		variables: {
			language: language,
			slug: "branch",
		},
	});
	const { data: languageData } = useQuery(GET_LANGUAGES);
	const { data, loading, refetch } = useQuery(GET_BRANCHES, {
		variables: {
			language: language,
			typeKey: type,
		},
	});

	const deleteModalHandler = () => {
		setIsDelete(!isDelete);
	};

	return (
		<div className="box container" style={{ maxWidth: "100%" }}>
			{loading && <BoxLoader />}
			<Modal active={isDelete}>
				<AcceptModal
					message="Устгахыг зөвшөөрч байна уу?"
					modalHandler={deleteModalHandler}
					yes={deleteBranch}
				/>
			</Modal>
			<div
				className="is-flex buttons"
				style={{
					justifyContent: "space-between",
					paddingBottom: 10,
					marginBottom: 20,
					borderBottom: "1px solid #ededed",
				}}
			>
				<h4 className="is-size-6 is-uppercase has-text-weight-bold">Салбарын мэдээлэл</h4>
				<div className="is-flex buttons">
					<div className="field">
						<div className="select" style={{ marginRight: 10 }}>
							<select value={language} onChange={(e) => setLanguage(e.target.value)}>
								{languageData?.getLanguages.map((language) => {
									return (
										<option value={language.slug} key={language.slug}>
											{language.country}
										</option>
									);
								})}
							</select>
						</div>
					</div>
					<div className="field">
						<div className="select" style={{ marginRight: 10 }}>
							<select value={type} onChange={(e) => setType(e.target.value)}>
								{branchTypes?.getTypes.map((type) => {
									return (
										<option value={type.key} key={type.key}>
											{type.title}
										</option>
									);
								})}
								<option value={allType.key}>{allType.title}</option>
							</select>
						</div>
					</div>

					<Link className="button is-success" to="/branches/new">
						Мэдээлэл оруулах
					</Link>
				</div>
			</div>
			<table className="table is-fullwidth is-bordered">
				<thead>
					<tr>
						<th>Нэр</th>
						<th>Төрөл</th>
						<th>Хэл</th>
						<th style={{ width: 1 }}></th>
					</tr>
				</thead>
				{!loading && !data?.getBranches.length && (
					<p className="is-size-6 has-text-danger" style={{ margin: 10 }}>
						Мэдээлэл байхгүй байна.
					</p>
				)}
				<tbody>
					{data?.getBranches &&
						data.getBranches.map((branch) => {
							return (
								<tr key={branch._id}>
									<td>{branch.title}</td>
									<td>{branch.type.title}</td>
									<td>{branch.language}</td>
									<td>
										<div className="is-flex">
											<button
												className="button is-small has-background-danger-light"
												onClick={() => {
													setSelectedBranch(branch);
													setIsDelete(true);
												}}
											>
												<i className="fas fa-trash-alt has-text-danger" />
											</button>
											<Link
												to={`/branches/detail/${branch.key}`}
												className="button is-small"
												style={{ marginLeft: 10 }}
											>
												Харах
											</Link>
										</div>
									</td>
								</tr>
							);
						})}
				</tbody>
			</table>
		</div>
	);
};

export default Branches;
