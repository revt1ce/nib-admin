import React, { useEffect, useState } from "react";
import { useQuery, useMutation } from "react-apollo";
import { GET_LANGUAGES, GET_PRODUCTS } from "../graphql/queries";
import { Link } from "react-router-dom";
import CatsTreeModal from "../components/CatsTreeModal";
import Modal from "../components/Modal";
import BoxLoader from "../components/BoxLoader";
import AcceptModal from "../components/AcceptModal";
import { DELETE_PRODUCT } from "../graphql/mutations";

const Product = () => {
	const [language, setLanguage] = useState("mn");
	const [cat, setCat] = useState(null);
	const [pickCat, setPickCat] = useState(false);
	const [isDelete, setIsDelete] = useState(false);
	const [hasMore, setHasMore] = useState(true);

	const [selectedProduct, setSelectedProduct] = useState();

	const { data: languageData } = useQuery(GET_LANGUAGES);

	const { data: productData, loading: productLoading, refetch, fetchMore } = useQuery(GET_PRODUCTS, {
		variables: {
			catKey: cat ? cat.key : null,
			language: language,
		},
	});

	useEffect(() => {
		if (productData) {
			if (productData.getProducts) {
				if (productData.getProducts.length < 10) {
					setHasMore(false);
				} else {
					setHasMore(true);
				}
			}
		}
	}, [productData]);

	const [deleteProduct] = useMutation(DELETE_PRODUCT, {
		variables: {
			key: selectedProduct,
		},
		onCompleted() {
			refetch();
		},
	});
	const loadMore = () => {
		fetchMore({
			variables: {
				from:
					productData.getProducts.length > 0
						? productData.getProducts[productData.getProducts.length - 1]._id
						: null,
				language: language,
				catKey: cat ? cat.key : null,
			},
			updateQuery: (prev, { fetchMoreResult }) => {
				if (!fetchMoreResult) {
					return prev;
				}
				if (fetchMoreResult.getProducts.length === 0) {
					setHasMore(false);
				}
				return Object.assign({}, prev, {
					getProducts: [...prev.getProducts, ...fetchMoreResult.getProducts],
				});
			},
		});
	};

	const deleteModalHandler = () => {
		setIsDelete(!isDelete);
	};

	const selectCatHandler = (cat) => {
		setCat(cat);
		modalHandler(!pickCat);
	};
	const modalHandler = () => {
		setPickCat(!pickCat);
	};
	return (
		<div className="box container" style={{ maxWidth: "100%" }}>
			{productLoading && <BoxLoader />}
			<Modal active={pickCat} modalHandler={modalHandler}>
				<CatsTreeModal
					language="mn"
					modalHandler={modalHandler}
					onSelect={selectCatHandler}
					isFilter={true}
					type="cat"
				/>
			</Modal>
			<Modal active={isDelete} modalHandler={deleteModalHandler}>
				<AcceptModal
					message="Устгахыг зөвшөөрч байна уу?"
					modalHandler={deleteModalHandler}
					yes={deleteProduct}
				/>
			</Modal>
			<div
				className="is-flex buttons"
				style={{
					justifyContent: "space-between",
					paddingBottom: 10,
					marginBottom: 20,
					borderBottom: "1px solid #ededed",
				}}
			>
				<h4 className="is-size-6 is-uppercase has-text-weight-bold">Бүтээгдэхүүн</h4>
				<div>
					<div className="select" style={{ marginRight: 10 }}>
						<select value={language} onChange={(e) => setLanguage(e.target.value)}>
							{languageData?.getLanguages.map((language) => {
								return (
									<option value={language.slug} key={language.slug}>
										{language.country}
									</option>
								);
							})}
						</select>
					</div>

					<button className="button" onClick={() => setPickCat(true)}>
						<span className="icon is-small">
							<i className="fal fa-filter"></i>
						</span>
						<span>{cat ? cat.name : "Бүх цэс"}</span>
					</button>

					<Link className="button is-success" to="/products/new">
						Бүтээгдэхүүн нэмэх
					</Link>
				</div>
			</div>
			<table className="table is-fullwidth is-bordered">
				<thead>
					<tr>
						<th>Бүтээгдэхүүний нэр</th>
						<th>Цэс</th>
						<th>Хэл</th>
						<th style={{ width: 1 }}></th>
					</tr>
				</thead>
				{!productLoading && !productData?.getProducts.length && (
					<p className="is-size-6 has-text-danger" style={{ margin: 10 }}>
						Бүтээгдэхүүн байхгүй байна.
					</p>
				)}
				<tbody>
					{productData?.getProducts &&
						productData.getProducts.map((product) => {
							return (
								<tr key={product._id}>
									<td>{product.name === " " ? "Бөглөөгүй байна" : product.name}</td>
									<td>{product.category.name === " " ? "Бөглөөгүй байна" : product.category.name}</td>
									<td>{product.language}</td>
									<td>
										<div className="is-flex">
											<button
												className="button is-small has-background-danger-light"
												onClick={() => {
													setSelectedProduct(product.key);
													setIsDelete(true);
												}}
											>
												<i className="fas fa-trash-alt has-text-danger" />
											</button>
											<Link
												to={`/products/detail/${product.key}`}
												className="button is-small"
												style={{ marginLeft: 10 }}
											>
												Засах
											</Link>
										</div>
									</td>
								</tr>
							);
						})}
				</tbody>
			</table>
			{hasMore && (
				<div className="has-text-centered">
					<button
						className="button is-small is-uppercase"
						style={{
							display: "inline-block",
							width: "100%",
							backgroundColor: "transparent",
							border: "dashed 1px #ccc",
							height: 50,
							color: "#000",
							padding: 10,
						}}
						onClick={loadMore}
					>
						Цааш үзэх
						<i className="fa fa-chevron-down" style={{ marginLeft: 10 }} />
					</button>
				</div>
			)}
		</div>
	);
};

export default Product;
