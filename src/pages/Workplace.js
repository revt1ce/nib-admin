import React, { useEffect, useState } from "react";
import { useMutation, useQuery } from "react-apollo";
import { Link } from "react-router-dom";
import AcceptModal from "../components/AcceptModal";
import BoxLoader from "../components/BoxLoader";
import Modal from "../components/Modal";
import { DELETE_JOB } from "../graphql/mutations";
import { GET_LANGUAGES, GET_JOBS } from "../graphql/queries";

const Workplace = () => {
	const [language, setLanguage] = useState("mn");
	const [hasMore, setHasMore] = useState(true);
	const [selectedJob, setSelectedJob] = useState(null);
	const [isDelete, setIsDelete] = useState(false);

	const { data: languageData } = useQuery(GET_LANGUAGES);
	const { data, loading, refetch, fetchMore } = useQuery(GET_JOBS, {
		variables: {
			language: language,
			type: "workplace",
		},
	});

	useEffect(() => {
		if (data?.getJobs?.length < 10) {
			setHasMore(false);
		} else {
			setHasMore(true);
		}
	}, [data]);

	const loadMore = () => {
		fetchMore({
			variables: {
				from: data.getJobs.length > 0 ? data.getJobs[data.getJobs.length - 1]._id : null,
				language: language,
				type: "workplace",
			},
			updateQuery: (prev, { fetchMoreResult }) => {
				if (!fetchMoreResult) {
					return prev;
				}
				if (fetchMoreResult.getJobs.length === 0) {
					setHasMore(false);
				}
				return Object.assign({}, prev, {
					getJobs: [...prev.getJobs, ...fetchMoreResult.getJobs],
				});
			},
		});
	};

	const [deleteJob] = useMutation(DELETE_JOB, {
		variables: {
			key: selectedJob?.key,
		},
		onCompleted() {
			refetch();
		},
	});
	const deleteModalHandler = () => {
		setIsDelete(!isDelete);
	};

	return (
		<div className="box container" style={{ maxWidth: "100%" }}>
			{loading && <BoxLoader />}
			<Modal active={isDelete}>
				<AcceptModal message="Устгахыг зөвшөөрч байна уу?" modalHandler={deleteModalHandler} yes={deleteJob} />
			</Modal>
			<div
				className="is-flex buttons"
				style={{
					justifyContent: "space-between",
					paddingBottom: 10,
					marginBottom: 20,
					borderBottom: "1px solid #ededed",
				}}
			>
				<h4 className="is-size-6 is-uppercase has-text-weight-bold">Нээлттэй ажлын байр</h4>
				<div>
					<div className="select" style={{ marginRight: 10 }}>
						<select value={language} onChange={(e) => setLanguage(e.target.value)}>
							{languageData?.getLanguages.map((language) => {
								return (
									<option value={language.slug} key={language.slug}>
										{language.country}
									</option>
								);
							})}
						</select>
					</div>
					<Link className="button is-success" to="/workplace/new">
						Мэдээлэл нэмэх
					</Link>
				</div>
			</div>
			<table className="table is-fullwidth is-bordered">
				<thead>
					<tr>
						<th>Нэр</th>
						<th>Дуусах хугацаа</th>
						<th>Хэл</th>
						<th style={{ width: 1 }}></th>
					</tr>
				</thead>
				{!loading && !data?.getJobs.length && (
					<p className="is-size-6 has-text-danger" style={{ margin: 10 }}>
						Мэдээлэл байхгүй байна.
					</p>
				)}
				<tbody>
					{data?.getJobs &&
						data.getJobs.map((job) => {
							return (
								<tr key={job._id}>
									<td>{job.title}</td>
									<td>{job.endDate}</td>
									<td>{job.language}</td>
									<td>
										<div className="is-flex">
											<button
												className="button is-small has-background-danger-light"
												onClick={() => {
													setSelectedJob(job);
													setIsDelete(true);
												}}
											>
												<i className="fas fa-trash-alt has-text-danger" />
											</button>
											<Link
												to={`/workplace/detail/${job.key}`}
												className="button is-small"
												style={{ marginLeft: 10 }}
											>
												Харах
											</Link>
										</div>
									</td>
								</tr>
							);
						})}
				</tbody>
			</table>
			{hasMore && (
				<div className="has-text-centered">
					<button
						className="button is-small is-uppercase"
						style={{
							display: "inline-block",
							width: "100%",
							backgroundColor: "transparent",
							border: "dashed 1px #ccc",
							height: 50,
							color: "#000",
							padding: 10,
						}}
						onClick={loadMore}
					>
						Цааш үзэх
						<i className="fa fa-chevron-down" style={{ marginLeft: 10 }} />
					</button>
				</div>
			)}
		</div>
	);
};

export default Workplace;
