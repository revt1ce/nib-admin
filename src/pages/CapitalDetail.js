import React, { useEffect } from "react";
import { useQuery } from "react-apollo";
import { Link, useParams } from "react-router-dom";
import BoxLoader from "../components/BoxLoader";
import { GET_CAPITAL_KEY } from "../graphql/queries";

const CapitalDetail = () => {
	const { key } = useParams();

	const { data, loading } = useQuery(GET_CAPITAL_KEY, {
		variables: {
			key: key,
		},
	});

	useEffect(() => {
		if (data?.getCapitalsKey) {
			document.title = data?.getCapitalsKey[0].title;
		}
	}, [data]);

	return (
		<div className="box container" style={{ maxWidth: "100%" }}>
			<div className="is-flex buttons" style={{ justifyContent: "space-between" }}>
				<div className="is-flex" style={{ alignItems: "center" }}>
					<Link to="/capital" className="button is-danger is-light">
						Буцах
					</Link>
				</div>
			</div>
			<table className="table is-fullwidth is-bordered">
				<thead>
					<tr>
						<th>Нэр</th>
						<th>Төрөл</th>
						<th>Хэл</th>
						<th style={{ width: 1 }}></th>
					</tr>
				</thead>
				<tbody>
					{loading && <BoxLoader />}
					{data?.getCapitalsKey &&
						data.getCapitalsKey.map((capital) => {
							return (
								<tr key={capital._id}>
									<td>{capital.title === " " ? "Бөглөөгүй байна" : capital.title}</td>
									<td>{capital.type.title}</td>
									<td>{capital.language}</td>
									<td>
										<div className="is-flex">
											<Link
												style={{ marginLeft: 10 }}
												className={
													capital.title === " "
														? "button is-small is-danger is-light"
														: "button is-small"
												}
												to={`/capital/${capital._id}`}
											>
												Засах
											</Link>
										</div>
									</td>
								</tr>
							);
						})}
				</tbody>
			</table>
		</div>
	);
};

export default CapitalDetail;
