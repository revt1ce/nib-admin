import React, { useState } from "react";
import { useMutation, useQuery } from "react-apollo";
import BoxLoader from "../components/BoxLoader";
import { UPDATE_INTRODUCTION } from "../graphql/mutations";
import { GET_INTRODUCTION, GET_LANGUAGES } from "../graphql/queries";

const Introduction = () => {
	const [language, setLanguage] = useState("mn");
	const [about, setAbout] = useState("");
	const [year, setYear] = useState(0);
	const [branches, setBranches] = useState(0);
	const [vision, setVision] = useState("");
	const [goal, setGoal] = useState("");
	const { data: languageData } = useQuery(GET_LANGUAGES);
	const { data, loading: fetching } = useQuery(GET_INTRODUCTION, {
		variables: {
			language: language,
		},
		onCompleted(data) {
			setAbout(data.getOneIntroduction.about);
			setYear(data.getOneIntroduction.year);
			setBranches(data.getOneIntroduction.branches);
			setVision(data.getOneIntroduction.vision);
			setGoal(data.getOneIntroduction.goal);
		},
	});

	const [updateIntroduction, { loading }] = useMutation(UPDATE_INTRODUCTION, {
		onCompleted() {
			alert("Амжилттай");
		},
	});

	const submit = (e) => {
		e.preventDefault();
		updateIntroduction({
			variables: {
				_id: data?.getOneIntroduction._id,
				about: about,
				year: parseInt(year),
				branches: parseInt(branches),
				vision: vision,
				goal: goal,
			},
		});
	};

	return (
		<div className="box container" style={{ maxWidth: "100%" }}>
			{fetching && <BoxLoader />}
			<div
				className="is-flex buttons"
				style={{
					justifyContent: "space-between",
					paddingBottom: 10,
					marginBottom: 20,
					borderBottom: "1px solid #ededed",
				}}
			>
				<h4 className="is-size-6 is-uppercase has-text-weight-bold">Бидний тухай</h4>
				<div className="select" style={{ marginRight: 10 }}>
					<select value={language} onChange={(e) => setLanguage(e.target.value)}>
						{languageData?.getLanguages.map((language) => {
							return (
								<option value={language.slug} key={language.slug}>
									{language.country}
								</option>
							);
						})}
					</select>
				</div>
			</div>
			<form onSubmit={submit}>
				<div className="field">
					<label className="label">Танилцуулга</label>
					<div className="control is-flex">
						<textarea
							className="textarea"
							value={about}
							required
							onChange={(e) => setAbout(e.target.value)}
						/>
					</div>
				</div>
				<div className="is-flex">
					<div className="field">
						<label className="label">Үүсгэн байгуулагдсан он</label>
						<div className="control is-flex">
							<input
								type="number"
								className="input"
								value={year}
								onChange={(e) => setYear(e.target.value)}
								required
							/>
						</div>
					</div>
					<div className="field" style={{ marginLeft: 20 }}>
						<label className="label">Салбарын тоо</label>
						<div className="control is-flex">
							<input
								type="number"
								className="input"
								value={branches}
								onChange={(e) => setBranches(e.target.value)}
								required
							/>
						</div>
					</div>
				</div>
				<div className="field">
					<label className="label">Алсын хараа</label>
					<div className="control is-flex">
						<textarea
							className="textarea"
							required
							value={vision}
							onChange={(e) => setVision(e.target.value)}
						/>
					</div>
				</div>
				<div className="field">
					<label className="label">Эрхэм зорилго</label>
					<div className="control is-flex">
						<textarea
							className="textarea"
							required
							value={goal}
							onChange={(e) => setGoal(e.target.value)}
						/>
					</div>
				</div>
				<button className="button is-success" disabled={loading}>
					Хадгалах
				</button>
			</form>
		</div>
	);
};

export default Introduction;
