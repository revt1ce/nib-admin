import React, { useState, useEffect } from "react";
import { useMutation } from "react-apollo";
import { LOGIN } from "../graphql/mutations";
import { GET_CURRENT_USER } from "../graphql/queries";
import { useHistory } from "react-router-dom";

const Login = () => {
	const history = useHistory();
	const [username, setUsername] = useState("");
	const [password, setPassword] = useState("");
	const [error, setError] = useState(false);
	const [errMessage, setErrMessage] = useState("");

	const [login, { loading }] = useMutation(LOGIN, {
		onCompleted({ login }) {
			localStorage.setItem("Token", login.token);

			setUsername("");
			setPassword("");

			history.push(`/`);
		},
		update(cache, { data }) {
			cache.writeQuery({
				query: GET_CURRENT_USER,
				data: { me: data.login.user },
			});
		},
		onError(error) {
			setUsername("");
			setPassword("");

			setError(true);
			setErrMessage(error?.graphQLErrors[0]?.message);
		},
	});

	const submit = (e) => {
		e.preventDefault();
		login({
			variables: {
				username: username,
				password: password,
			},
		});
	};
	useEffect(() => {
		document.title = "Нэвтрэх";
	}, []);
	return (
		<div className="is-flex" style={{ justifyContent: "center", alignItems: "center", height: "100vh" }}>
			<div className="container" style={{ maxWidth: 400 }}>
				<form onSubmit={submit}>
					<div className="box">
						<p
							className="is-size-6 has-text-centered has-text-weight-light has-text-link"
							style={{ padding: "10px 0" }}
						>
							<img src="./logo_blue.png" alt="login logo" />
							<br />
							<span style={{ color: "#282380" }}>NI BANK DASHBOARD</span>
						</p>
						<hr style={{ backgroundColor: "#ededed" }} />
						<div className="field">
							<label
								className="is-size-7 is-uppercase"
								style={{ marginBottom: 10, display: "inline-block", color: "#232880" }}
							>
								Хэрэглэгчийн нэр
							</label>
							<div className="control">
								<input
									className="input"
									style={{ border: "solid 1px #ededed", boxShadow: "none" }}
									type="text"
									placeholder="admin@example.com"
									required
									value={username}
									onChange={(e) => setUsername(e.target.value)}
								/>
							</div>
						</div>
						<div className="field">
							<label
								className="is-size-7 is-uppercase"
								style={{ marginBottom: 10, display: "inline-block", color: "#232880" }}
							>
								Нууц үг
							</label>
							<div className="control">
								<input
									className="input"
									style={{ border: "solid 1px #ededed", boxShadow: "none" }}
									type="password"
									placeholder="********"
									required
									value={password}
									onChange={(e) => setPassword(e.target.value)}
								/>
							</div>
						</div>
						{error && (
							<article className="message is-danger is-small">
								<div className="message-body">{errMessage}</div>
							</article>
						)}
						<button
							className="button is-fullwidth"
							style={{
								backgroundColor: "#232880",
								color: "#fff",
								border: 0,
								marginTop: 25,
								fontSize: 13,
							}}
							disabled={loading}
						>
							Нэвтрэх
						</button>
					</div>
				</form>
			</div>
		</div>
	);
};

export default Login;
