import React, { useState } from "react";
import { useQuery, useMutation } from "react-apollo";
import { GET_CATS_KEY, GET_CATS } from "../graphql/queries";
import { useParams, Link, useHistory } from "react-router-dom";
import BoxLoader from "../components/BoxLoader";
import Modal from "../components/Modal";
import NewCatModal from "../components/NewCatModal";
import AcceptModal from "../components/AcceptModal";
import { DELETE_CAT } from "../graphql/mutations";
import EditCatModal from "../components/EditCatModal";
import OrderCatsModal from "../components/OrderCatsModal";

const CatDetail = () => {
	const { key } = useParams();
	const history = useHistory();

	const [ordering, setOrdering] = useState(false);
	const [selectedCat, setSelectedCat] = useState("");
	const [isEdit, setIsEdit] = useState(false);
	const [isSub, setIsSub] = useState(false);
	const [isDelete, setIsDelete] = useState(false);

	const { data, loading } = useQuery(GET_CATS_KEY, {
		variables: {
			key: key,
		},
	});

	const [deleteCat] = useMutation(DELETE_CAT, {
		variables: {
			key: data?.getCatsKey[0].key,
		},
		onCompleted() {
			history.push("/cats");
		},
		update(cache, { data: { deleteCat } }) {
			const { getCats } = cache.readQuery({
				query: GET_CATS,
				variables: data?.getCatsKey[0].parentKey
					? { parentKey: data?.getCatsKey[0].parentKey, language: "mn" }
					: { language: "mn" },
			});

			cache.writeQuery({
				query: GET_CATS,
				data: { getCats: [...getCats, deleteCat] },
				variables: data?.getCatsKey[0].parentKey
					? { parentKey: data?.getCatsKey[0].parentKey, language: "mn" }
					: { language: "mn" },
			});
		},
	});
	const orderModalHandler = () => {
		setOrdering(!ordering);
	};

	const modalHandler = () => {
		setIsSub(!isSub);
	};
	const deleteModalHandler = () => {
		setIsDelete(!isDelete);
	};
	const editModalHandler = () => {
		setIsEdit(!isEdit);
	};

	return (
		<div className="container box" style={{ maxWidth: 1000 }}>
			<Modal active={isSub} modalHandler={modalHandler}>
				<NewCatModal isRoot={false} parentKey={data?.getCatsKey[0].key} modalHandler={modalHandler} />
			</Modal>
			<Modal active={isDelete} modalHandler={deleteModalHandler}>
				<AcceptModal
					message="Цэс устгавал устгасан цэстэй хамааралтай бүх контент устах болно. Утсгахдаа итгэлтэй байна уу?"
					modalHandler={deleteModalHandler}
					yes={deleteCat}
				/>
			</Modal>
			<Modal active={isEdit} modalHandler={editModalHandler}>
				<EditCatModal _id={selectedCat} modalHandler={editModalHandler} />
			</Modal>
			<Modal active={ordering} modalHandler={orderModalHandler}>
				<OrderCatsModal modalHandler={orderModalHandler} parentKey={data?.getCatsKey[0].parentKey} />
			</Modal>
			<div className="is-flex buttons" style={{ justifyContent: "space-between" }}>
				<h4 className="is-size-6">{data?.getCatsKey[0].name} - дэлгэрэнгүй</h4>
				<div className="is-flex" style={{ alignItems: "center" }}>
					<Link to="/cats" className="button is-danger is-light">
						Буцах
					</Link>
					<button className="button is-success" onClick={() => setIsSub(true)}>
						Дэд цэс үүсгэх
					</button>
					<button className="button is-warning" onClick={() => setOrdering(true)}>
						Эрэмбэлэх
					</button>
					<button
						className="button has-background-danger-light"
						disabled={data?.getCatsKey[0].type !== "cat"}
						onClick={() => setIsDelete(true)}
					>
						<i className="fas fa-trash-alt has-text-danger" />
					</button>
				</div>
			</div>
			<table className="table is-fullwidth is-bordered">
				<thead>
					<tr>
						<th>Нэр</th>
						<th>Хэл</th>
						<th style={{ width: 1 }}></th>
					</tr>
				</thead>
				<tbody>
					{loading && <BoxLoader />}
					{data &&
						data?.getCatsKey.map((oneCat) => {
							return (
								<tr key={oneCat._id}>
									<td>{oneCat.name === " " ? "Бөглөөгүй байна" : oneCat.name}</td>
									<td>{oneCat.language}</td>
									<td>
										<div className="buttons">
											<button
												className={
													oneCat.name === " "
														? "button is-small is-danger is-light"
														: "button is-small"
												}
												onClick={() => {
													setSelectedCat(oneCat._id);
													setIsEdit(true);
												}}
											>
												Засах
											</button>
										</div>
									</td>
								</tr>
							);
						})}
				</tbody>
			</table>
		</div>
	);
};

export default CatDetail;
