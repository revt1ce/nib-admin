import React, { useEffect, useState } from "react";
import { useMutation, useQuery } from "react-apollo";
import { Link } from "react-router-dom";
import AcceptModal from "../components/AcceptModal";
import BoxLoader from "../components/BoxLoader";
import Modal from "../components/Modal";
import { DELETE_CAPITAL } from "../graphql/mutations";
import { GET_CAPITAL, GET_TYPES, GET_LANGUAGES } from "../graphql/queries";

const Capital = () => {
	const allType = {
		title: "Бүгд",
		key: "",
	};
	const [language, setLanguage] = useState("mn");
	const [type, setType] = useState("");
	const [hasMore, setHasMore] = useState(true);

	const [isDelete, setIsDelete] = useState(false);
	const [selectedCap, setSelectedCap] = useState(null);

	const [deleteCapital] = useMutation(DELETE_CAPITAL, {
		variables: {
			key: selectedCap?.key,
		},
		onCompleted() {
			refetch();
		},
	});
	const { data: languageData } = useQuery(GET_LANGUAGES);
	const { data: capTypes } = useQuery(GET_TYPES, {
		variables: {
			language: language,
			slug: "capital",
		},
	});
	const { data, loading, refetch, fetchMore } = useQuery(GET_CAPITAL, {
		variables: {
			language: language,
			typeKey: type,
		},
	});

	useEffect(() => {
		if (data?.getCapitals?.length < 10) {
			setHasMore(false);
		} else {
			setHasMore(true);
		}
	}, [data]);

	const loadMore = () => {
		fetchMore({
			variables: {
				from: data.getCapitals.length > 0 ? data.getCapitals[data.getCapitals.length - 1]._id : null,
				language: language,
				typeKey: type,
			},
			updateQuery: (prev, { fetchMoreResult }) => {
				if (!fetchMoreResult) {
					return prev;
				}
				if (fetchMoreResult.getCapitals.length === 0) {
					setHasMore(false);
				}
				return Object.assign({}, prev, {
					getCapitals: [...prev.getCapitals, ...fetchMoreResult.getCapitals],
				});
			},
		});
	};

	const deleteModalHandler = () => {
		setIsDelete(!isDelete);
	};
	return (
		<div className="box container" style={{ maxWidth: "100%" }}>
			{loading && <BoxLoader />}
			<Modal active={isDelete}>
				<AcceptModal
					message="Устгахыг зөвшөөрч байна уу?"
					modalHandler={deleteModalHandler}
					yes={deleteCapital}
				/>
			</Modal>
			<div
				className="is-flex buttons"
				style={{
					justifyContent: "space-between",
					paddingBottom: 10,
					marginBottom: 20,
					borderBottom: "1px solid #ededed",
				}}
			>
				<h4 className="is-size-6 is-uppercase has-text-weight-bold">Борлуулах хөрөнгө</h4>
				<div className="is-flex buttons">
					<div className="field">
						<div className="select" style={{ marginRight: 10 }}>
							<select value={language} onChange={(e) => setLanguage(e.target.value)}>
								{languageData?.getLanguages.map((language) => {
									return (
										<option value={language.slug} key={language.slug}>
											{language.country}
										</option>
									);
								})}
							</select>
						</div>
					</div>
					<div className="field">
						<div className="select" style={{ marginRight: 10 }}>
							<select value={type} onChange={(e) => setType(e.target.value)} style={{ width: "100%" }}>
								{capTypes?.getTypes.map((type) => {
									return (
										<option value={type.key} key={type.key}>
											{type.title}
										</option>
									);
								})}
								<option value={allType.key}>{allType.title}</option>
							</select>
						</div>
					</div>

					<Link className="button is-success" to="/capital/new">
						Мэдээлэл оруулах
					</Link>
				</div>
			</div>
			<table className="table is-fullwidth is-bordered">
				<thead>
					<tr>
						<th>Нэр</th>
						<th>Төрөл</th>
						<th>Хэл</th>
						<th style={{ width: 1 }}></th>
					</tr>
				</thead>
				{!loading && !data?.getCapitals.length && (
					<p className="is-size-6 has-text-danger" style={{ margin: 10 }}>
						Мэдээлэл байхгүй байна.
					</p>
				)}
				<tbody>
					{data?.getCapitals &&
						data.getCapitals.map((capital) => {
							return (
								<tr key={capital._id}>
									<td>{capital.title}</td>
									<td>{capital.type.title}</td>
									<td>{capital.language}</td>
									<td>
										<div className="is-flex">
											<button
												className="button is-small has-background-danger-light"
												onClick={() => {
													setSelectedCap(capital);
													setIsDelete(true);
												}}
											>
												<i className="fas fa-trash-alt has-text-danger" />
											</button>
											<Link
												to={`/capital/detail/${capital.key}`}
												className="button is-small"
												style={{ marginLeft: 10 }}
											>
												Харах
											</Link>
										</div>
									</td>
								</tr>
							);
						})}
				</tbody>
			</table>
			{hasMore && (
				<div className="has-text-centered">
					<button
						className="button is-small is-uppercase"
						style={{
							display: "inline-block",
							width: "100%",
							backgroundColor: "transparent",
							border: "dashed 1px #ccc",
							height: 50,
							color: "#000",
							padding: 10,
						}}
						onClick={loadMore}
					>
						Цааш үзэх
						<i className="fa fa-chevron-down" style={{ marginLeft: 10 }} />
					</button>
				</div>
			)}
		</div>
	);
};

export default Capital;
