import React, { useEffect } from "react";
import { useQuery } from "react-apollo";
import { Link, useParams } from "react-router-dom";
import BoxLoader from "../components/BoxLoader";
import { GET_BRANCHES_KEY } from "../graphql/queries";

const BranchesDetail = () => {
	const { key } = useParams();

	const { data, loading } = useQuery(GET_BRANCHES_KEY, {
		variables: {
			key: key,
		},
	});
	useEffect(() => {
		if (data?.getBranchesKey) {
			document.title = data?.getBranchesKey[0].title;
		}
	}, [data]);
	return (
		<div className="box container" style={{ maxWidth: "100%" }}>
			<div className="is-flex buttons" style={{ justifyContent: "space-between" }}>
				<div className="is-flex" style={{ alignItems: "center" }}>
					<Link to="/branches" className="button is-danger is-light">
						Буцах
					</Link>
				</div>
			</div>
			<table className="table is-fullwidth is-bordered">
				<thead>
					<tr>
						<th>Нэр</th>
						<th>Төрөл</th>
						<th>Хэл</th>
						<th style={{ width: 1 }}></th>
					</tr>
				</thead>
				<tbody>
					{loading && <BoxLoader />}
					{data?.getBranchesKey &&
						data.getBranchesKey.map((branch) => {
							return (
								<tr key={branch._id}>
									<td>{branch.title === " " ? "Бөглөөгүй байна" : branch.title}</td>
									<td>{branch.type.title}</td>
									<td>{branch.language}</td>
									<td>
										<div className="is-flex">
											<Link
												style={{ marginLeft: 10 }}
												className={
													branch.title === " "
														? "button is-small is-danger is-light"
														: "button is-small"
												}
												to={`/branches/${branch._id}`}
											>
												Засах
											</Link>
										</div>
									</td>
								</tr>
							);
						})}
				</tbody>
			</table>
		</div>
	);
};

export default BranchesDetail;
