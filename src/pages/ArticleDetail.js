import React, { useEffect } from "react";
import { useQuery } from "react-apollo";
import { Link, useParams } from "react-router-dom";
import BoxLoader from "../components/BoxLoader";
import { GET_ARTICLES_KEY } from "../graphql/queries";

const ArticleDetail = () => {
	const { key } = useParams();
	const { data, loading } = useQuery(GET_ARTICLES_KEY, {
		variables: {
			key: key,
		},
	});
	useEffect(() => {
		if (data?.getArticlesKey) {
			document.title = data?.getArticlesKey[0].title;
		}
	}, [data]);
	return (
		<div className="box container" style={{ maxWidth: "100%" }}>
			<div className="is-flex buttons" style={{ justifyContent: "space-between" }}>
				<Link to="/article" className="button is-danger is-light">
					Буцах
				</Link>
			</div>
			<table className="table is-fullwidth is-bordered">
				<thead>
					<tr>
						<th>Гарчиг</th>
						<th>Хэл</th>
						<th style={{ width: 1 }}></th>
					</tr>
				</thead>
				<tbody>
					{loading && <BoxLoader />}
					{data?.getArticlesKey &&
						data.getArticlesKey.map((article) => {
							return (
								<tr key={article._id}>
									<td>{article.title === " " ? "Бөглөөгүй байна" : article.title}</td>
									<td>{article.language}</td>
									<td>
										<div className="is-flex">
											<Link
												style={{ marginLeft: 10 }}
												className={
													article.title === " "
														? "button is-small is-danger is-light"
														: "button is-small"
												}
												to={`/article/${article._id}`}
											>
												Засах
											</Link>
										</div>
									</td>
								</tr>
							);
						})}
				</tbody>
			</table>
		</div>
	);
};

export default ArticleDetail;
