import React, { useState } from "react";
import { useQuery } from "react-apollo";
import { GET_USERS } from "../graphql/queries";
import BoxLoader from "../components/BoxLoader";
import Modal from "../components/Modal";
import EditUserModal from "../components/EditUserModal";
import NewUserModal from "../components/NewUserModal";

const Users = () => {
	const [see, setSee] = useState(false);
	const [newUser, setNewUser] = useState(false);
	const [selectedUser, setSelectedUser] = useState();

	const { data, loading, refetch } = useQuery(GET_USERS);

	const modalHandler = () => {
		setSee(!see);
		refetch();
	};
	const newUserModalHandler = () => {
		setNewUser(!newUser);
	};

	if (loading) return <BoxLoader />;
	return (
		<div className="container box" style={{ maxWidth: "100%" }}>
			<div className="is-flex buttons" style={{ justifyContent: "space-between" }}>
				<h4 className="is-size-6">Нийт хэрэглэгчид</h4>
				<button className="button is-success" onClick={() => setNewUser(true)}>
					Шинэ хэрэглэгч
				</button>
				<Modal active={newUser} modalHandler={newUserModalHandler}>
					<NewUserModal modalHandler={newUserModalHandler} />
				</Modal>
			</div>
			<Modal active={see} modalHandler={modalHandler} width={1200} height={800}>
				<EditUserModal _id={selectedUser} modalHandler={modalHandler} />
			</Modal>
			<table className="table is-fullwidth is-bordered">
				<thead>
					<tr>
						<th>Хэрэглэгчийн нэр</th>
						<th style={{ width: 1 }}></th>
					</tr>
				</thead>
				<tbody>
					{data?.getUsers &&
						data.getUsers.map((user) => {
							return (
								<tr key={user._id}>
									<td>{user.username}</td>
									<td>
										<div className="buttons">
											<button
												className="button is-small"
												onClick={() => {
													setSee(true);
													setSelectedUser(user._id);
												}}
											>
												Харах
											</button>
										</div>
									</td>
								</tr>
							);
						})}
				</tbody>
			</table>
		</div>
	);
};

export default Users;
