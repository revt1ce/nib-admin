import React, { useState, useEffect } from "react";
import { Link, useParams } from "react-router-dom";
import { GET_CURRENT_USER, GET_PRODUCTS, GET_ONE_PRODUCT } from "../graphql/queries";
import { useMutation, useLazyQuery } from "react-apollo";
import ImageField from "../components/ImageField";
import Modal from "../components/Modal";
import CatsTreeModal from "../components/CatsTreeModal";
import TabItem from "../components/TabItem";
import { CREATE_PRODUCT, UPDATE_PRODUCT } from "../graphql/mutations";
import Message from "../components/Message";
import BoxLoader from "../components/BoxLoader";
import CKEditor from "ckeditor4-react";

const NewProduct = () => {
	const { id } = useParams();
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [tag, setTag] = useState(null);
	const [image, setImage] = useState(null);
	const [author, setAuthor] = useState("");
	const [tabs, setTabs] = useState([null]);

	const [message, setMessage] = useState("");
	const [messageType, setMessageType] = useState("");
	const [showMessage, setShowMessage] = useState(false);

	const [cat, setCat] = useState("");
	const [pickCat, setPickCat] = useState(false);

	const [isTag, setIsTag] = useState(false);

	const [getCurrentUser, { data: userData }] = useLazyQuery(GET_CURRENT_USER);

	const [getOneProduct, { data: oneProductData, loading: oneProductLoading }] = useLazyQuery(GET_ONE_PRODUCT, {
		variables: { _id: id },
	});

	useEffect(() => {
		if (id) {
			if (id !== "new") {
				getOneProduct();
			} else {
				getCurrentUser();
			}
		}
	}, [id, getOneProduct, getCurrentUser]);
	useEffect(() => {
		if (oneProductData) {
			if (oneProductData.getOneProduct) {
				setName(oneProductData.getOneProduct.name);
				setDescription(oneProductData.getOneProduct.description);
				setTag(oneProductData.getOneProduct.tag);
				setCat(oneProductData.getOneProduct.category);
				setImage(oneProductData.getOneProduct.image);
				setTabs(oneProductData.getOneProduct.tabs);
				setAuthor(oneProductData.getOneProduct.author);
			}
		}
	}, [oneProductData]);
	useEffect(() => {
		if (userData?.me) {
			setAuthor(userData.me.username);
		}
	}, [userData]);

	useEffect(() => {
		if (name) {
			document.title = name;
		}
	}, [name]);

	const [updateProduct] = useMutation(UPDATE_PRODUCT, {
		onCompleted() {
			window.scrollTo(0, 0);
			setMessage("Амжилттай хадгалагдлаа!");
			setMessageType("is-success");
			setShowMessage(true);
		},
	});

	const [createProduct, { loading }] = useMutation(CREATE_PRODUCT, {
		onCompleted() {
			setMessage("Амжилттай нэмэгдлээ!");
			setMessageType("is-success");
			setShowMessage(true);
			setName("");
			setDescription("");
			setTag("");
			setCat("");
			setImage(null);
			setTabs([null]);
		},
		onError() {
			setMessage("Алдаа гарлаа!");
			setMessageType("is-danger");
			setShowMessage(true);
		},
		update(cache, { data: { createProduct } }) {
			const { getProducts } = cache.readQuery({
				query: GET_PRODUCTS,
				variables: { language: "mn", catKey: null },
			});

			cache.writeQuery({
				query: GET_PRODUCTS,
				variables: { language: "mn", catKey: null },
				data: { getProducts: [...getProducts, createProduct] },
			});
		},
	});

	const submit = () => {
		if (!cat) {
			setMessage("Цэс сонгоогүй байна!");
			setMessageType("is-danger");
			setShowMessage(true);
		} else {
			if (id !== "new") {
				let sendTabs = tabs
					.filter((tab) => tab !== null)
					.map((item) => {
						return {
							title: item.title,
							content: item.content,
						};
					});
				updateProduct({
					variables: {
						_id: id,
						catKey: cat.key,
						name: name,
						description: description,
						tag: tag,
						image: image
							? {
									_id: image._id,
									path: image.path,
									width: image.width,
									height: image.height,
									type: image.type,
							  }
							: null,
						tabs: sendTabs,
					},
				});
			} else {
				createProduct({
					variables: {
						catKey: cat.key,
						name: name,
						description: description,
						tag: tag,
						image: image,
						tabs: tabs.filter((tab) => tab !== null),
						author: author,
					},
				});
			}
		}
	};

	const updateImage = (image) => {
		setImage(image);
	};

	const selectCatHandler = (cat) => {
		setCat(cat);
		modalHandler(!pickCat);
	};

	const modalHandler = () => {
		setPickCat(!pickCat);
	};

	const addOneTab = () => {
		const newTabs = [...tabs, null];
		setTabs(newTabs);
	};
	const deleteTab = (index) => {
		if (tabs.length > 1) {
			tabs.splice(index, 1);
			setTabs([...tabs]);
		} else {
			updateTab(null, index);
		}
	};
	const updateTab = (tab, index) => {
		const newTabs = [...tabs];
		newTabs[index] = tab;
		setTabs(newTabs);
	};

	return (
		<div className="container box" style={{ maxWidth: "100%" }}>
			<Modal active={pickCat} modalHandler={modalHandler}>
				<CatsTreeModal type="cat" language="mn" modalHandler={modalHandler} onSelect={selectCatHandler} />
			</Modal>
			<div
				className="is-flex"
				style={{
					justifyContent: "space-between",
					paddingBottom: 10,
					marginBottom: 20,
					borderBottom: "1px solid #ededed",
				}}
			>
				<h4 className="subtitle">Бүтээгдэхүүн оруулах</h4>
				<Link
					className="button is-danger is-light"
					to={id === "new" ? "/products" : `/products/detail/${oneProductData?.getOneProduct.key}`}
				>
					Буцах
				</Link>
			</div>
			{showMessage && <Message message={message} type={messageType} />}
			<div className="columns">
				{oneProductLoading && <BoxLoader />}
				<div className="column is-8">
					<div className="is-flex" style={{ justifyContent: "space-between" }}>
						<div className="field" style={{ width: "50%" }}>
							<label className="label">Нийтлэгч</label>
							<div className="control is-flex">
								<input className="input" disabled={true} value={author} />
							</div>
						</div>
						<div className="field" style={{ width: "45%" }}>
							<label className="label">Цэс</label>
							<div className="control is-flex">
								<input className="input" disabled={true} value={cat && cat.name} type="text" required />
								<span style={{ width: "20%" }} className="button" onClick={() => setPickCat(true)}>
									Сонгох
								</span>
							</div>
						</div>
					</div>
					<div className="is-flex" style={{ justifyContent: "space-between" }}>
						<div className="field" style={{ width: "50%" }}>
							<label className="label">Гарчиг</label>
							<div className="control">
								<input
									className="input"
									type="text"
									value={name}
									onChange={(e) => setName(e.target.value)}
									disabled={loading}
									required
								/>
							</div>
						</div>
						<div className="field" style={{ width: "45%" }}>
							<label className="label">Шошго</label>
							<div className="is-flex">
								<label class="checkbox" style={{ padding: "10px 10px 10px 0px" }}>
									<input type="checkbox" value={isTag} onClick={() => setIsTag(!isTag)} />
								</label>
								<div className="control">
									<input
										className="input"
										type="text"
										value={tag}
										onChange={(e) => setTag(e.target.value)}
										disabled={!isTag}
									/>
								</div>
							</div>
						</div>
					</div>
					<div className="field">
						<label className="label">Тайлбар</label>
						<div className="control">
							<CKEditor data={description} onChange={(evt) => setDescription(evt.editor.getData())} />
						</div>
					</div>
					<div className="field">
						<label className="label">Зураг</label>
						<ImageField
							size={150}
							value={image}
							onChange={(image) => updateImage(image)}
							onDelete={() => updateImage(null)}
						/>
					</div>
				</div>
				<div className="column is-4 has-background-white-ter" style={{ borderRadius: 3 }}>
					<div className="field" style={{ padding: 15 }}>
						<label className="label">Табууд</label>
						{tabs.map((tab, index) => {
							return (
								<TabItem
									key={index}
									value={tab}
									onDelete={() => deleteTab(index)}
									onChange={(data) => updateTab(data, index)}
								/>
							);
						})}
						<span
							className="button is-small is-uppercase"
							style={{
								width: "100%",
								backgroundColor: "transparent",
								border: "dashed 1px #48c774",
								height: 40,
								color: "#48c774",
							}}
							onClick={() => addOneTab()}
							disabled={loading}
						>
							<i className="fa fa-plus" style={{ marginRight: 10 }}></i> Таб нэмэх
						</span>
					</div>
				</div>
			</div>
			<div className="is-flex" style={{ justifyContent: "space-between" }}>
				<div />
				<div className="buttons">
					<button className="button is-success" onClick={() => submit()} disabled={loading}>
						{id === "new" ? "Нэмэх" : "Хадгалах"}
					</button>
				</div>
			</div>
		</div>
	);
};

export default NewProduct;
