import React, { useState } from "react";
import { useMutation, useQuery } from "react-apollo";
import { Link } from "react-router-dom";
import AcceptModal from "../components/AcceptModal";
import BoxLoader from "../components/BoxLoader";
import Modal from "../components/Modal";
import { DELETE_ARTICLE } from "../graphql/mutations";
import { GET_ARTICLES, GET_LANGUAGES } from "../graphql/queries";

const Article = () => {
	const [language, setLanguage] = useState("mn");
	const [selectedArticle, setSelectedArticle] = useState(null);
	const [isDelete, setIsDelete] = useState(false);
	const { data: languageData } = useQuery(GET_LANGUAGES);
	const { data, loading, refetch } = useQuery(GET_ARTICLES, {
		variables: {
			language: language,
		},
	});
	const [deleteArticle] = useMutation(DELETE_ARTICLE, {
		variables: {
			key: selectedArticle,
		},
		onCompleted() {
			refetch();
		},
	});
	const deleteModalHandler = () => {
		setIsDelete(!isDelete);
	};
	return (
		<div className="box container" style={{ maxWidth: "100%" }}>
			{loading && <BoxLoader />}
			<Modal active={isDelete}>
				<AcceptModal
					message="Устгахыг зөвшөөрч байна уу?"
					modalHandler={deleteModalHandler}
					yes={deleteArticle}
				/>
			</Modal>
			<div
				className="is-flex buttons"
				style={{
					justifyContent: "space-between",
					paddingBottom: 10,
					marginBottom: 20,
					borderBottom: "1px solid #ededed",
				}}
			>
				<h4 className="is-size-6 is-uppercase has-text-weight-bold">Удирдлага</h4>
				<div>
					<div className="select" style={{ marginRight: 10 }}>
						<select value={language} onChange={(e) => setLanguage(e.target.value)}>
							{languageData?.getLanguages.map((language) => {
								return (
									<option value={language.slug} key={language.slug}>
										{language.country}
									</option>
								);
							})}
						</select>
					</div>
					<Link className="button is-success" to="/article/new">
						Мэдээлэл нэмэх
					</Link>
				</div>
			</div>
			<table className="table is-fullwidth is-bordered">
				<thead>
					<tr>
						<th>Гарчиг</th>
						<th>Хэл</th>
						<th style={{ width: 1 }}></th>
					</tr>
				</thead>
				{!loading && !data?.getArticles.length && (
					<p className="is-size-6 has-text-danger" style={{ margin: 10 }}>
						Нийтлэл байхгүй байна.
					</p>
				)}
				<tbody>
					{data?.getArticles &&
						data.getArticles.map((article) => {
							return (
								<tr key={article._id}>
									<td>{article.title === " " ? "Бөглөөгүй байна" : article.title}</td>
									<td>{article.language}</td>
									<td>
										<div className="is-flex">
											<button
												className="button is-small has-background-danger-light"
												onClick={() => {
													setSelectedArticle(article.key);
													setIsDelete(true);
												}}
											>
												<i className="fas fa-trash-alt has-text-danger" />
											</button>
											<Link
												to={`/article/detail/${article.key}`}
												className="button is-small"
												style={{ marginLeft: 10 }}
											>
												Харах
											</Link>
										</div>
									</td>
								</tr>
							);
						})}
				</tbody>
			</table>
		</div>
	);
};

export default Article;
