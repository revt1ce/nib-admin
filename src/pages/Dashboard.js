import React, { useEffect } from "react";

const Dashboard = () => {
	useEffect(() => {
		document.title = "Dashboard";
	}, []);
	return (
		<div className="box container " style={{ maxWidth: "100%" }}>
			<p className="subtitle has-text-centered has-text-link">NIB DASHBOARD</p>
		</div>
	);
};

export default Dashboard;
