import React, { useEffect } from "react";
import { Link, useParams } from "react-router-dom";
import { useQuery } from "react-apollo";
import { GET_PRODUCTS_KEY } from "../graphql/queries";
import BoxLoader from "../components/BoxLoader";

const ProductDetail = () => {
	const { key } = useParams();

	const { data, loading } = useQuery(GET_PRODUCTS_KEY, {
		variables: {
			key: key,
		},
	});

	useEffect(() => {
		if (data?.getProductsKey) {
			document.title = data?.getProductsKey[0].name;
		}
	}, [data]);

	return (
		<div className="box container" style={{ maxWidth: "100%" }}>
			<div className="is-flex buttons" style={{ justifyContent: "space-between" }}>
				<div className="is-flex" style={{ alignItems: "center" }}>
					<Link to="/products" className="button is-danger is-light">
						Буцах
					</Link>
				</div>
			</div>
			<table className="table is-fullwidth is-bordered">
				<thead>
					<tr>
						<th>Гарчиг</th>
						<th>Цэс</th>
						<th>Хэл</th>
						<th style={{ width: 1 }}></th>
					</tr>
				</thead>
				<tbody>
					{loading && <BoxLoader />}
					{data?.getProductsKey &&
						data.getProductsKey.map((product) => {
							console.log(product);
							return (
								<tr key={product._id}>
									<td>{product.name === " " ? "Бөглөөгүй байна" : product.name}</td>
									<td>{product.category.name === " " ? "Бөглөөгүй байна" : product.category.name}</td>
									<td>{product.language}</td>
									<td>
										<div className="is-flex">
											<Link
												style={{ marginLeft: 10 }}
												className={
													product.name === " "
														? "button is-small is-danger is-light"
														: "button is-small"
												}
												to={`/products/${product._id}`}
											>
												Засах
											</Link>
										</div>
									</td>
								</tr>
							);
						})}
				</tbody>
			</table>
		</div>
	);
};

export default ProductDetail;
