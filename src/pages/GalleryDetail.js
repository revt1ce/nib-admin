import React from "react";
import { Link, useParams } from "react-router-dom";
import { GET_GALLERIES_KEY } from "../graphql/queries";
import { useQuery } from "react-apollo";
import BoxLoader from "../components/BoxLoader";

const GalleryDetail = () => {
	const { key } = useParams();
	const { data, loading } = useQuery(GET_GALLERIES_KEY, {
		variables: {
			key: key,
		},
	});

	return (
		<div className="box container" style={{ maxWidth: "100%" }}>
			{loading && <BoxLoader />}
			<div className="buttons is-flex" style={{ justifyContent: "space-between" }}>
				<div />
				<div>
					<Link className="button is-danger is-light" to="/galleries">
						Буцах
					</Link>
				</div>
			</div>
			<table className="table is-fullwidth is-bordered">
				<thead>
					<tr>
						<th>Нэр</th>
						<th>Цэс</th>
						<th>Хэл</th>
						<th>Төлөв</th>
						<th>Нийт зураг</th>
						<th style={{ width: 1 }}></th>
					</tr>
				</thead>
				<tbody>
					{loading && <BoxLoader />}
					{data?.getGalleriesKey &&
						data.getGalleriesKey.map((gallery) => {
							return (
								<tr key={gallery._id}>
									<td>{gallery.name === " " ? "Бөглөөгүй байна" : gallery.name}</td>
									<td>{gallery.category.name === " " ? "Бөглөөгүй байна" : gallery.category.name}</td>
									<td>{gallery.language}</td>
									<td>{gallery.status}</td>
									<td>{gallery.images.length}</td>
									<td>
										<div className="is-flex">
											<Link
												style={{ marginLeft: 10 }}
												className={
													gallery.name === " "
														? "button is-small is-danger is-light"
														: "button is-small"
												}
												to={`/galleries/${gallery._id}`}
											>
												Засах
											</Link>
										</div>
									</td>
								</tr>
							);
						})}
				</tbody>
			</table>
		</div>
	);
};

export default GalleryDetail;
