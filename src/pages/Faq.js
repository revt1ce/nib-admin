import React, { useEffect, useState } from "react";
import { useMutation, useQuery } from "react-apollo";
import AcceptModal from "../components/AcceptModal";
import BoxLoader from "../components/BoxLoader";
import FaqDetailModal from "../components/FaqDetailModal";
import Modal from "../components/Modal";
import NewFaqModal from "../components/NewFaqModal";
import { DELETE_FAQ } from "../graphql/mutations";
import { GET_LANGUAGES, GET_FAQ } from "../graphql/queries";
const Faq = () => {
	const [language, setLanguage] = useState("mn");
	const [hasMore, setHasMore] = useState(true);
	const [isNew, setIsNew] = useState(false);
	const [isDetail, setIsDetail] = useState(false);
	const [selectedFaq, setSelectedFaq] = useState(null);
	const [isDelete, setIsDelete] = useState(false);

	const { data: languageData } = useQuery(GET_LANGUAGES);
	const { data, loading, refetch, fetchMore } = useQuery(GET_FAQ, {
		variables: {
			language: language,
		},
	});

	useEffect(() => {
		if (data?.getFaq?.length < 10) {
			setHasMore(false);
		} else {
			setHasMore(true);
		}
	}, [data]);

	const loadMore = () => {
		fetchMore({
			variables: {
				from: data.getFaq.length > 0 ? data.getFaq[data.getFaq.length - 1]._id : null,
				language: language,
			},
			updateQuery: (prev, { fetchMoreResult }) => {
				if (!fetchMoreResult) {
					return prev;
				}
				if (fetchMoreResult.getFaq.length === 0) {
					setHasMore(false);
				}
				return Object.assign({}, prev, {
					getFaq: [...prev.getFaq, ...fetchMoreResult.getFaq],
				});
			},
		});
	};

	const [deleteFaq] = useMutation(DELETE_FAQ, {
		variables: {
			key: selectedFaq?.key,
		},
		onCompleted() {
			refetch();
		},
	});

	const deleteModalHandler = () => {
		setIsDelete(!isDelete);
	};

	return (
		<div className="box container" style={{ maxWidth: "100%" }}>
			{loading && <BoxLoader />}
			<Modal active={isDelete}>
				<AcceptModal message="Устгахыг зөвшөөрч байна уу?" modalHandler={deleteModalHandler} yes={deleteFaq} />
			</Modal>
			<Modal active={isNew}>
				<NewFaqModal modalHandler={(bool) => setIsNew(bool)} />
			</Modal>
			<Modal active={isDetail} width={1000}>
				<FaqDetailModal modalHandler={(bool) => setIsDetail(bool)} faq={selectedFaq} />
			</Modal>
			<div
				className="is-flex buttons"
				style={{
					justifyContent: "space-between",
					paddingBottom: 10,
					marginBottom: 20,
					borderBottom: "1px solid #ededed",
				}}
			>
				<h4 className="is-size-6 is-uppercase has-text-weight-bold">Асуулт хариулт</h4>
				<div className="buttons">
					<div className="select" style={{ marginRight: 10, marginBottom: 10 }}>
						<select value={language} onChange={(e) => setLanguage(e.target.value)}>
							{languageData?.getLanguages.map((language) => {
								return (
									<option value={language.slug} key={language.slug}>
										{language.country}
									</option>
								);
							})}
						</select>
					</div>

					<button className="button is-success" onClick={() => setIsNew(true)}>
						Нэмэх
					</button>
				</div>
			</div>

			<table className="table is-fullwidth is-bordered">
				<thead>
					<tr>
						<th>Асуулт</th>
						<th>Хариулт</th>
						<th>Хэл</th>
						<th style={{ width: 1 }}></th>
					</tr>
				</thead>
				{!loading && !data?.getFaq.length && (
					<p className="is-size-6 has-text-danger" style={{ margin: 10 }}>
						Асуулт хариулт байхгүй байна.
					</p>
				)}
				<tbody>
					{data?.getFaq &&
						data.getFaq.map((faq) => {
							return (
								<tr key={faq._id}>
									<td>{faq.question}</td>
									<td>{faq.answer}</td>
									<td>{faq.language}</td>
									<td>
										<div className="is-flex">
											<button
												className="button is-small has-background-danger-light"
												onClick={() => {
													setSelectedFaq(faq);
													setIsDelete(true);
												}}
											>
												<i className="fas fa-trash-alt has-text-danger" />
											</button>
											<button
												className="button is-small"
												style={{ marginLeft: 10 }}
												onClick={() => {
													setSelectedFaq(faq);
													setIsDetail(true);
												}}
											>
												Харах
											</button>
										</div>
									</td>
								</tr>
							);
						})}
				</tbody>
			</table>
			{hasMore && (
				<div className="has-text-centered">
					<button
						className="button is-small is-uppercase"
						style={{
							display: "inline-block",
							width: "100%",
							backgroundColor: "transparent",
							border: "dashed 1px #ccc",
							height: 50,
							color: "#000",
							padding: 10,
						}}
						onClick={loadMore}
					>
						Цааш үзэх
						<i className="fa fa-chevron-down" style={{ marginLeft: 10 }} />
					</button>
				</div>
			)}
		</div>
	);
};

export default Faq;
