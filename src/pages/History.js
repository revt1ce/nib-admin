import React, { useEffect, useState } from "react";
import { useMutation, useQuery } from "react-apollo";
import AcceptModal from "../components/AcceptModal";
import BoxLoader from "../components/BoxLoader";
import HistoryDetailModal from "../components/HistoryDetailModal";
import Modal from "../components/Modal";
import NewHistoryModal from "../components/NewHistoryModal";
import { DELETE_HISTORY } from "../graphql/mutations";
import { GET_LANGUAGES, GET_HISTORIES } from "../graphql/queries";

const History = () => {
	const [language, setLanguage] = useState("mn");
	const [hasMore, setHasMore] = useState(true);
	const [selectedHistory, setSelectedHistory] = useState(null);
	const [isNew, setIsNew] = useState(false);
	const [isDetail, setIsDetail] = useState(false);
	const [isDelete, setIsDelete] = useState(false);

	const { data: languageData } = useQuery(GET_LANGUAGES);
	const { data, loading, refetch, fetchMore } = useQuery(GET_HISTORIES, {
		variables: {
			language: language,
		},
	});

	useEffect(() => {
		if (data?.getHistories?.length < 10) {
			setHasMore(false);
		} else {
			setHasMore(true);
		}
	}, [data]);

	const loadMore = () => {
		fetchMore({
			variables: {
				from: data.getHistories.length > 0 ? data.getHistories[data.getHistories.length - 1]._id : null,
				language: language,
			},
			updateQuery: (prev, { fetchMoreResult }) => {
				if (!fetchMoreResult) {
					return prev;
				}
				if (fetchMoreResult.getHistories.length === 0) {
					setHasMore(false);
				}
				return Object.assign({}, prev, {
					getHistories: [...prev.getHistories, ...fetchMoreResult.getHistories],
				});
			},
		});
	};

	const [deleteHistory] = useMutation(DELETE_HISTORY, {
		variables: {
			key: selectedHistory?.key,
		},
		onCompleted() {
			refetch();
		},
	});

	const newModalHandler = () => {
		setIsNew(!isNew);
	};
	const deleteModalHandler = () => {
		setIsDelete(!isDelete);
	};

	return (
		<div className="box container" style={{ maxWidth: "100%" }}>
			<Modal active={isDelete}>
				<AcceptModal
					modalHandler={deleteModalHandler}
					message="Устгахыг зөвшөөрч байна уу?"
					yes={deleteHistory}
				/>
			</Modal>
			<Modal active={isNew}>
				<NewHistoryModal modalHandler={newModalHandler} />
			</Modal>
			<Modal active={isDetail}>
				<HistoryDetailModal modalHandler={() => setIsDetail(!isDetail)} history={selectedHistory} />
			</Modal>
			{loading && <BoxLoader />}
			<div
				className="is-flex buttons"
				style={{
					justifyContent: "space-between",
					paddingBottom: 10,
					marginBottom: 20,
					borderBottom: "1px solid #ededed",
				}}
			>
				<h4 className="is-size-6 is-uppercase has-text-weight-bold">Түүх</h4>
				<div>
					<div className="select" style={{ marginRight: 10 }}>
						<select value={language} onChange={(e) => setLanguage(e.target.value)}>
							{languageData?.getLanguages.map((language) => {
								return (
									<option value={language.slug} key={language.slug}>
										{language.country}
									</option>
								);
							})}
						</select>
					</div>
					<button className="button is-success" onClick={() => setIsNew(true)}>
						Түүх нэмэх
					</button>
				</div>
			</div>
			<table className="table is-fullwidth is-bordered">
				<thead>
					<tr>
						<th>Он</th>
						<th>Хэл</th>
						<th>Табууд</th>
						<th style={{ width: 1 }}></th>
					</tr>
				</thead>
				<tbody>
					{data?.getHistories &&
						data.getHistories.map((history) => {
							return (
								<tr key={history._id}>
									<td>{history.name}</td>
									<td>{history.language}</td>
									<td>{history.tabs.length}</td>
									<td>
										<div className="is-flex">
											<button
												className="button is-small has-background-danger-light"
												onClick={() => {
													setSelectedHistory(history);
													setIsDelete(true);
												}}
											>
												<i className="fas fa-trash-alt has-text-danger" />
											</button>
											<button
												className="button is-small"
												style={{ marginLeft: 10 }}
												onClick={() => {
													setSelectedHistory(history);
													setIsDetail(true);
												}}
											>
												Засах
											</button>
										</div>
									</td>
								</tr>
							);
						})}
				</tbody>
			</table>
			{hasMore && (
				<div className="has-text-centered">
					<button
						className="button is-small is-uppercase"
						style={{
							display: "inline-block",
							width: "100%",
							backgroundColor: "transparent",
							border: "dashed 1px #ccc",
							height: 50,
							color: "#000",
							padding: 10,
						}}
						onClick={loadMore}
					>
						Цааш үзэх
						<i className="fa fa-chevron-down" style={{ marginLeft: 10 }} />
					</button>
				</div>
			)}
		</div>
	);
};

export default History;
