import React, { useEffect, useState } from "react";
import { useMutation, useQuery } from "react-apollo";
import BoxLoader from "../components/BoxLoader";
import Message from "../components/Message";
import SocialTabItem from "../components/SocialTabItem";
import { UPDATE_SOCIAL } from "../graphql/mutations";
import { GET_LANGUAGES, GET_SOCIAL } from "../graphql/queries";

const Social = () => {
	const [phonenumber, setPhonenumber] = useState("");
	const [socials, setSocials] = useState([null]);
	const [copyright, setCopyright] = useState("");

	const [message, setMessage] = useState("");
	const [messageType, setMessageType] = useState("");
	const [showMessage, setShowMessage] = useState(false);

	const [language, setLanguage] = useState("mn");
	const { data: languageData } = useQuery(GET_LANGUAGES);
	const { data, loading } = useQuery(GET_SOCIAL, {
		variables: {
			language: language,
		},
		onCompleted(data) {
			setPhonenumber(data.getSocial.phonenumber);
			setSocials(data.getSocial.socials);
			setCopyright(data.getSocial.copyright);
		},
	});

	const [updateSocial] = useMutation(UPDATE_SOCIAL, {
		onCompleted() {
			setMessage("Амжилттай хадгалагдлаа!");
			setMessageType("is-success");
			setShowMessage(true);
		},
	});

	const submit = () => {
		updateSocial({
			variables: {
				_id: data?.getSocial._id,
				phonenumber: phonenumber,
				copyright: copyright,
				socials: socials
					.filter((social) => social !== null)
					.map((data) => {
						return {
							name: data.name,
							url: data.url,
						};
					}),
			},
		});
	};

	const addOneSocial = () => {
		const newSocials = [...socials, null];
		setSocials(newSocials);
	};
	const deleteSocial = (index) => {
		if (socials.length > 1) {
			socials.splice(index, 1);
			setSocials([...socials]);
		} else {
			updateSocials(null, index);
		}
	};
	const updateSocials = (social, index) => {
		const newSocials = [...socials];
		newSocials[index] = social;
		setSocials(newSocials);
	};
	useEffect(() => {
		console.log(socials);
	}, [socials]);

	useEffect(() => {
		if (showMessage) {
			setTimeout(() => {
				setShowMessage(false);
			}, [3000]);
		}
	}, [showMessage]);
	return (
		<div className="box container" style={{ maxWidth: "100%" }}>
			{loading && <BoxLoader />}
			<div
				className="is-flex buttons"
				style={{
					justifyContent: "space-between",
					paddingBottom: 10,
					marginBottom: 20,
					borderBottom: "1px solid #ededed",
				}}
			>
				<h4 className="is-size-6 is-uppercase has-text-weight-bold">Сошиал холбоосууд</h4>
				<div className="select" style={{ marginRight: 10 }}>
					<select value={language} onChange={(e) => setLanguage(e.target.value)}>
						{languageData?.getLanguages.map((language) => {
							return (
								<option value={language.slug} key={language.slug}>
									{language.country}
								</option>
							);
						})}
					</select>
				</div>
			</div>
			{showMessage && <Message message={message} type={messageType} />}
			<div>
				<div className="field">
					<label className="label">Лавлах утасны дугаар</label>
					<div className="control">
						<input
							className="input"
							type="text"
							value={phonenumber}
							onChange={(e) => setPhonenumber(e.target.value)}
							required
						/>
					</div>
				</div>
				<div className="field">
					<label className="label">Зохиогчийн эрх</label>
					<div className="control">
						<input
							className="input"
							type="text"
							value={copyright}
							onChange={(e) => setCopyright(e.target.value)}
							required
						/>
					</div>
				</div>
				<div className="column has-background-white-ter">
					<label className="label">Сошиал холбоос</label>
					{socials.map((social, index) => {
						return (
							<SocialTabItem
								key={index}
								value={social}
								onDelete={() => deleteSocial(index)}
								onChange={(data) => updateSocials(data, index)}
							/>
						);
					})}
					<span
						className="button is-small is-uppercase"
						style={{
							width: "100%",
							backgroundColor: "transparent",
							border: "dashed 1px #48c774",
							height: 40,
							color: "#48c774",
						}}
						onClick={() => addOneSocial()}
					>
						<i className="fa fa-plus" style={{ marginRight: 10 }}></i> Сошиал холбоос нэмэх
					</span>
				</div>
				<div className="buttons" style={{ marginTop: 10 }}>
					<button className="button is-success" onClick={() => submit()}>
						Хадгалах
					</button>
				</div>
			</div>
		</div>
	);
};

export default Social;
