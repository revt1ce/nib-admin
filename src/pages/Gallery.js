import React, { useState, useEffect } from "react";
import Modal from "../components/Modal";
import NewGalleryModal from "../components/NewGalleryModal";
import { GET_LANGUAGES, GET_GALLERIES } from "../graphql/queries";
import { useQuery, useMutation } from "react-apollo";
import CatsTreeModal from "../components/CatsTreeModal";
import BoxLoader from "../components/BoxLoader";
import { DELETE_GALLERY, ACTIVE_GALLERY } from "../graphql/mutations";
import AcceptModal from "../components/AcceptModal";
import { Link } from "react-router-dom";

const Gallery = () => {
	const [language, setLanguage] = useState("mn");
	const [hasMore, setHasMore] = useState(true);
	const [isNew, setIsNew] = useState(false);
	const [pickCat, setPickCat] = useState(false);
	const [cat, setCat] = useState();
	const [selectedGallery, setSelectedGallery] = useState();
	const [isDelete, setIsDelete] = useState(false);

	const { data: languageData } = useQuery(GET_LANGUAGES);

	const [deleteGallery] = useMutation(DELETE_GALLERY, {
		variables: {
			key: selectedGallery && selectedGallery.key,
		},
		onCompleted() {
			refetch();
		},
	});

	const { data: galleryData, loading, refetch, fetchMore } = useQuery(GET_GALLERIES, {
		variables: {
			language: language,
			catKey: cat?.key,
		},
	});

	const [activeGallery] = useMutation(ACTIVE_GALLERY, {
		onCompleted() {
			window.location.reload();
		},
	});

	useEffect(() => {
		if (galleryData) {
			if (galleryData.getGalleries) {
				if (galleryData.getGalleries.length < 10) {
					setHasMore(false);
				} else {
					setHasMore(true);
				}
			}
		}
	}, [galleryData]);

	const loadMore = () => {
		fetchMore({
			variables: {
				from:
					galleryData.getGalleries.length > 0
						? galleryData.getGalleries[galleryData.getGalleries.length - 1]._id
						: null,
				language: language,
				catKey: cat?.key,
			},
			updateQuery: (prev, { fetchMoreResult }) => {
				if (!fetchMoreResult) {
					return prev;
				}
				if (fetchMoreResult.getGalleries.length === 0) {
					setHasMore(false);
				}
				return Object.assign({}, prev, {
					getGalleries: [...prev.getGalleries, ...fetchMoreResult.getGalleries],
				});
			},
		});
	};

	const modalHandler = () => {
		setIsNew(!isNew);
	};

	const selectCatHandler = (cat) => {
		setCat(cat);
		setPickCat(false);
	};

	const catModalHandler = () => {
		setPickCat(!pickCat);
	};

	const deleteModalHandler = () => {
		setIsDelete(!isDelete);
	};

	useEffect(() => {
		document.title = "Галлерей";
	}, []);

	return (
		<div className="box container" style={{ maxWidth: "100%" }}>
			{loading && <BoxLoader />}
			<Modal active={isDelete} modalHandler={deleteModalHandler}>
				<AcceptModal
					message="Устгахыг зөвшөөрч байна уу?"
					modalHandler={deleteModalHandler}
					yes={deleteGallery}
				/>
			</Modal>
			<Modal active={isNew} modalHandler={modalHandler}>
				<NewGalleryModal modalHandler={modalHandler} />
			</Modal>
			<Modal active={pickCat} modalHandler={catModalHandler}>
				<CatsTreeModal
					language="mn"
					modalHandler={catModalHandler}
					onSelect={selectCatHandler}
					isFilter={true}
					type="cat"
				/>
			</Modal>
			<Modal></Modal>
			<div
				className="is-flex buttons"
				style={{
					justifyContent: "space-between",
					paddingBottom: 10,
					marginBottom: 20,
					borderBottom: "1px solid #ededed",
				}}
			>
				<h4 className="is-size-6 is-uppercase has-text-weight-bold">Галерей</h4>
				<div>
					<div className="select" style={{ marginRight: 10 }}>
						<select value={language} onChange={(e) => setLanguage(e.target.value)}>
							{languageData?.getLanguages.map((language) => {
								return (
									<option value={language.slug} key={language.slug}>
										{language.country}
									</option>
								);
							})}
						</select>
					</div>

					<button className="button" onClick={() => setPickCat(true)}>
						<span className="icon is-small">
							<i className="fal fa-filter"></i>
						</span>
						<span>{cat ? cat.name : "Бүх цэс"}</span>
					</button>

					<button className="button is-success" onClick={() => setIsNew(true)}>
						Шинэ галерей
					</button>
				</div>
			</div>
			<table className="table is-fullwidth is-bordered">
				<thead>
					<tr>
						<th>Нэр</th>
						<th>Цэс</th>
						<th>Хэл</th>
						<th>Төлөв</th>
						<th>Нийт зураг</th>
						<th style={{ width: 1 }}></th>
					</tr>
				</thead>
				{galleryData?.getGalleries.length === 0 && (
					<p className="is-size-6 has-text-danger" style={{ margin: 10 }}>
						Галерей байхгүй байна.
					</p>
				)}
				<tbody>
					{galleryData?.getGalleries &&
						galleryData.getGalleries.map((gallery) => {
							return (
								<tr key={gallery._id}>
									<td>{gallery.name}</td>
									<td>{gallery.category.name}</td>
									<td>{gallery.language}</td>
									<td>
										<span
											className={`tag ${
												gallery.status === "inactive" ? "is-light" : "is-success"
											}`}
										>
											{gallery.status}
										</span>
									</td>
									<td>{gallery.images.length}</td>
									<td>
										<div className="is-flex">
											<button
												className="button is-small has-background-danger-light"
												onClick={() => {
													setSelectedGallery(gallery);
													setIsDelete(true);
												}}
											>
												<i className="fas fa-trash-alt has-text-danger" />
											</button>
											<Link
												className="button is-small"
												style={{ marginLeft: 10 }}
												to={`/galleries/detail/${gallery.key}`}
											>
												Засах
											</Link>
											<button
												className="button is-success is-small"
												style={{ marginLeft: 10 }}
												// onClick={() =>
												// 	activeGallery({
												// 		variables: {
												// 			key: gallery.key,
												// 		},
												// 	})
												// }
											>
												Идэвхжүүлэх
											</button>
										</div>
									</td>
								</tr>
							);
						})}
				</tbody>
			</table>
			{hasMore && (
				<div className="has-text-centered">
					<button
						className="button is-small is-uppercase"
						style={{
							display: "inline-block",
							width: "100%",
							backgroundColor: "transparent",
							border: "dashed 1px #ccc",
							height: 50,
							color: "#000",
							padding: 10,
						}}
						onClick={loadMore}
					>
						Цааш үзэх
						<i className="fa fa-chevron-down" style={{ marginLeft: 10 }} />
					</button>
				</div>
			)}
		</div>
	);
};

export default Gallery;
