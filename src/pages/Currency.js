import React from "react";

const Currency = () => {
	return (
		<div className="box container" style={{ maxWidth: "100%" }}>
			<div
				className="is-flex buttons"
				style={{
					justifyContent: "space-between",
					paddingBottom: 10,
					marginBottom: 20,
					borderBottom: "1px solid #ededed",
				}}
			>
				<h4 className="is-size-6 is-uppercase has-text-weight-bold">Ханш</h4>
			</div>
		</div>
	);
};

export default Currency;
