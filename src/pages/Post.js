import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { GET_LANGUAGES, GET_POSTS } from "../graphql/queries";
import { useQuery, useMutation } from "react-apollo";
import Modal from "../components/Modal";
import CatsTreeModal from "../components/CatsTreeModal";
import BoxLoader from "../components/BoxLoader";
import AcceptModal from "../components/AcceptModal";
import { DELETE_POST } from "../graphql/mutations";

const Post = () => {
	const [language, setLanguage] = useState("mn");
	const [hasMore, setHasMore] = useState(true);
	const [cat, setCat] = useState(null);
	const [pickCat, setPickCat] = useState(false);
	const [isDelete, setIsDelete] = useState(false);
	const [selectedPost, setSelectedPost] = useState();

	const { data: languageData } = useQuery(GET_LANGUAGES);

	const { data: postsData, loading, refetch, fetchMore } = useQuery(GET_POSTS, {
		variables: { language: language, catKey: cat ? cat.key : null },
	});

	useEffect(() => {
		if (postsData) {
			if (postsData.getPosts) {
				if (postsData.getPosts.length < 10) {
					setHasMore(false);
				} else {
					setHasMore(true);
				}
			}
		}
	}, [postsData]);

	const [deletePost] = useMutation(DELETE_POST, {
		variables: {
			key: selectedPost?.key,
		},
		onCompleted() {
			refetch();
		},
	});

	const loadMore = () => {
		fetchMore({
			variables: {
				from: postsData.getPosts.length > 0 ? postsData.getPosts[postsData.getPosts.length - 1]._id : null,
				language: language,
				catKey: cat ? cat.key : null,
			},
			updateQuery: (prev, { fetchMoreResult }) => {
				if (!fetchMoreResult) {
					return prev;
				}
				if (fetchMoreResult.getPosts.length === 0) {
					setHasMore(false);
				}
				return Object.assign({}, prev, {
					getPosts: [...prev.getPosts, ...fetchMoreResult.getPosts],
				});
			},
		});
	};

	const selectCatHandler = (cat) => {
		setCat(cat);
		modalHandler(!pickCat);
	};

	const deleteModalHandler = () => {
		setIsDelete(!isDelete);
	};

	const modalHandler = () => {
		setPickCat(!pickCat);
	};

	useEffect(() => {
		document.title = "Нийтлэлүүд";
	}, []);

	return (
		<div className="container box" style={{ maxWidth: "100%" }}>
			{loading && <BoxLoader />}
			<Modal active={pickCat} modalHandler={modalHandler}>
				<CatsTreeModal
					type="news"
					language="mn"
					modalHandler={modalHandler}
					onSelect={selectCatHandler}
					isFilter={true}
				/>
			</Modal>
			<Modal active={isDelete} modalHandler={deleteModalHandler}>
				<AcceptModal message="Устгахыг зөвшөөрч байна уу?" modalHandler={deleteModalHandler} yes={deletePost} />
			</Modal>
			<div
				className="is-flex buttons"
				style={{
					justifyContent: "space-between",
					paddingBottom: 10,
					marginBottom: 20,
					borderBottom: "1px solid #ededed",
				}}
			>
				<h4 className="is-size-6 is-uppercase has-text-weight-bold">Нийтлэлүүд</h4>
				<div>
					<div className="select" style={{ marginRight: 10 }}>
						<select value={language} onChange={(e) => setLanguage(e.target.value)}>
							{languageData?.getLanguages.map((language) => {
								return (
									<option value={language.slug} key={language.slug}>
										{language.country}
									</option>
								);
							})}
						</select>
					</div>

					<button className="button" onClick={() => setPickCat(true)}>
						<span className="icon is-small">
							<i className="fal fa-filter"></i>
						</span>
						<span>{cat ? cat.name : "Бүх цэс"}</span>
					</button>

					<Link className="button is-success" to="/posts/new">
						Нийтэл нэмэх
					</Link>
				</div>
			</div>

			<table className="table is-fullwidth is-bordered">
				<thead>
					<tr>
						<th>Гарчиг</th>
						<th>Цэс</th>
						<th>Хэл</th>
						<th style={{ width: 1 }}></th>
					</tr>
				</thead>
				{!loading && !postsData?.getPosts.length && (
					<p className="is-size-6 has-text-danger" style={{ margin: 10 }}>
						Нийтлэл байхгүй байна.
					</p>
				)}
				<tbody>
					{postsData?.getPosts &&
						postsData.getPosts.map((post) => {
							return (
								<tr key={post._id}>
									<td>{post.title}</td>
									<td>{post.category.name === " " ? "Бөглөөгүй байна" : post.category.name}</td>
									<td>{post.language}</td>
									<td>
										<div className="is-flex">
											<button
												className="button is-small has-background-danger-light"
												onClick={() => {
													setSelectedPost(post);
													setIsDelete(true);
												}}
											>
												<i className="fas fa-trash-alt has-text-danger" />
											</button>
											<Link
												to={`/posts/detail/${post.key}`}
												className="button is-small"
												style={{ marginLeft: 10 }}
											>
												Харах
											</Link>
										</div>
									</td>
								</tr>
							);
						})}
				</tbody>
			</table>
			{hasMore && (
				<div className="has-text-centered">
					<button
						className="button is-small is-uppercase"
						style={{
							display: "inline-block",
							width: "100%",
							backgroundColor: "transparent",
							border: "dashed 1px #ccc",
							height: 50,
							color: "#000",
							padding: 10,
						}}
						onClick={loadMore}
					>
						Цааш үзэх
						<i className="fa fa-chevron-down" style={{ marginLeft: 10 }} />
					</button>
				</div>
			)}
		</div>
	);
};

export default Post;
