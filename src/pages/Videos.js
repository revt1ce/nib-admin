import React, { useState, useEffect } from "react";
import { useMutation, useQuery } from "react-apollo";
import { CREATE_CONTENT } from "../graphql/mutations";
import { GET_CONTENTS } from "../graphql/queries";
import BoxLoader from "../components/BoxLoader";
import ReactPlayer from "react-player";

const Videos = ({ modalHandler, isModal, onSelect }) => {
	const [uploading, setUploading] = useState(false);
	const [hasMore, setHasMore] = useState(true);

	const { data, loading, fetchMore } = useQuery(GET_CONTENTS, {
		variables: {
			type: "video",
		},
	});

	const [createContent] = useMutation(CREATE_CONTENT, {
		update(cache, { data: { createContent } }) {
			const { getContents } = cache.readQuery({
				query: GET_CONTENTS,
				variables: { type: "video" },
			});

			cache.writeQuery({
				query: GET_CONTENTS,
				variables: { type: "video" },
				data: { getContents: [...getContents, createContent] },
			});
		},
	});

	useEffect(() => {
		if (data?.getContents?.length < 8) {
			setHasMore(false);
		} else {
			setHasMore(true);
		}
	}, [data]);

	function loadMore() {
		fetchMore({
			variables: {
				type: "video",
				from: data.getContents[data.getContents.length - 1]._id,
			},
			updateQuery: (prev, { fetchMoreResult }) => {
				if (!fetchMoreResult) {
					return prev;
				}
				if (fetchMoreResult.getContents.length === 0) {
					setHasMore(false);
				}
				return Object.assign({}, prev, {
					getContents: [...prev.getContents, ...fetchMoreResult.getContents],
				});
			},
		});
	}

	function upload(e) {
		let form = document.getElementById("imageForm");
		let formData = new FormData(form);
		setUploading(true);
		fetch(`http://localhost:8081/upload/videos`, {
			method: "POST",
			body: formData,
			headers: {
				Accept: "*/*",
			},
		})
			.then(function (response) {
				if (response.ok) {
					return response.json();
				}
			})
			.then(function (data) {
				createContent({ variables: { path: data.path, type: "video" } });
			})
			.catch(function (err) {
				console.error(err);
			})
			.finally(function () {
				setUploading(false);
			});
		e.target.value = null;
	}
	if (uploading || loading) return <BoxLoader />;
	return (
		<div className="box container" style={{ maxWidth: "100%" }}>
			<div
				className="is-flex buttons"
				style={{ justifyContent: "space-between", borderBottom: "solid 1px #ededed", paddingBottom: 15 }}
			>
				<div className="is-flex" style={{ alignItems: "center" }}>
					{isModal && (
						<span
							className="button"
							onClick={() => modalHandler(false)}
							style={{ border: "none", padding: 0, margin: 0 }}
						>
							<i
								className="fa fa-chevron-left"
								style={{
									marginRight: 10,
									padding: "5px 10px",
									position: "relative",
									top: "-2px",
									borderRadius: 3,
									backgroundColor: "rgba(0,0,0,0.1)",
								}}
							/>
						</span>
					)}
					<h4 className="is-size-6 is-uppercase has-text-weight-bold">Видео</h4>
				</div>
				<form encType="multipart/form-data" id="imageForm" style={{ padding: 0, border: 0 }}>
					<div className="file">
						<label className="file-label">
							<input multiple={false} className="file-input" type="file" name="file" onChange={upload} />
							<span
								className="button is-small is-uppercase"
								style={{
									display: "inline-block",
									width: "100%",
									backgroundColor: "transparent",
									border: "dashed 1px #48c774",
									height: 40,
									color: "#48c774",
									padding: 10,
									margin: 0,
								}}
							>
								<i className="fal fa-upload" style={{ marginRight: 10 }}></i>Видео хуулах
							</span>
						</label>
					</div>
				</form>
			</div>
			<div
				className="is-flex"
				style={{
					flexWrap: "wrap",
					justifyContent: "flex-start",
					marginBottom: "1rem",
					overflowY: "auto",
					maxHeight: isModal ? "70vh" : "auto",
					backgroundColor: "#ededed",
				}}
			>
				{data?.getContents.map((video) => (
					<ListVideo video={video} key={video._id} onSelect={isModal && onSelect} />
				))}
			</div>
			{hasMore && (
				<div className="has-text-centered">
					<button
						className="button is-small is-uppercase"
						style={{
							display: "inline-block",
							width: "100%",
							backgroundColor: "transparent",
							border: "dashed 1px #ccc",
							height: 50,
							color: "#000",
							padding: 10,
						}}
						onClick={loadMore}
					>
						Цааш үзэх
						<i className="fa fa-chevron-down" style={{ marginLeft: 10 }} />
					</button>
				</div>
			)}
		</div>
	);
};

function ListVideo({ video }) {
	return (
		<div style={{ margin: 10 }}>
			<ReactPlayer
				style={{ backgroundColor: "#232880" }}
				controls={true}
				url={`http://localhost:8081/${video.path.slice(6)}`}
			/>
		</div>
	);
}

export default Videos;
