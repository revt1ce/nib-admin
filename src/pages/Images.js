import React, { useState, useEffect } from "react";
import { useMutation, useQuery } from "react-apollo";
import { CREATE_CONTENT, UPDATE_CONTENT } from "../graphql/mutations";
import { GET_CONTENTS } from "../graphql/queries";
import BoxLoader from "../components/BoxLoader";

const Images = ({ modalHandler, isModal, onSelect }) => {
	const [uploading, setUploading] = useState(false);
	const [hasMore, setHasMore] = useState(true);

	const { data, loading, fetchMore } = useQuery(GET_CONTENTS, {
		variables: {
			type: "image",
		},
	});

	const [createContent] = useMutation(CREATE_CONTENT, {
		update(cache, { data: { createContent } }) {
			const { getContents } = cache.readQuery({
				query: GET_CONTENTS,
				variables: { type: "image" },
			});

			cache.writeQuery({
				query: GET_CONTENTS,
				variables: { type: "image" },
				data: { getContents: [...getContents, createContent] },
			});
		},
	});

	useEffect(() => {
		if (data?.getContents?.length < 8) {
			setHasMore(false);
		} else {
			setHasMore(true);
		}
	}, [data]);

	function loadMore() {
		fetchMore({
			variables: {
				type: "image",
				from: data.getContents[data.getContents.length - 1]._id,
			},
			updateQuery: (prev, { fetchMoreResult }) => {
				if (!fetchMoreResult) {
					return prev;
				}
				if (fetchMoreResult.getContents.length === 0) {
					setHasMore(false);
				}
				return Object.assign({}, prev, {
					getContents: [...prev.getContents, ...fetchMoreResult.getContents],
				});
			},
		});
	}

	function upload(e) {
		let form = document.getElementById("imageForm");
		let formData = new FormData(form);
		setUploading(true);
		fetch(`http://localhost:8081/upload/images`, {
			method: "POST",
			body: formData,
			headers: {
				Accept: "*/*",
			},
		})
			.then(function (response) {
				if (response.ok) {
					return response.json();
				}
			})
			.then(function (data) {
				data.forEach((path) => createContent({ variables: { path: path, type: "image" } }));
			})
			.catch(function (err) {
				console.error(err);
			})
			.finally(function () {
				setUploading(false);
			});
		e.target.value = null;
	}
	if (uploading || loading) return <BoxLoader />;
	return (
		<div className="box container" style={{ maxWidth: "100%" }}>
			<div
				className="is-flex buttons"
				style={{ justifyContent: "space-between", borderBottom: "solid 1px #ededed", paddingBottom: 15 }}
			>
				<div className="is-flex" style={{ alignItems: "center" }}>
					{isModal && (
						<span
							className="button"
							onClick={() => modalHandler(false)}
							style={{ border: "none", padding: 0, margin: 0 }}
						>
							<i
								className="fa fa-chevron-left"
								style={{
									marginRight: 10,
									padding: "5px 10px",
									position: "relative",
									top: "-2px",
									borderRadius: 3,
									backgroundColor: "rgba(0,0,0,0.1)",
								}}
							/>
						</span>
					)}
					<h4 className="is-size-6 is-uppercase has-text-weight-bold">Зурагнууд</h4>
				</div>
				<form encType="multipart/form-data" id="imageForm" style={{ padding: 0, border: 0 }}>
					<div className="file">
						<label className="file-label">
							<input multiple={true} className="file-input" type="file" name="files" onChange={upload} />
							<span
								className="button is-small is-uppercase"
								style={{
									display: "inline-block",
									width: "100%",
									backgroundColor: "transparent",
									border: "dashed 1px #48c774",
									height: 40,
									color: "#48c774",
									padding: 10,
									margin: 0,
								}}
							>
								<i className="fal fa-upload" style={{ marginRight: 10 }}></i>Зураг хуулах
							</span>
						</label>
					</div>
				</form>
			</div>
			<div
				className="is-flex"
				style={{
					flexWrap: "wrap",
					justifyContent: "flex-start",
					marginBottom: "1rem",
					overflowY: "auto",
					maxHeight: isModal ? "70vh" : "auto",
					backgroundColor: "#ededed",
				}}
			>
				{data?.getContents.map((image) => (
					<ListImage image={image} key={image._id} onSelect={isModal && onSelect} />
				))}
			</div>
			{hasMore && (
				<div className="has-text-centered">
					<button
						className="button is-small is-uppercase"
						style={{
							display: "inline-block",
							width: "100%",
							backgroundColor: "transparent",
							border: "dashed 1px #ccc",
							height: 50,
							color: "#000",
							padding: 10,
						}}
						onClick={loadMore}
					>
						Цааш үзэх
						<i className="fa fa-chevron-down" style={{ marginLeft: 10 }} />
					</button>
				</div>
			)}
		</div>
	);
};

function ListImage({ image, onSelect }) {
	const [updateImage] = useMutation(UPDATE_CONTENT, {
		onError(error) {
			alert(error?.graphQLErrors[0]?.message);
		},
	});

	useEffect(() => {
		if (image.width === null || image.height === null) {
			let img = new Image();
			img.onload = function () {
				updateImage({
					variables: {
						_id: image._id,
						width: this.width,
						height: this.height,
					},
				});
			};
			img.src = `http://localhost:8081/${image.path.slice(6)}`;
		}
		// eslint-disable-next-line
	}, [image.width, image.height]);

	return (
		<figure
			className="image"
			onClick={() => onSelect && onSelect(image)}
			style={{
				cursor: "pointer",
				height: 150,
				width: 150,
				margin: "1rem",
				boxSizing: "border-box",
				backgroundSize: "90%",
				backgroundPosition: "center",
				border: "solid 1px white",
				borderRadius: "3px",
				backgroundColor: "#fff",
				backgroundRepeat: "no-repeat",
				backgroundImage: `url('http://localhost:8081/${image.path.slice(6)}')`,
			}}
		></figure>
	);
}

export default Images;
