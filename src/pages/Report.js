import React, { useEffect, useState } from "react";
import { useMutation, useQuery } from "react-apollo";
import AcceptModal from "../components/AcceptModal";
import BoxLoader from "../components/BoxLoader";
import CatsTreeModal from "../components/CatsTreeModal";
import Modal from "../components/Modal";
import NewReportModal from "../components/NewReportModal";
import ReportDetailModal from "../components/ReportDetailModal";
import { DELETE_REPORT } from "../graphql/mutations";
import { GET_LANGUAGES, GET_REPORTS } from "../graphql/queries";

const Report = () => {
	const [language, setLanguage] = useState("mn");
	const [hasMore, setHasMore] = useState(true);
	const [isNew, setIsNew] = useState(false);
	const [isDetail, setIsDetail] = useState(false);
	const [isDelete, setIsDelete] = useState(false);
	const [cat, setCat] = useState(null);
	const [pickCat, setPickCat] = useState(false);
	const [selectedReport, setSelectedReport] = useState(null);
	const { data: languageData } = useQuery(GET_LANGUAGES);
	const { data, loading, refetch, fetchMore } = useQuery(GET_REPORTS, {
		variables: {
			catKey: cat?.key,
			language: language,
		},
	});
	useEffect(() => {
		if (data?.getReports?.length < 10) {
			setHasMore(false);
		} else {
			setHasMore(true);
		}
	}, [data]);

	const loadMore = () => {
		fetchMore({
			variables: {
				from: data.getReports.length > 0 ? data.getReports[data.getReports.length - 1]._id : null,
				language: language,
				catKey: cat?.key,
			},
			updateQuery: (prev, { fetchMoreResult }) => {
				if (!fetchMoreResult) {
					return prev;
				}
				if (fetchMoreResult.getReports.length === 0) {
					setHasMore(false);
				}
				return Object.assign({}, prev, {
					getReports: [...prev.getReports, ...fetchMoreResult.getReports],
				});
			},
		});
	};

	const [deleteReport] = useMutation(DELETE_REPORT, {
		variables: {
			key: selectedReport,
		},
		onCompleted() {
			refetch();
		},
	});

	const modalHandler = () => {
		setIsNew(!isNew);
	};
	const catModalHandler = () => {
		setPickCat(!pickCat);
	};
	const deleteModalHandler = () => {
		setIsDelete(!isDelete);
	};
	const detailModalHandler = () => {
		setIsDetail(!isDetail);
	};

	return (
		<div className="box container" style={{ maxWidth: "100%" }}>
			{loading && <BoxLoader />}
			<Modal active={isDetail}>
				<ReportDetailModal modalHandler={detailModalHandler} report={selectedReport} />
			</Modal>
			<Modal active={isDelete}>
				<AcceptModal
					message="Устгахыг зөвшөөрч байна уу?"
					modalHandler={deleteModalHandler}
					yes={deleteReport}
				/>
			</Modal>
			<Modal active={pickCat}>
				<CatsTreeModal
					type="finance"
					language="mn"
					modalHandler={catModalHandler}
					onSelect={(data) => {
						setCat(data);
						setPickCat(!pickCat);
					}}
				/>
			</Modal>
			<Modal active={isNew}>
				<NewReportModal modalHandler={modalHandler} />
			</Modal>
			<div
				className="is-flex buttons"
				style={{
					justifyContent: "space-between",
					paddingBottom: 10,
					marginBottom: 20,
					borderBottom: "1px solid #ededed",
				}}
			>
				<h4 className="is-size-6 is-uppercase has-text-weight-bold">Санхүү</h4>
				<div>
					<div className="select" style={{ marginRight: 10 }}>
						<select value={language} onChange={(e) => setLanguage(e.target.value)}>
							{languageData?.getLanguages.map((language) => {
								return (
									<option value={language.slug} key={language.slug}>
										{language.country}
									</option>
								);
							})}
						</select>
					</div>
					<button className="button" onClick={() => setPickCat(true)}>
						<span className="icon is-small">
							<i className="fal fa-filter"></i>
						</span>
						<span>{cat ? cat.name : "Бүх цэс"}</span>
					</button>

					<button className="button is-success" onClick={() => setIsNew(true)}>
						Шинэ
					</button>
				</div>
			</div>
			<table className="table is-fullwidth is-bordered">
				<thead>
					<tr>
						<th>Hэр</th>
						<th>Төрөл</th>
						<th>Хэл</th>
						<th style={{ width: 1 }}></th>
					</tr>
				</thead>
				{!loading && !data?.getReports.length && (
					<p className="is-size-6 has-text-danger" style={{ margin: 10 }}>
						Тайлан байхгүй байна.
					</p>
				)}
				<tbody>
					{data?.getReports &&
						data.getReports.map((report) => {
							return (
								<tr key={report._id}>
									<td>{report.title === " " ? "Бөглөөгүй байна" : report.title}</td>
									<td>{report.category.name}</td>
									<td>{report.language}</td>
									<td>
										<div className="is-flex">
											<button
												className="button is-small has-background-danger-light"
												onClick={() => {
													setSelectedReport(report.key);
													setIsDelete(true);
												}}
											>
												<i className="fas fa-trash-alt has-text-danger" />
											</button>
											<button
												className="button is-small"
												style={{ marginLeft: 10 }}
												onClick={() => {
													setSelectedReport(report);
													setIsDetail(true);
												}}
											>
												Засах
											</button>
										</div>
									</td>
								</tr>
							);
						})}
				</tbody>
			</table>
			{hasMore && (
				<div className="has-text-centered">
					<button
						className="button is-small is-uppercase"
						style={{
							display: "inline-block",
							width: "100%",
							backgroundColor: "transparent",
							border: "dashed 1px #ccc",
							height: 50,
							color: "#000",
							padding: 10,
						}}
						onClick={loadMore}
					>
						Цааш үзэх
						<i className="fa fa-chevron-down" style={{ marginLeft: 10 }} />
					</button>
				</div>
			)}
		</div>
	);
};

export default Report;
