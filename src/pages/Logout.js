import React, { useEffect } from "react";
import BoxLoader from "../components/BoxLoader";

const Logout = () => {
	useEffect(() => {
		document.title = "...";
		localStorage.removeItem("Token");
		setTimeout(() => {
			window.location = "/";
		}, 1000);
		// eslint-disable-next-line
	}, []);

	return <BoxLoader />;
};

export default Logout;
