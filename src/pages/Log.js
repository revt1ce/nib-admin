import React, { useState } from "react";
import { useQuery } from "react-apollo";
import { GET_LOGS, GET_USERS } from "../graphql/queries";
import moment from "moment";
import "moment/locale/mn";

const Log = () => {
	const types = [
		{
			name: "Үүсгэсэн",
			slug: "create",
		},
		{
			name: "Зассан",
			slug: "update",
		},
		{
			name: "Уншсан",
			slug: "read",
		},
		{
			name: "Устгасан",
			slug: "delete",
		},
		{
			name: "Бүгд",
			slug: "",
		},
	];
	const [user, setUser] = useState("");
	const [hasMore, setHasMore] = useState(true);
	const [type, setType] = useState("");

	const { data: users } = useQuery(GET_USERS);

	const { data, fetchMore } = useQuery(GET_LOGS, {
		variables: {
			method: type ? type : null,
			user: user,
		},
	});
	const loadMore = () => {
		fetchMore({
			variables: {
				from: data.getLogs.length > 0 ? data.getLogs[data.getLogs.length - 1]._id : null,
				method: type ? type : null,
			},
			updateQuery: (prev, { fetchMoreResult }) => {
				if (!fetchMoreResult) {
					return prev;
				}
				if (fetchMoreResult.getLogs.length === 0) {
					setHasMore(false);
				}
				return Object.assign({}, prev, {
					getLogs: [...prev.getLogs, ...fetchMoreResult.getLogs],
				});
			},
		});
	};

	return (
		<div className="box container" style={{ maxWidth: "100%" }}>
			<div
				className="is-flex buttons"
				style={{
					justifyContent: "space-between",
					paddingBottom: 10,
					marginBottom: 20,
					borderBottom: "1px solid #ededed",
				}}
			>
				<h4 className="is-size-6 is-uppercase has-text-weight-bold">лог</h4>
				<div className="is-flex buttons">
					<div className="select" style={{ marginRight: 10, marginBottom: 10 }}>
						<select value={user} onChange={(e) => setUser(e.target.value)}>
							{users?.getUsers.map((user) => {
								return (
									<option value={user.username} key={user.username}>
										{user.username}
									</option>
								);
							})}
							<option value={""}>Бүх хэрэглэгч</option>
						</select>
					</div>
					<div className="select" style={{ marginRight: 10, marginBottom: 10 }}>
						<select value={type} onChange={(e) => setType(e.target.value)}>
							{types.map((type) => {
								return (
									<option value={type.slug} key={type.slug}>
										{type.name}
									</option>
								);
							})}
						</select>
					</div>
				</div>
			</div>
			{data?.getLogs.map((log) => {
				return (
					<article
						key={log._id}
						className={`message is-small 
                        ${log.method === "create" && "is-success"} 
                        ${log.method === "update" && "is-warning"} 
                        ${log.method === "delete" && "is-danger"} 
                        ${log.method === "read" && "is-link"}`}
					>
						<div class="message-body">
							{log.username} {log.name} {log.model && `${log.model}ийг`}{" "}
							{moment(parseInt(log._id.substring(0, 8), 16) * 1000)
								.locale("mn")
								.fromNow()}{" "}
							{log.operation}. ({" "}
							{moment(parseInt(log._id.substring(0, 8), 16) * 1000).format("dddd, MMMM Do YYYY, h:mm")} )
						</div>
					</article>
				);
			})}
			{hasMore && (
				<div className="has-text-centered">
					<button
						className="button is-small is-uppercase"
						style={{
							display: "inline-block",
							width: "100%",
							backgroundColor: "transparent",
							border: "dashed 1px #ccc",
							height: 50,
							color: "#000",
							padding: 10,
						}}
						onClick={loadMore}
					>
						Цааш үзэх
						<i className="fa fa-chevron-down" style={{ marginLeft: 10 }} />
					</button>
				</div>
			)}
		</div>
	);
};

export default Log;
