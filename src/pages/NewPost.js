import React, { useState, useEffect } from "react";
import { Link, useParams } from "react-router-dom";
import CKEditor from "ckeditor4-react";
import ImageField from "../components/ImageField";
import Modal from "../components/Modal";
import CatsTreeModal from "../components/CatsTreeModal";
import { useQuery, useMutation, useLazyQuery } from "react-apollo";
import { GET_CURRENT_USER, GET_LANGUAGES, GET_POSTS, GET_ONE_POST } from "../graphql/queries";
import { CREATE_POST, UPDATE_POST } from "../graphql/mutations";
import Message from "../components/Message";
import BoxLoader from "../components/BoxLoader";

const NewPost = () => {
	const { id } = useParams();
	const [language, setLanguage] = useState("mn");

	const [title, setTitle] = useState("");
	const [description, setDescription] = useState("");
	const [body, setBody] = useState("");
	const [author, setAuthor] = useState("");
	const [images, setImages] = useState([null]);
	const [cat, setCat] = useState("");
	const [pickCat, setPickCat] = useState(false);

	const [message, setMessage] = useState("");
	const [showMessage, setShowMessage] = useState(false);
	const [messageType, setMessageType] = useState("");

	const [disableInputs, setDisableInputs] = useState(false);

	const { data: languageData } = useQuery(GET_LANGUAGES);

	const [getCurrentUser, { data: currentUserData }] = useLazyQuery(GET_CURRENT_USER);

	const [getOnePost, { data: onePostData, loading }] = useLazyQuery(GET_ONE_POST, {
		variables: {
			_id: id,
		},
	});

	useEffect(() => {
		if (id) {
			if (id !== "new") {
				getOnePost();
				setDisableInputs(true);
			} else {
				getCurrentUser();
			}
		}
	}, [id, getOnePost, getCurrentUser]);

	useEffect(() => {
		if (onePostData) {
			if (onePostData.getOnePost) {
				setTitle(onePostData.getOnePost.title);
				setDescription(onePostData.getOnePost.description);
				setBody(onePostData.getOnePost.body);
				setCat(onePostData.getOnePost.category);
				setImages(onePostData.getOnePost.images);
				setAuthor(onePostData.getOnePost.author);
				setLanguage(onePostData.getOnePost.language);
			}
		}
	}, [onePostData]);

	useEffect(() => {
		if (title) {
			document.title = title;
		}
	}, [title]);

	useEffect(() => {
		if (currentUserData?.me) {
			setAuthor(currentUserData.me.username);
		}
	}, [currentUserData]);

	const [updatePost] = useMutation(UPDATE_POST, {
		onCompleted() {
			setMessageType("is-success");
			setMessage("Амжилттай хадгалагдлаа!");
			setShowMessage(true);
		},
		onError(error) {
			setMessageType("is-danger");
			setMessage(error?.graphQLErrors[0]?.message);
			setShowMessage(true);
		},
	});

	const [createPost] = useMutation(CREATE_POST, {
		onCompleted() {
			setTitle("");
			setDescription("");
			setBody("");
			setImages([null]);
			setCat("");
			setMessageType("is-success");
			setMessage("Амжилттай нэмэгдлээ!");
			setShowMessage(true);
		},
		onError(error) {
			setMessageType("is-danger");
			setMessage(error?.graphQLErrors[0]?.message);
			setShowMessage(true);
		},
		update(cache, { data: { createPost } }) {
			const { getPosts } = cache.readQuery({
				query: GET_POSTS,
				variables: { language: createPost.language, catKey: null },
			});

			cache.writeQuery({
				query: GET_POSTS,
				variables: { language: createPost.language, catKey: null },
				data: { getPosts: [...getPosts, createPost] },
			});
		},
	});

	useEffect(() => {
		console.log(cat);
	}, [cat]);

	const submit = () => {
		if (!cat) {
			setMessageType("is-danger");
			setMessage("Цэс сонгоогүй байна!");
			setShowMessage(true);
		} else {
			if (id !== "new") {
				let sendImages = images
					.filter((image) => image !== null)
					.map((oneImage) => {
						return {
							_id: oneImage._id,
							path: oneImage.path,
							width: oneImage.width,
							height: oneImage.height,
						};
					});
				updatePost({
					variables: {
						_id: id,
						title: title,
						description: description,
						body: body,
						catKey: cat?.key,
						images: sendImages,
						author: author,
					},
				});
			} else {
				createPost({
					variables: {
						title: title,
						description: description,
						body: body,
						catKey: cat && cat.key,
						images: images.filter((image) => image !== null),
						author: author,
						language: language,
					},
				});
			}
		}
	};

	function addNewImage() {
		const newImages = [...images, null];
		setImages(newImages);
	}
	function updateImages(image, index) {
		const newImages = [...images];
		newImages[index] = image;
		setImages(newImages);
	}
	function deleteImage(index) {
		if (images.length > 1) {
			images.splice(index, 1);
			setImages([...images]);
		} else {
			updateImages(null, index);
		}
	}

	const selectCatHandler = (cat) => {
		setCat(cat);
		modalHandler(!pickCat);
	};

	const modalHandler = () => {
		setPickCat(!pickCat);
	};

	useEffect(() => {
		if (showMessage) {
			setTimeout(() => {
				setShowMessage(false);
			}, [4000]);
		}
	}, [showMessage]);

	return (
		<div className="container box" style={{ maxWidth: "100%" }}>
			{loading && <BoxLoader />}
			<Modal active={pickCat} modalHandler={modalHandler}>
				<CatsTreeModal
					type="news"
					language={language}
					modalHandler={modalHandler}
					onSelect={selectCatHandler}
				/>
			</Modal>
			<div
				className="is-flex"
				style={{
					justifyContent: "space-between",
					paddingBottom: 10,
					marginBottom: 20,
					borderBottom: "1px solid #ededed",
				}}
			>
				<h4 className="subtitle">{id !== "new" ? "Нийтлэл засах" : "Нийтлэл оруулах"}</h4>
				<Link
					className="button is-danger is-light"
					to={id !== "new" ? `/posts/detail/${onePostData?.getOnePost?.key}` : "/posts"}
				>
					Буцах
				</Link>
			</div>
			{showMessage && <Message message={message} type={messageType} />}
			<div className="columns">
				<div className="column is-8">
					<div className="field">
						<label className="label">Нийтлэгч</label>
						<div className="control is-flex">
							<input className="input" disabled={true} value={author} />
						</div>
					</div>
					<div className="is-flex" style={{ justifyContent: "space-between" }}>
						<div className="field" style={{ width: "45%" }}>
							<label className="label">Цэс</label>
							<div className="control is-flex">
								<input className="input" disabled={true} value={cat && cat.name} type="text" required />
								<button
									style={{ width: "20%" }}
									className="button"
									onClick={() => setPickCat(true)}
									disabled={disableInputs}
								>
									Сонгох
								</button>
							</div>
						</div>
						<div className="field" style={{ width: "45%" }}>
							<label className="label">Хэл</label>
							<div className="select" style={{ marginRight: 10, width: "100%" }}>
								<select
									value={language}
									onChange={(e) => setLanguage(e.target.value)}
									style={{ width: "100%" }}
									disabled={disableInputs}
								>
									{languageData?.getLanguages.map((language) => {
										return (
											<option value={language.slug} key={language.slug}>
												{language.country}
											</option>
										);
									})}
								</select>
							</div>
						</div>
					</div>
					<div className="field">
						<label className="label">Гарчиг</label>
						<div className="control">
							<input
								className="input"
								type="text"
								value={title}
								onChange={(e) => setTitle(e.target.value)}
							/>
						</div>
					</div>
					<div className="field">
						<label className="label">Тайлбар</label>
						<div className="control">
							<textarea
								className="textarea"
								type="text"
								value={description}
								onChange={(e) => setDescription(e.target.value)}
							/>
						</div>
					</div>
					<div className="field">
						<label className="label">Агуулга</label>
						<div className="control">
							<CKEditor data={body} onChange={(evt) => setBody(evt.editor.getData())} />
						</div>
					</div>
				</div>
				<div className="column is-4 has-background-white-ter" style={{ borderRadius: 3 }}>
					<div className="field" style={{ padding: 15 }}>
						<label className="label">Зураг</label>
						{images.map((image, index) => (
							<ImageField
								key={index}
								value={image}
								onChange={(val) => updateImages(val, index)}
								onDelete={() => deleteImage(index)}
							/>
						))}
						<button
							className="button is-small is-uppercase"
							style={{
								display: "inline-block",
								width: "100%",
								backgroundColor: "transparent",
								border: "dashed 1px #48c774",
								height: 40,
								color: "#48c774",
							}}
							onClick={addNewImage}
						>
							<i className="fa fa-plus" style={{ marginRight: 10 }}></i> Зураг нэмэх
						</button>
					</div>
				</div>
			</div>

			<button className="button is-success" onClick={() => submit()}>
				{id !== "new" ? "Хадгалах" : "Нэмэх"}
			</button>
		</div>
	);
};

export default NewPost;
