import CKEditor from "ckeditor4-react";
import React, { useEffect, useState } from "react";
import { useMutation, useLazyQuery } from "react-apollo";
import { Link, useParams } from "react-router-dom";
import ArticleTabItem from "../components/ArticleTabItem";
import BoxLoader from "../components/BoxLoader";
import ComponentTypeModal from "../components/ComponentTypeModal";
import ImageField from "../components/ImageField";
import Message from "../components/Message";
import Modal from "../components/Modal";
import { CREATE_ARTICLE, UPDATE_ARTICLE } from "../graphql/mutations";
import { GET_ARTICLES, GET_CURRENT_USER, GET_ONE_ARTICLE } from "../graphql/queries";

const NewArticle = () => {
	const { id } = useParams();
	const [title, setTitle] = useState("");
	const [description, setDescription] = useState("");
	const [body, setBody] = useState("");
	const [tabs, setTabs] = useState([null]);
	const [image, setImage] = useState(null);
	const [internal, setInternal] = useState("0");
	const [external, setExternal] = useState("0");
	const [author, setAuthor] = useState("");
	const [type, setType] = useState();
	const [pickType, setPickType] = useState(false);

	const [message, setMessage] = useState("");
	const [messageType, setMessageType] = useState("");
	const [showMessage, setShowMessage] = useState(false);

	const [getCurrentUser, { data: currentUserData }] = useLazyQuery(GET_CURRENT_USER);
	const [getOneArticle, { data: oneArticleData, loading: oneArticleLoading }] = useLazyQuery(GET_ONE_ARTICLE, {
		variables: { _id: id },
	});

	const [createArticle, { loading: creating }] = useMutation(CREATE_ARTICLE, {
		onCompleted() {
			window.scrollTo(0, 0);
			setMessage("Амжилттай нэмэгдлээ!");
			setMessageType("is-success");
			setShowMessage(true);

			setTitle("");
			setDescription("");
			setBody("");
			setTabs([null]);
			setImage(null);
			setInternal("");
			setExternal("");
			setType("");
		},
		update(cache, { data: { createArticle } }) {
			const { getArticles } = cache.readQuery({
				query: GET_ARTICLES,
				variables: { language: "mn" },
			});

			cache.writeQuery({
				query: GET_ARTICLES,
				variables: { language: "mn" },
				data: { getArticles: [...getArticles, createArticle] },
			});
		},
	});

	const [updateArticle] = useMutation(UPDATE_ARTICLE, {
		onCompleted() {
			window.scrollTo(0, 0);
			setMessage("Амжилттай хадгалагдлаа!");
			setMessageType("is-success");
			setShowMessage(true);
		},
	});

	const submit = () => {
		if (id !== "new") {
			updateArticle({
				variables: {
					_id: id,
					title: title,
					description: description,
					body: body,
					internal: internal,
					external: external,
					image: image
						? {
								_id: image._id,
								path: image.path,
								width: image.width,
								height: image.height,
								type: image.type,
						  }
						: null,
					tabs: tabs
						.filter((tab) => tab !== null)
						.map((oneTab) => {
							return {
								title: oneTab.title,
								description: oneTab.description,
								body: oneTab.body,
								image: oneTab.image
									? {
											_id: oneTab.image._id,
											path: oneTab.image.path,
											width: oneTab.image.width,
											height: oneTab.image.height,
											type: oneTab.image.type,
									  }
									: null,
							};
						}),
				},
			});
		} else {
			createArticle({
				variables: {
					title: title,
					description: description,
					body: body,
					internal: internal,
					external: external,
					author: author,
					type: type,
					image: image
						? {
								_id: image._id,
								path: image.path,
								width: image.width,
								height: image.height,
								type: image.type,
						  }
						: null,
					tabs: tabs
						.filter((tab) => tab !== null)
						.map((oneTab) => {
							return {
								title: oneTab.title,
								description: oneTab.description,
								body: oneTab.body,
								image: oneTab.image
									? {
											_id: oneTab.image._id,
											path: oneTab.image.path,
											width: oneTab.image.width,
											height: oneTab.image.height,
											type: oneTab.image.type,
									  }
									: null,
							};
						}),
				},
			});
		}
	};

	useEffect(() => {
		if (id !== "new") {
			getOneArticle();
		} else {
			getCurrentUser();
		}
	}, [id, getOneArticle, getCurrentUser]);

	useEffect(() => {
		if (currentUserData) {
			setAuthor(currentUserData.me.username);
		}
	}, [currentUserData]);

	useEffect(() => {
		if (oneArticleData) {
			if (oneArticleData.getOneArticle) {
				setTitle(oneArticleData.getOneArticle.title);
				setDescription(oneArticleData.getOneArticle.description);
				setBody(oneArticleData.getOneArticle.body);
				setTabs(oneArticleData.getOneArticle.tabs);
				setImage(oneArticleData.getOneArticle.image);
				setInternal(oneArticleData.getOneArticle.internal);
				setExternal(oneArticleData.getOneArticle.external);
				setType(oneArticleData.getOneArticle.type);
				setAuthor(oneArticleData.getOneArticle.author);
			}
		}
	}, [oneArticleData]);

	const updateImage = (image) => {
		setImage(image);
	};
	const addOneTab = () => {
		const newTabs = [...tabs, null];
		setTabs(newTabs);
	};
	const deleteTab = (index) => {
		if (tabs.length > 1) {
			tabs.splice(index, 1);
			setTabs([...tabs]);
		} else {
			updateTab(null, index);
		}
	};
	const updateTab = (tab, index) => {
		const newTabs = [...tabs];
		newTabs[index] = tab;
		setTabs(newTabs);
	};
	const typeModalHandler = () => {
		setPickType(!pickType);
	};
	const typeSubmitHandler = (type) => {
		setType(type);
	};

	return (
		<div className="box container" style={{ maxWidth: "100%" }}>
			{oneArticleLoading && <BoxLoader />}
			<Modal active={pickType} width={1000}>
				<ComponentTypeModal modalHandler={typeModalHandler} onSubmit={typeSubmitHandler} />
			</Modal>
			{showMessage && <Message message={message} type={messageType} />}
			<div
				className="is-flex"
				style={{
					justifyContent: "space-between",
					paddingBottom: 10,
					marginBottom: 20,
					borderBottom: "1px solid #ededed",
				}}
			>
				<h4 className="subtitle">Бүтээгдэхүүн оруулах</h4>
				<Link
					className="button is-danger is-light"
					to={id === "new" ? "/article" : `/article/detail/${oneArticleData?.getOneArticle.key}`}
				>
					Буцах
				</Link>
			</div>
			<div className="columns">
				<div className="column is-8">
					<div className="is-flex" style={{ justifyContent: "space-between" }}>
						<div className="field" style={{ width: "50%" }}>
							<label className="label">Нийтлэгч</label>
							<div className="control is-flex">
								<input className="input" disabled={true} value={author} />
							</div>
						</div>
						<div className="field" style={{ width: "45%" }}>
							<label className="label">Төрөл</label>
							<div className="is-flex">
								<input className="input" value={type} disabled={true} style={{ width: "50%" }} />
								<button
									className="button"
									style={{ width: 100, marginLeft: 20 }}
									onClick={() => setPickType(true)}
								>
									Сонгох
								</button>
							</div>
						</div>
					</div>

					<div className="is-flex" style={{ justifyContent: "space-between" }}>
						<div className="field" style={{ width: "50%" }}>
							<label className="label">Гарчиг</label>
							<div className="control">
								<input
									className="input"
									type="text"
									value={title}
									onChange={(e) => setTitle(e.target.value)}
									required
								/>
							</div>
						</div>
						<div className="is-flex" style={{ width: "45%", justifyContent: "start" }}>
							<div className="field" style={{ width: "20%" }}>
								<label className="label">Дотоод</label>
								<div className="control">
									<input
										className="input"
										type="number"
										value={internal}
										onChange={(e) => setInternal(e.target.value)}
										required
									/>
								</div>
							</div>
							<div className="field" style={{ width: "20%", marginLeft: 20 }}>
								<label className="label">Гадаад</label>
								<div className="control">
									<input
										className="input"
										type="number"
										value={external}
										onChange={(e) => setExternal(e.target.value)}
										required
									/>
								</div>
							</div>
						</div>
					</div>
					<div className="field">
						<label className="label">Тайлбар</label>
						<div className="control">
							<textarea
								className="textarea"
								type="text"
								value={description}
								onChange={(e) => setDescription(e.target.value)}
								required
							/>
						</div>
					</div>
					<div className="field">
						<label className="label">Агуулга</label>
						<div className="control">
							<CKEditor data={body} onChange={(evt) => setBody(evt.editor.getData())} />
						</div>
					</div>
					<div className="field">
						<label className="label">Зураг</label>
						<ImageField
							size={150}
							value={image}
							onChange={(image) => updateImage(image)}
							onDelete={() => updateImage(null)}
						/>
					</div>
				</div>
				<div className="column is-4 has-background-white-ter" style={{ borderRadius: 3 }}>
					<div className="field" style={{ padding: 15 }}>
						<label className="label">Табууд</label>
						{tabs.map((tab, index) => {
							return (
								<ArticleTabItem
									key={index}
									value={tab}
									onDelete={() => deleteTab(index)}
									onChange={(data) => updateTab(data, index)}
								/>
							);
						})}
						<span
							className="button is-small is-uppercase"
							style={{
								width: "100%",
								backgroundColor: "transparent",
								border: "dashed 1px #48c774",
								height: 40,
								color: "#48c774",
							}}
							onClick={() => addOneTab()}
						>
							<i className="fa fa-plus" style={{ marginRight: 10 }}></i> Таб нэмэх
						</span>
					</div>
				</div>
			</div>
			<div className="is-flex" style={{ justifyContent: "space-between" }}>
				<div />
				<div className="buttons">
					<button className="button is-success" onClick={() => submit()} disabled={creating}>
						{id === "new" ? "Нэмэх" : "Хадгалах"}
					</button>
				</div>
			</div>
		</div>
	);
};

export default NewArticle;
