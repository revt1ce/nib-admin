import React, { useState } from "react";
import { useQuery } from "react-apollo";
import { GET_CATS, GET_LANGUAGES } from "../graphql/queries";
import Tree from "../components/Tree";
import BoxLoader from "../components/BoxLoader";
import NewCatModal from "../components/NewCatModal";
import Modal from "../components/Modal";

const Cats = () => {
	const [language, setLanguage] = useState("mn");
	const [newCat, setNewCat] = useState(false);
	const { data: languageData } = useQuery(GET_LANGUAGES);

	const { data, loading } = useQuery(GET_CATS, {
		variables: {
			language: language,
			type: null,
		},
	});

	const modalHandler = () => {
		setNewCat(!newCat);
	};
	if (loading) return <BoxLoader />;
	return (
		<div className="container box" style={{ maxWidth: "100%" }}>
			<Modal active={newCat} modalHandler={modalHandler}>
				<NewCatModal isRoot={true} modalHandler={modalHandler} />
			</Modal>

			<div className="is-flex buttons" style={{ justifyContent: "space-between" }}>
				<h4 className="is-size-6">Цэсүүд</h4>
				<div>
					<div className="select" style={{ marginRight: 10 }}>
						<select value={language} onChange={(e) => setLanguage(e.target.value)}>
							{languageData?.getLanguages.map((language) => {
								return (
									<option value={language.slug} key={language.slug}>
										{language.country}
									</option>
								);
							})}
						</select>
					</div>

					<button className="button is-success" onClick={() => setNewCat(true)}>
						Шинэ цэс
					</button>
				</div>
			</div>
			<aside className="menu">
				{data?.getCats &&
					data?.getCats.map((cat) => {
						return (
							<ul className="menu-list" key={cat._id}>
								<Tree cat={cat} language={language} isModal={false} type="cat" />
							</ul>
						);
					})}
			</aside>
		</div>
	);
};

export default Cats;
