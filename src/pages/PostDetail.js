import React, { useEffect, useState } from "react";
import { useParams, Link } from "react-router-dom";
import { useQuery, useMutation } from "react-apollo";
import { GET_POSTS_KEY, GET_CURRENT_USER } from "../graphql/queries";
import BoxLoader from "../components/BoxLoader";
import { DELETE_POST, CREATE_POST_LANGUAGE } from "../graphql/mutations";
import Modal from "../components/Modal";
import AcceptModal from "../components/AcceptModal";

const PostDetail = () => {
	const { key } = useParams();
	const [buttons, setButtons] = useState([]);
	const [selectedPost, setSelectedPost] = useState(null);
	const [isDelete, setIsDelete] = useState(false);
	const [author, setAuthor] = useState("");

	useQuery(GET_CURRENT_USER, {
		onCompleted(data) {
			setAuthor(data.me.username);
		},
	});

	const { data, loading, refetch } = useQuery(GET_POSTS_KEY, {
		variables: {
			key: key,
		},
	});

	const findMissingLanguages = (lang) => {
		const languages = new Map([
			[
				"mn",
				{
					title: "Монгол",
					slug: "mn",
				},
			],
			[
				"en",
				{
					title: "Англи",
					slug: "en",
				},
			],
			[
				"jp",
				{
					title: "Япон",
					slug: "jp",
				},
			],
			[
				"cn",
				{
					title: "Хятад",
					slug: "cn",
				},
			],
		]);
		lang.forEach((oneLang) => languages.delete(oneLang));
		setButtons(Array.from(languages.values()));
	};

	const [deletePost] = useMutation(DELETE_POST, {
		variables: {
			_id: selectedPost?._id,
		},
		onCompleted() {
			refetch();
		},
	});
	const [createPostLanguage] = useMutation(CREATE_POST_LANGUAGE, {
		onCompleted() {
			refetch();
		},
	});

	const modalHandler = () => {
		setIsDelete(!isDelete);
	};

	useEffect(() => {
		if (data?.getPostsKey) {
			document.title = data?.getPostsKey[0].title + " дэлгэрэнгүй";
			findMissingLanguages(data.getPostsKey.map((post) => post.language));
		}
	}, [data]);

	useEffect(() => {
		if (buttons) {
		}
	}, [buttons]);

	return (
		<div className="container box" style={{ maxWidth: "100%" }}>
			<Modal active={isDelete} modalHandler={modalHandler}>
				<AcceptModal message="Устгахыг зөвшөөрч байна уу?" modalHandler={modalHandler} yes={deletePost} />
			</Modal>
			{loading && <BoxLoader />}
			<div className="is-flex buttons" style={{ justifyContent: "space-between" }}>
				<Link to="/posts" className="button is-danger is-light">
					Буцах
				</Link>
				<div className="is-flex" style={{ alignItems: "center" }}>
					{buttons.map((button) => {
						return (
							<button
								className="button"
								onClick={() =>
									createPostLanguage({
										variables: {
											key: key,
											author: author,
											catKey: data?.getPostsKey[0].catKey,
											language: button.slug,
										},
									})
								}
							>
								{button.title + " орчуулга нэмэх"}
							</button>
						);
					})}
				</div>
			</div>
			<table className="table is-fullwidth is-bordered">
				<thead>
					<tr>
						<th>Гарчиг</th>
						<th>Цэс</th>
						<th>Хэл</th>
						<th style={{ width: 1 }}></th>
					</tr>
				</thead>
				<tbody>
					{data?.getPostsKey &&
						data.getPostsKey.map((onePost) => {
							return (
								<tr key={onePost._id}>
									<td>{onePost.title === " " ? "Бөглөөгүй байна" : onePost.title}</td>
									<td>{onePost.category.name}</td>
									<td>{onePost.language}</td>
									<td>
										<span className="is-flex">
											<button
												className="button has-background-danger-light is-small"
												onClick={() => {
													setSelectedPost(onePost);
													setIsDelete(true);
												}}
											>
												<i className="fas fa-trash-alt has-text-danger" />
											</button>
											<Link
												to={`/posts/${onePost._id}`}
												style={{ marginLeft: 10 }}
												className={
													onePost.title === " "
														? "button is-small is-danger is-light"
														: "button is-small"
												}
											>
												Засах
											</Link>
										</span>
									</td>
								</tr>
							);
						})}
				</tbody>
			</table>
		</div>
	);
};

export default PostDetail;
