import CKEditor from "ckeditor4-react";
import React, { useEffect, useState } from "react";
import { useLazyQuery, useMutation, useQuery } from "react-apollo";
import { Link, useParams } from "react-router-dom";
import BoxLoader from "../components/BoxLoader";
import ImageField from "../components/ImageField";
import Message from "../components/Message";
import { CREATE_CAPITAL, UPDATE_CAPITAL } from "../graphql/mutations";
import { GET_CAPITAL, GET_TYPES, GET_CURRENT_USER, GET_ONE_CAPITAL } from "../graphql/queries";

const NewCapital = () => {
	const { id } = useParams();

	const [title, setTitle] = useState("");
	const [size, setSize] = useState("");
	const [phonenumber, setPhonenumber] = useState("");
	const [address, setAddress] = useState("");
	const [body, setBody] = useState("");
	const [type, setType] = useState(null);
	const [images, setImages] = useState([null]);
	const [author, setAuthor] = useState("");

	const [message, setMessage] = useState("");
	const [messageType, setMessageType] = useState("");
	const [showMessage, setShowMessage] = useState("");

	const { data: capTypes } = useQuery(GET_TYPES, {
		variables: {
			language: "mn",
			slug: "capital",
		},
		onCompleted(data) {
			setType(data.getTypes[0].key);
		},
	});
	const [getCurrentUser, { data: currentUserData }] = useLazyQuery(GET_CURRENT_USER);
	const [getOneCapital, { data: oneCapitalData, loading: fetchingOneCapital }] = useLazyQuery(GET_ONE_CAPITAL, {
		variables: {
			_id: id,
		},
	});

	const [createCapital, { loading: creating }] = useMutation(CREATE_CAPITAL, {
		onCompleted() {
			setMessage("Амжилттай нэмэгдлээ!");
			setMessageType("is-success");
			setShowMessage(true);

			setTitle("");
			setSize("");
			setPhonenumber("");
			setAddress("");
			setBody("");
			setType("oronsuts");
			setImages([null]);
		},
		update(cache, { data: { createCapital } }) {
			const { getCapitals } = cache.readQuery({
				query: GET_CAPITAL,
				variables: { language: createCapital.language, typeKey: "" },
			});

			cache.writeQuery({
				query: GET_CAPITAL,
				variables: { language: createCapital.language, typeKey: "" },
				data: { getCapitals: [...getCapitals, createCapital] },
			});
		},
	});
	const [updateCapital] = useMutation(UPDATE_CAPITAL, {
		onCompleted() {
			setMessage("Амжилттай хадгалагдлаа!");
			setMessageType("is-success");
			setShowMessage(true);

			setTitle("");
			setSize("");
			setPhonenumber("");
			setAddress("");
			setBody("");
			setType("oronsuts");
			setImages([null]);
		},
	});

	function addNewImage() {
		const newImages = [...images, null];
		setImages(newImages);
	}
	function updateImages(image, index) {
		const newImages = [...images];
		newImages[index] = image;
		setImages(newImages);
	}
	function deleteImage(index) {
		if (images.length > 1) {
			images.splice(index, 1);
			setImages([...images]);
		} else {
			updateImages(null, index);
		}
	}
	const submit = () => {
		if (id !== "new") {
			updateCapital({
				variables: {
					_id: id,
					title: title,
					size: size,
					phonenumber: phonenumber,
					address: address,
					body: body,
					typeKey: type,
					images: images
						.filter((image) => image !== null)
						.map((oneImage) => {
							return {
								_id: oneImage._id,
								path: oneImage.path,
								width: oneImage.width,
								height: oneImage.height,
							};
						}),
					author: author,
				},
			});
		} else {
			createCapital({
				variables: {
					title: title,
					size: size,
					phonenumber: phonenumber,
					address: address,
					body: body,
					typeKey: type,
					images: images.filter((image) => image !== null),
					author: author,
				},
			});
		}
	};
	useEffect(() => {
		if (oneCapitalData) {
			if (oneCapitalData.getOneCapital) {
				setTitle(oneCapitalData.getOneCapital.title);
				setSize(oneCapitalData.getOneCapital.size);
				setPhonenumber(oneCapitalData.getOneCapital.phonenumber);
				setAddress(oneCapitalData.getOneCapital.address);
				setBody(oneCapitalData.getOneCapital.body);
				setType(oneCapitalData.getOneCapital.type.key);
				setImages(oneCapitalData.getOneCapital.images);
				setAuthor(oneCapitalData.getOneCapital.author);
			}
		}
	}, [oneCapitalData]);

	useEffect(() => {
		if (id) {
			if (id !== "new") {
				getOneCapital();
			} else {
				getCurrentUser();
			}
		}
	}, [id, getCurrentUser, getOneCapital]);

	useEffect(() => {
		if (currentUserData?.me) {
			setAuthor(currentUserData.me.username);
		}
	}, [currentUserData]);

	useEffect(() => {
		if (showMessage) {
			setTimeout(() => {
				setShowMessage(false);
			}, [3000]);
		}
	}, [showMessage]);
	useEffect(() => {
		console.log(type);
	}, [type]);

	return (
		<div className="box container" style={{ maxWidth: "100%" }}>
			{fetchingOneCapital && <BoxLoader />}
			<div
				className="is-flex"
				style={{
					justifyContent: "space-between",
					paddingBottom: 10,
					marginBottom: 20,
					borderBottom: "1px solid #ededed",
				}}
			>
				<h4 className="subtitle">{id !== "new" ? "Хөрөнгө засах" : "Хөрөнгө оруулах"}</h4>
				<Link className="button is-danger is-light" to="/capital">
					Буцах
				</Link>
			</div>
			{showMessage && <Message message={message} type={messageType} />}
			<div className="columns">
				<div className="column is-8">
					<div className="field">
						<label className="label">Нийтлэгч</label>
						<div className="control is-flex">
							<input className="input" disabled={true} value={author} />
						</div>
					</div>
					<div className="field">
						<label className="label">Нэр</label>
						<div className="control">
							<input
								className="input"
								type="text"
								value={title}
								onChange={(e) => setTitle(e.target.value)}
							/>
						</div>
					</div>
					<div className="field">
						<label className="label">Байршил</label>
						<div className="control">
							<input
								className="input"
								type="text"
								value={address}
								onChange={(e) => setAddress(e.target.value)}
							/>
						</div>
					</div>
					<div className="is-flex" style={{ justifyContent: "space-between" }}>
						<div className="field" style={{ width: "30%" }}>
							<label className="label">Хэмжээ</label>
							<div className="control">
								<input
									className="input"
									type="number"
									value={size}
									onChange={(e) => setSize(e.target.value)}
								/>
							</div>
						</div>
						<div className="field" style={{ width: "30%" }}>
							<label className="label">Утасны дугаар</label>
							<div className="control">
								<input
									className="input"
									type="number"
									value={phonenumber}
									onChange={(e) => setPhonenumber(e.target.value)}
								/>
							</div>
						</div>
						<div className="field" style={{ width: "30%" }}>
							<label className="label">Төрөл</label>
							<div className="select" style={{ marginRight: 10, width: "100%" }}>
								<select
									value={type}
									onChange={(e) => setType(e.target.value)}
									style={{ width: "100%" }}
								>
									{capTypes?.getTypes.map((type) => {
										return (
											<option value={type.key} key={type.key}>
												{type.title}
											</option>
										);
									})}
								</select>
							</div>
						</div>
					</div>
					<div className="field">
						<label className="label">Агуулга</label>
						<div className="control">
							<CKEditor data={body} onChange={(evt) => setBody(evt.editor.getData())} />
						</div>
					</div>
				</div>
				<div className="column is-4 has-background-white-ter" style={{ borderRadius: 3 }}>
					<div className="field" style={{ padding: 15 }}>
						<label className="label">Зураг</label>
						{images.map((image, index) => (
							<ImageField
								key={index}
								value={image}
								onChange={(val) => updateImages(val, index)}
								onDelete={() => deleteImage(index)}
							/>
						))}
						<button
							className="button is-small is-uppercase"
							style={{
								display: "inline-block",
								width: "100%",
								backgroundColor: "transparent",
								border: "dashed 1px #48c774",
								height: 40,
								color: "#48c774",
							}}
							onClick={addNewImage}
						>
							<i className="fa fa-plus" style={{ marginRight: 10 }}></i> Зураг нэмэх
						</button>
					</div>
				</div>
			</div>
			<button className="button is-success" disabled={creating} onClick={() => submit()}>
				{id !== "new" ? "Хадгалах" : "Нэмэх"}
			</button>
		</div>
	);
};

export default NewCapital;
