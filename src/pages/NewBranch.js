import React, { useEffect, useState } from "react";
import { useLazyQuery, useMutation, useQuery } from "react-apollo";
import { Link, useParams } from "react-router-dom";
import BoxLoader from "../components/BoxLoader";
import ImageField from "../components/ImageField";
import Message from "../components/Message";
import { CREATE_BRANCH, UPDATE_BRANCH } from "../graphql/mutations";
import { GET_BRANCHES, GET_TYPES, GET_CURRENT_USER, GET_ONE_BRANCH } from "../graphql/queries";

const NewBranch = () => {
	const { id } = useParams();
	const [title, setTitle] = useState("");
	const [description, setDescription] = useState("");
	const [phonenumbers, setPhonenumbers] = useState([null]);
	const [workdays, setWorkDays] = useState("");
	const [weekend, setWeekend] = useState("");
	const [longitude, setLongitude] = useState("");
	const [latitude, setLatitude] = useState("");
	const [image, setImage] = useState(null);
	const [author, setAuthor] = useState("");
	const [type, setType] = useState("");

	const [message, setMessage] = useState("");
	const [messageType, setMessageType] = useState("");
	const [showMessage, setShowMessage] = useState(false);

	const { data: branchTypes } = useQuery(GET_TYPES, {
		variables: {
			language: "mn",
			slug: "branch",
		},
		onCompleted(data) {
			setType(data.getTypes[0].key);
		},
	});
	const [getCurrentUser, { data: currentUser }] = useLazyQuery(GET_CURRENT_USER);
	const [getOneBranch, { data: oneBranch, loading: fetchingOneBranch }] = useLazyQuery(GET_ONE_BRANCH, {
		variables: {
			_id: id,
		},
	});
	const [createBranch, { loading: creating }] = useMutation(CREATE_BRANCH, {
		onCompleted() {
			setMessage("Амжилттай нэмэгдлээ!");
			setMessageType("is-success");
			setShowMessage(true);

			setTitle("");
			setDescription("");
			setPhonenumbers([null]);
			setWorkDays("");
			setWeekend("");
			setLongitude("");
			setLatitude("");
			setImage(null);
			setAuthor("");
			setType("");
		},
		update(cache, { data: { createBranch } }) {
			const { getBranches } = cache.readQuery({
				query: GET_BRANCHES,
				variables: { language: createBranch.language, typeKey: "" },
			});

			cache.writeQuery({
				query: GET_BRANCHES,
				variables: { language: createBranch.language, typeKey: "" },
				data: { getBranches: [...getBranches, createBranch] },
			});
		},
	});
	const [updateBranch, { loading: updating }] = useMutation(UPDATE_BRANCH, {
		onCompleted() {
			setMessage("Амжилттай хадгалагдлаа!");
			setMessageType("is-success");
			setShowMessage(true);

			setTitle("");
			setDescription("");
			setPhonenumbers([null]);
			setWorkDays("");
			setWeekend("");
			setLongitude("");
			setLatitude("");
			setImage(null);
			setAuthor("");
			setType("");
		},
	});

	useEffect(() => {
		if (id) {
			if (id !== "new") {
				getOneBranch();
			} else {
				getCurrentUser();
			}
		}
	}, [id, getOneBranch, getCurrentUser]);

	useEffect(() => {
		if (currentUser) {
			setAuthor(currentUser.me.username);
		}
	}, [currentUser]);

	useEffect(() => {
		if (oneBranch) {
			if (oneBranch.getOneBranch) {
				setTitle(oneBranch.getOneBranch.title);
				setDescription(oneBranch.getOneBranch.description);
				setPhonenumbers(oneBranch.getOneBranch.phonenumbers);
				setWorkDays(oneBranch.getOneBranch.workdays);
				setWeekend(oneBranch.getOneBranch.weekend);
				setLongitude(oneBranch.getOneBranch.longitude);
				setLatitude(oneBranch.getOneBranch.latitude);
				setImage(oneBranch.getOneBranch.image);
				setAuthor(oneBranch.getOneBranch.author);
				setType(oneBranch.getOneBranch.type.key);
			}
		}
	}, [oneBranch]);

	const submit = () => {
		if (id !== "new") {
			updateBranch({
				variables: {
					_id: id,
					title: title,
					description: description,
					phonenumbers: phonenumbers.filter((number) => number !== null),
					workdays: workdays,
					weekend: weekend,
					longitude: longitude,
					latitude: latitude,
					image: image,
					author: author,
					typeKey: type,
				},
			});
		} else {
			createBranch({
				variables: {
					title: title,
					description: description,
					phonenumbers: phonenumbers.filter((number) => number !== null),
					workdays: workdays,
					weekend: weekend,
					longitude: longitude,
					latitude: latitude,
					image: image,
					author: author,
					typeKey: type,
				},
			});
		}
	};

	const updateImage = (image) => {
		setImage(image);
	};
	const addPhonenumber = () => {
		const numbers = [...phonenumbers, null];
		setPhonenumbers(numbers);
	};
	const removePhonenumber = () => {
		if (phonenumbers.length > 1) {
			phonenumbers.splice(phonenumbers.length - 1, 1);
			setPhonenumbers([...phonenumbers]);
		} else {
			updatePhonenumber(null, 0);
		}
	};

	const updatePhonenumber = (number, index) => {
		const newNumbers = [...phonenumbers];
		newNumbers[index] = number;
		setPhonenumbers(newNumbers);
	};

	useEffect(() => {
		if (showMessage) {
			setTimeout(() => {
				setShowMessage(false);
			}, [3000]);
		}
	}, [showMessage]);

	return (
		<div className="box container" style={{ maxWidth: "100%" }}>
			{fetchingOneBranch && <BoxLoader />}
			<div
				className="is-flex buttons"
				style={{
					justifyContent: "space-between",
					paddingBottom: 10,
					marginBottom: 20,
					borderBottom: "1px solid #ededed",
				}}
			>
				<h4 className="is-size-6 is-uppercase has-text-weight-bold">
					{id !== "new" ? "Салбарийн мэдээлэл засах" : "Салбарийн мэдээлэл оруулах"}
				</h4>
				<Link className="button is-danger is-light" to="/branches">
					Буцах
				</Link>
			</div>
			{showMessage && <Message message={message} type={messageType} />}
			<div className="columns">
				<div className="column is-8">
					<div className="field">
						<label className="label">Нийтлэгч</label>
						<div className="control is-flex">
							<input className="input" disabled={true} value={author} />
						</div>
					</div>
					<div className="field">
						<label className="label">Нэр</label>
						<div className="control">
							<input
								className="input"
								type="text"
								value={title}
								onChange={(e) => setTitle(e.target.value)}
							/>
						</div>
					</div>
					<div className="field">
						<label className="label">Байршил</label>
						<div className="control">
							<textarea
								className="textarea"
								type="text"
								value={description}
								onChange={(e) => setDescription(e.target.value)}
							/>
						</div>
					</div>
					<div className="is-flex" style={{ justifyContent: "space-between" }}>
						<div className="field" style={{ width: "50%" }}>
							<label className="label">Ажлын өдөр</label>
							<div className="control">
								<input
									className="input"
									type="text"
									value={workdays}
									onChange={(e) => setWorkDays(e.target.value)}
								/>
							</div>
						</div>
						<div className="field" style={{ width: "50%", marginLeft: 20 }}>
							<label className="label">Амралтын өдөр</label>
							<div className="control">
								<input
									className="input"
									type="text"
									value={weekend}
									onChange={(e) => setWeekend(e.target.value)}
								/>
							</div>
						</div>
					</div>
					<div className="is-flex" style={{ justifyContent: "space-between" }}>
						<div className="field" style={{ width: "50%" }}>
							<label className="label">Уртраг</label>
							<div className="control">
								<input
									className="input"
									type="text"
									value={longitude}
									onChange={(e) => setLongitude(e.target.value)}
								/>
							</div>
						</div>
						<div className="field" style={{ width: "50%", marginLeft: 20 }}>
							<label className="label">Өргөрөг</label>
							<div className="control">
								<input
									className="input"
									type="text"
									value={latitude}
									onChange={(e) => setLatitude(e.target.value)}
								/>
							</div>
						</div>
					</div>
					<div className="is-flex">
						<div className="field" style={{ width: "50%" }}>
							<label className="label">Холбоо барих утас</label>
							{phonenumbers.map((number, index) => {
								return (
									<div key={index} className="control" style={{ marginBottom: 10 }}>
										<input
											className="input"
											type="text"
											value={number}
											onChange={(e) => updatePhonenumber(e.target.value, index)}
										/>
									</div>
								);
							})}
							<div className="is-flex" style={{ justifyContent: "space-between" }}>
								<span
									className="button is-small is-uppercase"
									style={{
										width: "50%",
										backgroundColor: "transparent",
										border: "dashed 1px #48c774",
										height: 40,
										color: "#48c774",
										marginTop: 10,
									}}
									onClick={() => addPhonenumber()}
								>
									<i className="fa fa-plus" style={{ marginRight: 10 }}></i> Дугаар нэмэх
								</span>
								<span
									className="button is-small is-uppercase"
									style={{
										width: "45%",
										backgroundColor: "transparent",
										border: "dashed 1px red",
										height: 40,
										color: "red",
										marginTop: 10,
									}}
									onClick={() => removePhonenumber()}
								>
									<i className="fa fa-minus" style={{ marginRight: 10 }}></i> Дугаар хасах
								</span>
							</div>
						</div>
						<div className="field" style={{ width: "50%", marginLeft: 20 }}>
							<label className="label">Төрөл</label>
							<div className="select" style={{ marginRight: 10 }}>
								<select value={type} onChange={(e) => setType(e.target.value)}>
									{branchTypes?.getTypes.map((type) => {
										return (
											<option value={type.key} key={type.key}>
												{type.title}
											</option>
										);
									})}
								</select>
							</div>
						</div>
					</div>
				</div>
				<div className="column is-4 has-background-white-ter" style={{ borderRadius: 3 }}>
					<div className="field" style={{ padding: 15 }}>
						<label className="label">Зураг</label>
						<ImageField
							value={image}
							onChange={(val) => updateImage(val)}
							onDelete={() => updateImage(null)}
						/>
					</div>
				</div>
			</div>
			<div className="is-flex" style={{ justifyContent: "space-between" }}>
				<div />
				<div className="buttons">
					<button className="button is-success" disabled={creating || updating} onClick={() => submit()}>
						{id === "new" ? "Нэмэх" : "Хадгалах"}
					</button>
				</div>
			</div>
		</div>
	);
};

export default NewBranch;
