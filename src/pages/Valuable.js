import React, { useState } from "react";
import { GET_LANGUAGES, GET_VALUABLES } from "../graphql/queries";
import { useQuery, useMutation } from "react-apollo";
import { DELETE_VALUABLE } from "../graphql/mutations";
import Modal from "../components/Modal";
import AcceptModal from "../components/AcceptModal";
import NewValuableModal from "../components/NewValuableModal";
import ValuableDetailModal from "../components/ValuableDetailModal";
import BoxLoader from "../components/BoxLoader";

const Valuable = () => {
	const [language, setLanguage] = useState("mn");
	const [selectedValuable, setSelectedValuable] = useState(null);
	const [isNew, setIsNew] = useState(false);
	const [isDetail, setIsDetail] = useState(false);
	const [isDelete, setIsDelete] = useState(false);

	const { data, loading, refetch } = useQuery(GET_VALUABLES, {
		variables: {
			language: language,
		},
	});

	const [deleteValuable] = useMutation(DELETE_VALUABLE, {
		variables: {
			key: selectedValuable?.key,
		},
		onCompleted() {
			refetch();
		},
	});

	const { data: languageData } = useQuery(GET_LANGUAGES);

	const deleteModalHandler = () => {
		setIsDelete(!isDelete);
	};
	const detailModalHandler = () => {
		setIsDetail(!isDetail);
		setSelectedValuable(null);
	};
	const newModalHandler = () => {
		setIsNew(!isNew);
	};

	return (
		<div className="box container" style={{ maxWidth: "100%" }}>
			{loading && <BoxLoader />}
			<Modal active={isDelete} modalHandler={deleteModalHandler}>
				<AcceptModal
					message="Устгахыг зөвшөөрч байна уу?"
					modalHandler={deleteModalHandler}
					yes={deleteValuable}
				/>
			</Modal>
			<Modal active={isDetail}>
				<ValuableDetailModal modalHandler={detailModalHandler} valuable={selectedValuable} />
			</Modal>
			<Modal active={isNew} modalHandler={newModalHandler}>
				<NewValuableModal modalHandler={newModalHandler} valuable={selectedValuable} />
			</Modal>
			<div
				className="is-flex buttons"
				style={{
					justifyContent: "space-between",
					paddingBottom: 10,
					marginBottom: 20,
					borderBottom: "1px solid #ededed",
				}}
			>
				<h4 className="is-size-6 is-uppercase has-text-weight-bold">Үнэт зүйлс</h4>
				<div>
					<div className="select" style={{ marginRight: 10 }}>
						<select value={language} onChange={(e) => setLanguage(e.target.value)}>
							{languageData?.getLanguages.map((language) => {
								return (
									<option value={language.slug} key={language.slug}>
										{language.country}
									</option>
								);
							})}
						</select>
					</div>
					<button className="button is-success" onClick={() => setIsNew(true)}>
						Шинэ
					</button>
				</div>
			</div>
			<table className="table is-fullwidth is-bordered">
				<thead>
					<tr>
						<th>Нэр</th>
						<th>Хэл</th>
						<th style={{ width: 1 }}></th>
					</tr>
				</thead>
				<tbody>
					{data?.getValuables &&
						data.getValuables.map((valuable) => {
							return (
								<tr key={valuable._id}>
									<td>{valuable.name === " " ? "Бөглөөгүй байна" : valuable.name}</td>
									<td>{valuable.language}</td>
									<td>
										<span className="is-flex">
											<button
												className="button has-background-danger-light is-small"
												onClick={() => {
													setSelectedValuable(valuable);
													setIsDelete(true);
												}}
											>
												<i className="fas fa-trash-alt has-text-danger" />
											</button>
											<button
												style={{ marginLeft: 10 }}
												className={
													valuable.name === " "
														? "button is-small is-danger is-light"
														: "button is-small"
												}
												onClick={() => {
													setSelectedValuable(valuable);
													setIsDetail(true);
												}}
											>
												Засах
											</button>
										</span>
									</td>
								</tr>
							);
						})}
				</tbody>
			</table>
		</div>
	);
};

export default Valuable;
