import React, { useEffect, useState } from "react";
import { useMutation, useQuery } from "react-apollo";
import BoxLoader from "../components/BoxLoader";
import ContactTabItem from "../components/ContactTabItem";
import Message from "../components/Message";
import { UPDATE_CONTACT } from "../graphql/mutations";
import { GET_CONTACT, GET_LANGUAGES } from "../graphql/queries";

const Contact = () => {
	const [language, setLanguage] = useState("mn");

	const [address, setAddress] = useState("");
	const [longitude, setLongitude] = useState("");
	const [latitude, setLatitude] = useState("");
	const [phonenumber, setPhonenumber] = useState("");
	const [fax, setFax] = useState("");
	const [email, setEmail] = useState("");
	const [workDays, setWorkDays] = useState("");
	const [weekend, setWeekend] = useState("");
	const [branches, setBranches] = useState([null]);

	const [message, setMessage] = useState("");
	const [messageType, setMessageType] = useState("");
	const [showMessage, setShowMessage] = useState(false);

	const { data: languageData } = useQuery(GET_LANGUAGES);
	const { data, loading } = useQuery(GET_CONTACT, {
		variables: {
			language: language,
		},
		onCompleted(data) {
			setAddress(data.getContact.address);
			setLongitude(data.getContact.longitude);
			setLatitude(data.getContact.latitude);
			setPhonenumber(data.getContact.phonenumber);
			setFax(data.getContact.fax);
			setEmail(data.getContact.email);
			setWorkDays(data.getContact.workDays);
			setWeekend(data.getContact.weekend);
			setBranches(data.getContact.branches);
		},
	});
	const [updateContact] = useMutation(UPDATE_CONTACT, {
		onCompleted() {
			setMessage("Амжилттай хадгалагдлаа!");
			setMessageType("is-success");
			setShowMessage(true);
		},
	});

	const submit = () => {
		updateContact({
			variables: {
				_id: data.getContact._id,
				address: address,
				longitude: longitude,
				latitude: latitude,
				phonenumber: phonenumber,
				fax: fax,
				email: email,
				workDays: workDays,
				weekend: weekend,
				branches: branches
					.filter((branch) => branch !== null)
					.map((data) => {
						return {
							title: data.title,
							number: data.number,
						};
					}),
			},
		});
	};

	const addOneBranch = () => {
		const newBranches = [...branches, null];
		setBranches(newBranches);
	};
	const deleteBranch = (index) => {
		if (branches.length > 1) {
			branches.splice(index, 1);
			setBranches([...branches]);
		} else {
			updateBranch(null, index);
		}
	};
	const updateBranch = (tab, index) => {
		const newBranches = [...branches];
		newBranches[index] = tab;
		setBranches(newBranches);
	};

	useEffect(() => {
		if (showMessage) {
			setTimeout(() => {
				setShowMessage(false);
			}, [3000]);
		}
	}, [showMessage]);

	return (
		<div className="box container" style={{ maxWidth: "100%" }}>
			{loading && <BoxLoader />}
			<div
				className="is-flex buttons"
				style={{
					justifyContent: "space-between",
					paddingBottom: 10,
					marginBottom: 20,
					borderBottom: "1px solid #ededed",
				}}
			>
				<h4 className="is-size-6 is-uppercase has-text-weight-bold">Холбоо барих</h4>
				<div className="select" style={{ marginRight: 10 }}>
					<select value={language} onChange={(e) => setLanguage(e.target.value)}>
						{languageData?.getLanguages.map((language) => {
							return (
								<option value={language.slug} key={language.slug}>
									{language.country}
								</option>
							);
						})}
					</select>
				</div>
			</div>
			{showMessage && <Message message={message} type={messageType} />}
			<div>
				<div className="field">
					<label className="label">Хаяг</label>
					<div className="control">
						<textarea
							className="textarea"
							type="text"
							value={address}
							onChange={(e) => setAddress(e.target.value)}
							required
						/>
					</div>
				</div>
				<div className="columns">
					<div className="column is-4">
						<div className="field">
							<label className="label">Уртраг</label>
							<div className="control">
								<input
									className="input"
									type="text"
									value={longitude}
									onChange={(e) => setLongitude(e.target.value)}
									required
								/>
							</div>
						</div>
						<div className="field">
							<label className="label">Өргөрөг</label>
							<div className="control">
								<input
									className="input"
									type="text"
									value={latitude}
									onChange={(e) => setLatitude(e.target.value)}
									required
								/>
							</div>
						</div>
					</div>
					<div className="column is-4">
						<div className="field">
							<label className="label">Лавлах</label>
							<div className="control">
								<input
									className="input"
									type="text"
									value={phonenumber}
									onChange={(e) => setPhonenumber(e.target.value)}
									required
								/>
							</div>
						</div>
						<div className="field">
							<label className="label">Факс</label>
							<div className="control">
								<input
									className="input"
									type="text"
									value={fax}
									onChange={(e) => setFax(e.target.value)}
									required
								/>
							</div>
						</div>
						<div className="field">
							<label className="label">Имейл</label>
							<div className="control">
								<input
									className="input"
									type="text"
									value={email}
									onChange={(e) => setEmail(e.target.value)}
									required
								/>
							</div>
						</div>
						<div className="field">
							<label className="label">Да-Ба</label>
							<div className="control">
								<input
									className="input"
									type="text"
									value={workDays}
									onChange={(e) => setWorkDays(e.target.value)}
									required
								/>
							</div>
						</div>
						<div className="field">
							<label className="label">Бя-Нямд</label>
							<div className="control">
								<input
									className="input"
									type="text"
									value={weekend}
									onChange={(e) => setWeekend(e.target.value)}
									required
								/>
							</div>
						</div>
					</div>
					<div className="column is-4 has-background-white-ter">
						<label className="label">Салбар</label>
						{branches.map((branch, index) => {
							return (
								<ContactTabItem
									key={index}
									value={branch}
									onDelete={() => deleteBranch(index)}
									onChange={(data) => updateBranch(data, index)}
								/>
							);
						})}
						<span
							className="button is-small is-uppercase"
							style={{
								width: "100%",
								backgroundColor: "transparent",
								border: "dashed 1px #48c774",
								height: 40,
								color: "#48c774",
							}}
							onClick={() => addOneBranch()}
						>
							<i className="fa fa-plus" style={{ marginRight: 10 }}></i> Салбар нэмэх
						</span>
					</div>
				</div>
			</div>
			<div className="buttons">
				<button className="button is-success" onClick={() => submit()}>
					Хадгалах
				</button>
			</div>
		</div>
	);
};

export default Contact;
