import React, { useState, useEffect } from "react";
import { Link, useParams } from "react-router-dom";
import { GET_ONE_GALLERY } from "../graphql/queries";
import { useQuery, useMutation } from "react-apollo";
import BoxLoader from "../components/BoxLoader";
import GalleryItem from "../components/GalleryItem";
import { UPDATE_GALLERY } from "../graphql/mutations";
import Message from "../components/Message";

const EditGallery = () => {
	const { id } = useParams();

	const [name, setName] = useState("");
	const [cat, setCat] = useState(null);
	const [galleryItem, setGalleryItem] = useState([null]);

	const [message, setMessage] = useState("");
	const [messageType, setMessageType] = useState("");
	const [showMessage, setShowMessage] = useState(false);

	const { data, loading } = useQuery(GET_ONE_GALLERY, {
		variables: {
			_id: id,
		},
		onCompleted(data) {
			setName(data.getOneGallery.name);
			setCat(data.getOneGallery.category);
			setGalleryItem(data.getOneGallery.images);
		},
	});

	const [updateGallery] = useMutation(UPDATE_GALLERY, {
		variables: {
			_id: id,
			name: name,
			images: galleryItem
				.filter((item) => item !== null)
				.map((oneItem) => {
					return {
						title: oneItem.title,
						image: {
							_id: oneItem?.image._id,
							path: oneItem?.image.path,
							width: oneItem?.image.width,
							height: oneItem?.image.height,
							type: oneItem?.image.type,
						},
					};
				}),
		},
		onCompleted() {
			setMessage("Амжилттай хадгалагдлаа!");
			setMessageType("is-success");
			setShowMessage(true);
		},
	});

	function addNewItem() {
		const newItems = [...galleryItem, null];
		setGalleryItem(newItems);
	}
	function updateItems(item, index) {
		const newItems = [...galleryItem];
		newItems[index] = item;
		setGalleryItem(newItems);
	}
	function deleteItem(index) {
		if (galleryItem.length > 1) {
			galleryItem.splice(index, 1);
			setGalleryItem([...galleryItem]);
		} else {
			updateItems(null, index);
		}
	}

	useEffect(() => {
		console.log(galleryItem);
	}, [galleryItem]);

	return (
		<div className="box container" style={{ maxWidth: "100%" }}>
			{showMessage && <Message message={message} type={messageType} />}
			<div
				className="is-flex"
				style={{
					justifyContent: "space-between",
					paddingBottom: 10,
					marginBottom: 20,
					borderBottom: "1px solid #ededed",
				}}
			>
				<h4 className="is-size-6 is-uppercase has-text-weight-bold">Галерей засах</h4>
				<Link className="button is-danger is-light" to="/galleries">
					Буцах
				</Link>
			</div>
			{loading && <BoxLoader />}
			<div className="columns">
				<div className="column is-6">
					<div className="field">
						<label className="label">Нийтлэгч</label>
						<div className="control is-flex">
							<input className="input" disabled={true} value={data?.getOneGallery.author} />
						</div>
					</div>
					<div className="is-flex" style={{ justifyContent: "space-between" }}>
						<div className="field" style={{ width: "45%" }}>
							<label className="label">Нэр</label>
							<div className="control is-flex">
								<input className="input" value={name} onChange={(e) => setName(e.target.value)} />
							</div>
						</div>
						<div className="field" style={{ width: "50%" }}>
							<label className="label">Цэс</label>
							<div className="control is-flex">
								<input className="input" disabled={true} value={cat && cat.name} />
							</div>
						</div>
					</div>
				</div>
				<div className="column is-6 has-background-white-ter">
					<div className="field">
						<label className="label">Зурагнууд</label>
						<div className="control">
							{galleryItem.map((item, index) => (
								<GalleryItem
									key={index}
									value={item}
									onChange={(val) => updateItems(val, index)}
									onDelete={() => deleteItem(index)}
								/>
							))}
							<button
								className="button is-small is-uppercase"
								style={{
									display: "inline-block",
									width: "100%",
									backgroundColor: "transparent",
									border: "dashed 1px #48c774",
									height: 40,
									color: "#48c774",
								}}
								onClick={addNewItem}
							>
								<i className="fa fa-plus" style={{ marginRight: 10 }}></i> Зураг нэмэх
							</button>
						</div>
					</div>
				</div>
			</div>
			<div className="buttons is-flex" style={{ justifyContent: "space-between" }}>
				<div />
				<div>
					<button className="button is-success" onClick={() => updateGallery()}>
						Хадгалах
					</button>
				</div>
			</div>
		</div>
	);
};

export default EditGallery;
