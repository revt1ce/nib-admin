import CKEditor from "ckeditor4-react";
import React, { useEffect, useState } from "react";
import { useLazyQuery, useMutation } from "react-apollo";
import { Link, useParams } from "react-router-dom";
import BoxLoader from "../components/BoxLoader";
import Message from "../components/Message";
import { CREATE_JOB, UPDATE_JOB } from "../graphql/mutations";
import { GET_CURRENT_USER, GET_JOBS, GET_ONE_JOB } from "../graphql/queries";

const NewTender = () => {
	const { id } = useParams();

	const [author, setAuthor] = useState("");
	const [title, setTitle] = useState("");
	const [number, setNumber] = useState("");
	const [startDate, setStartDate] = useState("");
	const [endDate, setEndDate] = useState("");
	const [body, setBody] = useState("");
	const [contact, setContact] = useState("");
	const [ignore, setIgnore] = useState("");
	const [email, setEmail] = useState("");
	const [address, setAddress] = useState("");

	const [message, setMessage] = useState("");
	const [messageType, setMessageType] = useState("");
	const [showMessage, setShowMessage] = useState(false);
	const [createJob] = useMutation(CREATE_JOB, {
		onCompleted() {
			window.scrollTo(0, 0);
			setMessage("Амжилттай нэмэгдлээ!");
			setMessageType("is-success");
			setShowMessage(true);
			window.scrollTo(0, 0);
		},
		onError() {
			setMessage("Алдаа гарлаа!");
			setMessageType("is-danger");
			setShowMessage(true);
		},
		update(cache, { data: { createJob } }) {
			const { getJobs } = cache.readQuery({
				query: GET_JOBS,
				variables: { language: "mn", type: "tender" },
			});

			cache.writeQuery({
				query: GET_JOBS,
				variables: { language: "mn", type: "tender" },
				data: { getJobs: [...getJobs, createJob] },
			});
		},
	});

	const [updateJob] = useMutation(UPDATE_JOB, {
		onCompleted() {
			window.scrollTo(0, 0);
			setMessage("Амжилттай хадгалагдлаа!");
			setMessageType("is-success");
			setShowMessage(true);
			window.scrollTo(0, 0);
		},
	});

	const [getCurrentUser, { data: currentUserData }] = useLazyQuery(GET_CURRENT_USER);

	const [getOneJob, { data: oneJobData, loading: fetchingOneJob }] = useLazyQuery(GET_ONE_JOB, {
		variables: {
			_id: id,
		},
	});

	useEffect(() => {
		if (id !== "new") {
			getOneJob();
		} else {
			getCurrentUser();
		}
	}, [id, getCurrentUser, getOneJob]);

	useEffect(() => {
		if (currentUserData) {
			if (currentUserData.me) {
				setAuthor(currentUserData.me.username);
			}
		}
	}, [currentUserData]);

	useEffect(() => {
		if (oneJobData) {
			if (oneJobData.getOneJob) {
				setTitle(oneJobData.getOneJob.title);
				setNumber(oneJobData.getOneJob.jobNumber);
				setStartDate(oneJobData.getOneJob.startDate);
				setEndDate(oneJobData.getOneJob.endDate);
				setBody(oneJobData.getOneJob.body);
				setContact(oneJobData.getOneJob.contact);
				setIgnore(oneJobData.getOneJob.ignore);
				setEmail(oneJobData.getOneJob.recieveEmail);
				setAddress(oneJobData.getOneJob.recieveAddress);
				setAuthor(oneJobData.getOneJob.author);
			}
		}
	}, [oneJobData]);

	const submit = () => {
		if (id !== "new") {
			updateJob({
				variables: {
					_id: id,
					title: title,
					startDate: startDate,
					endDate: endDate,
					jobNumber: number,
					body: body,
					contact: contact,
					ignore: ignore,
					recieveEmail: email,
					recieveAddress: address,
					type: "tender",
					author: author,
				},
			});
		} else {
			createJob({
				variables: {
					title: title,
					startDate: startDate,
					endDate: endDate,
					jobNumber: number,
					body: body,
					contact: contact,
					ignore: ignore,
					recieveEmail: email,
					recieveAddress: address,
					type: "tender",
					author: author,
				},
			});
		}
	};

	useEffect(() => {
		if (showMessage) {
			setTimeout(() => {
				setShowMessage(false);
			}, [3000]);
		}
	}, [showMessage]);

	return (
		<div className="box container" style={{ maxWidth: "100%" }}>
			{fetchingOneJob && <BoxLoader />}
			<div
				className="is-flex"
				style={{
					justifyContent: "space-between",
					paddingBottom: 10,
					marginBottom: 20,
					borderBottom: "1px solid #ededed",
				}}
			>
				<h4 className="is-size-6 is-uppercase has-text-weight-bold">Тендер оруулах</h4>
				<Link
					className="button is-danger is-light"
					to={id !== "new" ? `/tender/detail/${oneJobData?.getOneJob.key}` : "/tender"}
				>
					Буцах
				</Link>
			</div>
			{showMessage && <Message message={message} type={messageType} />}
			<div className="columns">
				<div className="column is-6">
					<div className="is-flex" style={{ justifyContent: "space-between" }}>
						<div className="field" style={{ width: "100%" }}>
							<label className="label">Нийтлэгч</label>
							<div className="control is-flex">
								<input className="input" disabled={true} value={author} />
							</div>
						</div>
					</div>

					<div className="field">
						<label className="label">Нэр</label>
						<div className="control is-flex">
							<input className="input" value={title} onChange={(e) => setTitle(e.target.value)} />
						</div>
					</div>
					<div className="field">
						<label className="label">Дугаар</label>
						<div className="control is-flex">
							<input className="input" value={number} onChange={(e) => setNumber(e.target.value)} />
						</div>
					</div>
					<div className="is-flex">
						<div className="field" style={{ width: "50%" }}>
							<label className="label">Эхлэх хугацаа</label>
							<div className="control is-flex">
								<input
									className="input"
									value={startDate}
									onChange={(e) => setStartDate(e.target.value)}
								/>
							</div>
						</div>
						<div className="field" style={{ width: "50%", marginLeft: 20 }}>
							<label className="label">Дуусах хугацаа</label>
							<div className="control is-flex">
								<input className="input" value={endDate} onChange={(e) => setEndDate(e.target.value)} />
							</div>
						</div>
					</div>
					<div className="field">
						<label className="label">Агуулга</label>
						<div className="control">
							<CKEditor data={body} onChange={(evt) => setBody(evt.editor.getData())} />
						</div>
					</div>
				</div>
				<div className="column is-6">
					<div className="field">
						<label className="label">Лавлах</label>
						<div className="control is-flex">
							<textarea
								className="textarea"
								value={contact}
								onChange={(e) => setContact(e.target.value)}
							/>
						</div>
					</div>
					<div className="field">
						<label className="label">Үл зөвшөөрөх</label>
						<div className="control is-flex">
							<textarea className="textarea" value={ignore} onChange={(e) => setIgnore(e.target.value)} />
						</div>
					</div>
					<div className="field">
						<label className="label">CV хүлээн авах цахим хаяг</label>
						<div className="control is-flex">
							<textarea className="textarea" value={email} onChange={(e) => setEmail(e.target.value)} />
						</div>
					</div>
					<div className="field">
						<label className="label">CV хүлээн биеээр авах хаяг</label>
						<div className="control is-flex">
							<textarea
								className="textarea"
								value={address}
								onChange={(e) => setAddress(e.target.value)}
							/>
						</div>
					</div>
				</div>
			</div>
			<div className="buttons">
				<button className="button is-success" onClick={() => submit()}>
					Нэмэх
				</button>
			</div>
		</div>
	);
};

export default NewTender;
